
import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

import 'enums/transaction_type.dart';
import 'model/expense.dart';
import 'model/transaction.dart';

class MockData {
  static BarChartGroupData makeGroupData(
      int x, double y1, Color barColor, double width) {
    return BarChartGroupData(
      barsSpace: 1,
      x: x,
      barRods: [
        BarChartRodData(
          color: barColor,
          width: width,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ), toY: y1,
        ),
      ],
    );
  }

  static List<BarChartGroupData> getBarChartItems(data,Color color,
      {double width = 20}) {
   return List<BarChartGroupData>.from(
      data.asMap().entries.map((item) => makeGroupData(item.key, 5, color, width)), // Replace Text() with your desired widget
    );
  }

  static List<Transaction> get transactions {
    return [
      Transaction(
        "45673",
        "Spotify",
        DateTime.now(),
        569.50,
        TransactionType.outgoing,
        "https://i.cbc.ca/1.6128145.1628713609!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_940/regina-john-doe.jpg",
      ),
      Transaction(
        "76154",
        "Transfer",
        DateTime.now(),
        350.50,
        TransactionType.incoming,
        "https://i.cbc.ca/1.6128145.1628713609!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_940/regina-john-doe.jpg",
      ),
      Transaction(
        "322587",
        "Investments",
        DateTime.now(),
        3448.99,
        TransactionType.outgoing,
        "https://i.cbc.ca/1.6128145.1628713609!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_940/regina-john-doe.jpg",
      ),
    ];
  }

  static List<Expense> get otherExpanses {
    return [
      Expense(
        color: ColorsManager.main,
        expenseName: "Other expenses",
        expensePercentage: 40,
      ),
      Expense(
        color: ColorsManager.warning,
        expenseName: "Entertainment",
        expensePercentage: 45,
      ),
      Expense(
        color: ColorsManager.success,
        expenseName: "Investments",
        expensePercentage: 15,
      )
    ];
  }
}
