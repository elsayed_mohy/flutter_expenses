
import 'dart:convert';

class BoxDetails {
  dynamic id;
  dynamic name;
  List<Member> members;
  dynamic invitations;
  dynamic notes;
  dynamic totalExpense;
  dynamic totalIncome;
  dynamic currentBalance;
  dynamic edetable;

  BoxDetails({
     this.id,
     this.name,
    required this.members,
    this.invitations,
    this.notes,
     this.totalExpense,
     this.totalIncome,
     this.currentBalance,
     this.edetable,
  });


  factory BoxDetails.fromRawJson(String str) => BoxDetails.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BoxDetails.fromJson(Map<String, dynamic> json) => BoxDetails(
    id: json["id"],
    name: json["name"],
    members: List<Member>.from(json["members"].map((x) => Member.fromJson(x))),
    invitations: json["invitations"],
    notes: json["notes"],
    totalExpense: json["totalExpense"],
    totalIncome: json["totalIncome"],
    currentBalance: json["currentBalance"],
    edetable: json["edetable"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "members": List<dynamic>.from(members.map((x) => x.toJson())),
    "invitations": invitations,
    "notes": notes,
    "totalExpense": totalExpense,
    "totalIncome": totalIncome,
    "currentBalance": currentBalance,
    "edetable": edetable,
  };
}

class Member {
  dynamic box;
  MemberInfo member;
  bool admin;

  Member({
    this.box,
    required this.member,
    required this.admin,
  });


  factory Member.fromRawJson(String str) => Member.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Member.fromJson(Map<String, dynamic> json) => Member(
    box: json["box"],
    member: MemberInfo.fromJson(json["member"]),
    admin: json["admin"],
  );

  Map<String, dynamic> toJson() => {
    "box": box,
    "member": member.toJson(),
    "admin": admin,
  };
}

class MemberInfo {
  dynamic id;
  dynamic fullName;
  dynamic phoneNumber;
  dynamic email;
  dynamic imagePath;

  MemberInfo({
     this.id,
     this.fullName,
    this.phoneNumber,
    this.email,
    this.imagePath,
  });


  factory MemberInfo.fromRawJson(String str) => MemberInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberInfo.fromJson(Map<String, dynamic> json) => MemberInfo(
    id: json["id"],
    fullName: json["fullName"],
    phoneNumber: json["phoneNumber"],
    email: json["email"],
    imagePath: json["imagePath"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullName": fullName,
    "phoneNumber": phoneNumber,
    "email": email,
    "imagePath": imagePath,
  };
}
