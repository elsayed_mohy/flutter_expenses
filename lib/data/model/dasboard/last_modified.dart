// To parse this JSON data, do
//
//     final lastModified = lastModifiedFromJson(jsonString);

import 'dart:convert';

LastModified lastModifiedFromJson(String str) => LastModified.fromJson(json.decode(str));

String lastModifiedToJson(LastModified data) => json.encode(data.toJson());

class LastModified {
  dynamic id;
  dynamic amount;
  dynamic beneficiary;
  dynamic tag;
  dynamic notes;
  Box box;
  String createdDate;
  bool edetable;
  bool expense;

  LastModified({
    required this.id,
    required this.amount,
    required this.beneficiary,
    required this.tag,
    required this.notes,
    required this.box,
    required this.createdDate,
    required this.edetable,
    required this.expense,
  });

  factory LastModified.fromJson(Map<String, dynamic> json) => LastModified(
    id: json["id"],
    amount: json["amount"],
    beneficiary: json["beneficiary"],
    tag: json["tag"],
    notes: json["notes"],
    box: Box.fromJson(json["box"]),
    createdDate: json["createdDate"],
    edetable: json["edetable"],
    expense: json["expense"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "amount": amount,
    "beneficiary": beneficiary,
    "tag": tag,
    "notes": notes,
    "box": box.toJson(),
    "createdDate": createdDate,
    "edetable": edetable,
    "expense": expense,
  };
}

class Box {
  int id;
  String name;

  Box({
    required this.id,
    required this.name,
  });

  factory Box.fromJson(Map<String, dynamic> json) => Box(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}
