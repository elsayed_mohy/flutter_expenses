// To parse this JSON data, do
//
//     final fundNumber = fundNumberFromJson(jsonString);

import 'dart:convert';

FundNumber fundNumberFromJson(String str) => FundNumber.fromJson(json.decode(str));

String fundNumberToJson(FundNumber data) => json.encode(data.toJson());

class FundNumber {
  dynamic elementCount;

  FundNumber({
    required this.elementCount,
  });

  factory FundNumber.fromJson(Map<String, dynamic> json) => FundNumber(
    elementCount: json["elementCount"],
  );

  Map<String, dynamic> toJson() => {
    "elementCount": elementCount,
  };
}
