//
//     final maxBox = maxBoxFromJson(jsonString);

import 'dart:convert';

MaxBox maxBoxFromJson(String str) => MaxBox.fromJson(json.decode(str));

String maxBoxToJson(MaxBox data) => json.encode(data.toJson());

class MaxBox {
  String name;
  dynamic elementCount;
  dynamic elementSum;
  dynamic elementIndex;

  MaxBox({
    required this.name,
    this.elementCount,
    required this.elementSum,
    required this.elementIndex,
  });

  factory MaxBox.fromJson(Map<String, dynamic> json) => MaxBox(
    name: json["name"],
    elementCount: json["elementCount"],
    elementSum: json["elementSum"],
    elementIndex: json["elementIndex"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "elementCount": elementCount,
    "elementSum": elementSum,
    "elementIndex": elementIndex,
  };
}