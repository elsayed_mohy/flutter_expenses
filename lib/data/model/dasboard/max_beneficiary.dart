// To parse this JSON data, do
//
//     final maxBeneficiary = maxBeneficiaryFromJson(jsonString);

import 'dart:convert';

MaxBeneficiary maxBeneficiaryFromJson(String str) => MaxBeneficiary.fromJson(json.decode(str));

String maxBeneficiaryToJson(MaxBeneficiary data) => json.encode(data.toJson());

class MaxBeneficiary {
  String name;
  dynamic elementCount;
  dynamic elementSum;

  MaxBeneficiary({
    required this.name,
    this.elementCount,
    required this.elementSum,
  });

  factory MaxBeneficiary.fromJson(Map<String, dynamic> json) => MaxBeneficiary(
    name: json["name"],
    elementCount: json["elementCount"],
    elementSum: json["elementSum"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "elementCount": elementCount,
    "elementSum": elementSum,
  };
}
