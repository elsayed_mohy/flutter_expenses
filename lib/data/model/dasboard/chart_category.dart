// To parse this JSON data, do
//
//     final chartCategory = chartCategoryFromJson(jsonString);

import 'dart:convert';

ChartCategory chartCategoryFromJson(String str) => ChartCategory.fromJson(json.decode(str));

String chartCategoryToJson(ChartCategory data) => json.encode(data.toJson());

class ChartCategory {
  dynamic name;
  dynamic elementCount;
  dynamic elementSum;

  ChartCategory({
    required this.name,
    required this.elementCount,
    required this.elementSum,
  });

  factory ChartCategory.fromJson(Map<String, dynamic> json) => ChartCategory(
    name: json["name"],
    elementCount: json["elementCount"],
    elementSum: json["elementSum"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "elementCount": elementCount,
    "elementSum": elementSum,
  };
}
