// To parse this JSON data, do
//
//     final userSummary = userSummaryFromJson(jsonString);

import 'dart:convert';

UserSummary userSummaryFromJson(String str) => UserSummary.fromJson(json.decode(str));

String userSummaryToJson(UserSummary data) => json.encode(data.toJson());

class UserSummary {
  dynamic totalIncome;
  dynamic elementCount;
  dynamic totalOutcome;
  dynamic difference;

  UserSummary({
    required this.totalIncome,
    this.elementCount,
    required this.totalOutcome,
    required this.difference,
  });

  factory UserSummary.fromJson(Map<String, dynamic> json) => UserSummary(
    totalIncome: json["totalIncome"],
    elementCount: json["elementCount"],
    totalOutcome: json["totalOutcome"],
    difference: json["difference"],
  );

  Map<String, dynamic> toJson() => {
    "totalIncome": totalIncome,
    "elementCount": elementCount,
    "totalOutcome": totalOutcome,
    "difference": difference,
  };
}
