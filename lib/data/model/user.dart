// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  int id;
  String fullName;
  String phoneNumber;
  String accessToken;
  String refreshToken;
  String imagePath;
  dynamic preferLanguage;

  User({
    required this.id,
    required this.fullName,
    required this.phoneNumber,
    required this.accessToken,
    required this.refreshToken,
    required this.imagePath,
    this.preferLanguage,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        fullName: json["fullName"],
        phoneNumber: json["phoneNumber"],
        accessToken: json["accessToken"],
        refreshToken: json["refreshToken"],
        imagePath: json["imagePath"],
        preferLanguage: json["preferLanguage"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "fullName": fullName,
        "phoneNumber": phoneNumber,
        "accessToken": accessToken,
        "refreshToken": refreshToken,
        "imagePath": imagePath,
        "preferLanguage": preferLanguage,
      };
}
