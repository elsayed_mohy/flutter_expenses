// To parse this JSON data, do
//
//     final invitation = invitationFromJson(jsonString);

import 'dart:convert';

class Invitation {
  dynamic id;
  dynamic phoneNumber;
  User user;
  Box box;
  Status status;
  dynamic toUserName;
  dynamic expired;

  Invitation({
     this.id,
    required this.phoneNumber,
    required this.user,
    required this.box,
    required this.status,
     this.toUserName,
     this.expired,
  });

  factory Invitation.fromRawJson(String str) => Invitation.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Invitation.fromJson(Map<String, dynamic> json) => Invitation(
    id: json["id"],
    phoneNumber: json["phoneNumber"],
    user: User.fromJson(json["user"]),
    box: Box.fromJson(json["box"]),
    status: Status.fromJson(json["status"]),
    toUserName: json["toUserName"],
    expired: json["expired"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "phoneNumber": phoneNumber,
    "user": user.toJson(),
    "box": box.toJson(),
    "status": status.toJson(),
    "toUserName": toUserName,
    "expired": expired,
  };
}

class Box {
  int id;
  String name;

  Box({
    required this.id,
    required this.name,
  });

  factory Box.fromRawJson(String str) => Box.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Box.fromJson(Map<String, dynamic> json) => Box(
    id: json["id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
  };
}

class Status {
  dynamic id;
  dynamic name;
  dynamic color;
  dynamic code;

  Status({
     this.id,
     this.name,
     this.color,
     this.code,
  });

  factory Status.fromRawJson(String str) => Status.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Status.fromJson(Map<String, dynamic> json) => Status(
    id: json["id"],
    name: json["name"],
    color: json["color"],
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "color": color,
    "code": code,
  };
}

class User {
  dynamic id;
  dynamic fullName;
  dynamic phoneNumber;
  dynamic email;
  dynamic imagePath;

  User({
     this.id,
     this.fullName,
    this.phoneNumber,
    this.email,
    this.imagePath,
  });

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    fullName: json["fullName"],
    phoneNumber: json["phoneNumber"],
    email: json["email"],
    imagePath: json["imagePath"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullName": fullName,
    "phoneNumber": phoneNumber,
    "email": email,
    "imagePath": imagePath,
  };
}
