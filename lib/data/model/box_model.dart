// To parse this JSON data, do
//
//     final box = boxFromJson(jsonString);

import 'dart:convert';

class BoxModel {
  dynamic id;
  dynamic name;
  dynamic numberOfMembers;
  dynamic totalExpense;
  dynamic totalIncome;
  dynamic currentBalance;
  dynamic members;
  dynamic edetable;
  dynamic highestTag;

  BoxModel({
    required this.id,
    required this.name,
    required this.numberOfMembers,
    required this.totalExpense,
    required this.totalIncome,
    required this.currentBalance,
    this.members,
    required this.edetable,
    required this.highestTag,
  });


  factory BoxModel.fromJson(Map<String, dynamic> json) => BoxModel(
    id: json["id"],
    name: json["name"],
    numberOfMembers: json["numberOfMembers"],
    totalExpense: json["totalExpense"],
    totalIncome: json["totalIncome"],
    currentBalance: json["currentBalance"],
    members: json["members"],
    edetable: json["edetable"],
    highestTag: json["highestTag"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "numberOfMembers": numberOfMembers,
    "totalExpense": totalExpense,
    "totalIncome": totalIncome,
    "currentBalance": currentBalance,
    "members": members,
    "edetable": edetable,
    "highestTag": highestTag,
  };
}
