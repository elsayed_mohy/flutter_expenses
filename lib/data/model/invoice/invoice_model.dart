// To parse this JSON data, do
//
//     final invoice = invoiceFromJson(jsonString);

import 'dart:convert';

class InvoiceModel {
  dynamic id;
  dynamic box;
  dynamic beneficiary;
  dynamic amount;
  dynamic tag;
  dynamic notes;
  dynamic createdDate;
  List<Attachment> attachments;
  dynamic edetable;
  dynamic expense;

  InvoiceModel({
    this.id,
    this.box,
    this.beneficiary,
    this.amount,
    this.tag,
    this.notes,
    this.createdDate,
    required this.attachments,
    this.edetable,
    this.expense,
  });

  factory InvoiceModel.fromRawJson(String str) =>
      InvoiceModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory InvoiceModel.fromJson(Map<String, dynamic> json) => InvoiceModel(
        id: json["id"],
        box: json["box"],
        beneficiary: json["beneficiary"],
        amount: json["amount"],
        tag: json["tag"],
        notes: json["notes"],
        createdDate: json["createdDate"],
        attachments: json["attachments"] != null ? List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))) : [],
        edetable: json["edetable"],
        expense: json["expense"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "box": box,
        "beneficiary": beneficiary,
        "amount": amount,
        "tag": tag,
        "notes": notes,
        "createdDate": createdDate,
        "attachments": attachments.isNotEmpty ? List<dynamic>.from(attachments.map((x) => x.toJson())) : [],
        "edetable": edetable,
        "expense": expense,
      };
}

class Attachment {
  int id;
  String imagePath;

  Attachment({
    required this.id,
    required this.imagePath,
  });

  factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        imagePath: json["imagePath"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "imagePath": imagePath,
      };
}
