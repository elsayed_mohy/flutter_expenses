// To parse this JSON data, do
//
//     final beneficiaryDetails = beneficiaryDetailsFromJson(jsonString);

import 'dart:convert';

List<BeneficiaryDetails> beneficiaryDetailsFromJson(String str) => List<BeneficiaryDetails>.from(json.decode(str).map((x) => BeneficiaryDetails.fromJson(x)));

String beneficiaryDetailsToJson(List<BeneficiaryDetails> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BeneficiaryDetails {
  int id;
  String name;
  List<Invoice> invoices;

  BeneficiaryDetails({
    required this.id,
    required this.name,
    required this.invoices,
  });

  factory BeneficiaryDetails.fromJson(Map<String, dynamic> json) => BeneficiaryDetails(
    id: json["id"],
    name: json["name"],
    invoices: List<Invoice>.from(json["invoices"].map((x) => Invoice.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "invoices": List<dynamic>.from(invoices.map((x) => x.toJson())),
  };
}

class Invoice {
  int id;
  String beneficiary;
  dynamic amount;
  String tag;
  String notes;
  String createdDate;
  bool edetable;
  bool expense;

  Invoice({
    required this.id,
    required this.beneficiary,
    required this.amount,
    required this.tag,
    required this.notes,
    required this.createdDate,
    required this.edetable,
    required this.expense,
  });

  factory Invoice.fromJson(Map<String, dynamic> json) => Invoice(
    id: json["id"],
    beneficiary: json["beneficiary"],
    amount: json["amount"],
    tag: json["tag"],
    notes: json["notes"],
    createdDate: json["createdDate"],
    edetable: json["edetable"],
    expense: json["expense"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "beneficiary": beneficiary,
    "amount": amount,
    "tag": tag,
    "notes": notes,
    "createdDate": createdDate,
    "edetable": edetable,
    "expense": expense,
  };
}
