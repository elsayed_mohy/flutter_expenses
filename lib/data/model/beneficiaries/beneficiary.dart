// To parse this JSON data, do
//
//     final beneficiary = beneficiaryFromJson(jsonString);

import 'dart:convert';


Beneficiary beneficiaryFromJson(String str) => Beneficiary.fromJson(json.decode(str));

String beneficiaryToJson(Beneficiary data) => json.encode(data.toJson());

List<Beneficiary> beneficiariesFromJson(String str) => List<Beneficiary>.from(json.decode(str).map((x) => Beneficiary.fromJson(x)));

String beneficiariesToJson(List<Beneficiary> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));




class Beneficiary {
  dynamic totalInCome;
  dynamic totalOutCome;
  String beneficiary;

  Beneficiary({
    required this.totalInCome,
    required this.totalOutCome,
    required this.beneficiary,
  });



  factory Beneficiary.fromJson(Map<String, dynamic> json) => Beneficiary(
    totalInCome: json["totalInCome"],
    totalOutCome: json["totalOutCome"],
    beneficiary: json["beneficiary"],
  );

  Map<String, dynamic> toJson() => {
    "totalInCome": totalInCome,
    "totalOutCome": totalOutCome,
    "beneficiary": beneficiary,
  };
}