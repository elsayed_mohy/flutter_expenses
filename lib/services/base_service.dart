import 'dart:io';
import 'package:dio/dio.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import 'package:get_storage/get_storage.dart';

import '../utils/constants/colors.dart';
import 'base_dio.dart';


class BaseService {
  final base = BaseDio();
  Future<Map<String, dynamic>> uploadImage({required File file,required id,required pathId}) async {
    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file":
      await MultipartFile.fromFile(file.path, filename:fileName),
    });
    var response = await base.post(url: 'v1/upload/cloud/file?id=${id}&pathId=${pathId}', requestBody: formData);

    return response.data;
  }

  showInternetError() {
    Get.snackbar(
      "No Internet Connection",
      "",
      icon: Icon(CupertinoIcons.info_circle, color: Colors.white,size: 25,),
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: ColorsManager.darkDisabled,
      borderRadius: 20,
      margin: EdgeInsets.all(15),
      padding: EdgeInsets.fromLTRB(10,20,10,0),
      colorText: Colors.white,
      duration: Duration(seconds: 4),
      isDismissible: true,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }

  void showError(String message) {
    Get.snackbar(
      message,
      '',
      titleText: TextUtils(text: message,color: ColorsManager.white,),
      icon: Icon(CupertinoIcons.info_circle, color: Colors.white,size: 25,),
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: ColorsManager.warning,
      borderRadius: 20,
      margin: EdgeInsets.all(15),
      padding: EdgeInsets.fromLTRB(10,20,10,0),
      colorText: Colors.white,
      duration: Duration(seconds: 4),
      isDismissible: true,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }

  void showSuccess(String message) {
    Get.snackbar(
      message,
      '',
      titleText: TextUtils(text: message,color: ColorsManager.white,),
      icon: Icon(CupertinoIcons.check_mark_circled, color: Colors.white,size: 25,),
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: ColorsManager.success,
      borderRadius: 20,
      margin: EdgeInsets.all(15),
      padding: EdgeInsets.fromLTRB(10,20,10,0),
      colorText: Colors.white,
      duration: Duration(seconds: 4),
      isDismissible: true,
      forwardAnimationCurve: Curves.easeOutBack,
    );
  }

  String getDeviceLanguage() {
    return WidgetsBinding.instance.platformDispatcher.locale.languageCode;
  }
  String getCurrentLanguage() {
    return GetStorage().read<String>('lang') !=null ? GetStorage().read<String>('lang').toString() : getDeviceLanguage();
  }


}