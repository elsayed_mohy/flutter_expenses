import 'dart:io';

import 'package:dio/dio.dart';
import 'package:expenses/services/base_service.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;
import '../data/model/user.dart';

class BaseDio {
  dynamic userAgent;

  static final dio = Dio(
    BaseOptions(baseUrl: AppUrls.apiUrl, receiveDataWhenStatusError: true),
  );

  static void initializeInterceptors() {
    final GetStorage storage = GetStorage();
    String deviceLanguage = BaseService().getDeviceLanguage();
    String lang = GetStorage().read<String>('lang') !=null ? GetStorage().read<String>('lang').toString() : deviceLanguage;
    lang ??= 'en';

    bool _isServerDown(DioException error) {
      return (error.error is SocketException) || (error.type == DioExceptionType.connectionTimeout);
    }

    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (request, handler) async {
        User user = userFromJson(GetStorage().read("user"));
        var headers = {
          'Accept': "application/json",
          'Authorization': 'Bearer ${user.accessToken}',
          'accept-language':'${lang}'
        };
        request.headers.addAll(headers);
        print(request.headers);
        return handler.next(request);
      },
      onResponse: (response, handler) {
        return handler.next(response);
      },
      onError: (error, handler) async {
        if (_isServerDown(error)) {
          Response? response;
          try {
            final result = await InternetAddress.lookup('www.google.com');
            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
              Get.snackbar(
                "Internet Connection Restored",
                "",
                icon: Icon(CupertinoIcons.check_mark_circled, color: Colors.green,size: 25,),
                snackPosition: SnackPosition.BOTTOM,
                backgroundColor: ColorsManager.darkDisabled,
                borderRadius: 20,
                margin: EdgeInsets.all(15),
                padding: EdgeInsets.fromLTRB(10,20,10,0),
                colorText: Colors.white,
                duration: Duration(seconds: 4),
                isDismissible: true,
                forwardAnimationCurve: Curves.easeOutBack,
              );
            }
          } on SocketException catch (_) {
            BaseService().showInternetError();
            handler.next(error);
            return;
          }
        }
        if (error.response?.statusCode == 401) {
          User user = userFromJson(GetStorage().read("user"));
          if (user.refreshToken != null) {
            Response<dynamic> response = await dio
                .post("${AppUrls.apiUrl}v1/auth/refresh-access-token", data: {
              'refreshToken': user.refreshToken,
            });
            if (response.statusCode == 200) {
              if (response.data['data'] != null) {
                // await UserPreferences.updateAccessToken(response.data['data']['jwt']);
                RequestOptions options = error.requestOptions;
                try {
                  var resp = await dio.request(error.requestOptions.path,
                      data: options.data,
                      cancelToken: options.cancelToken,
                      onReceiveProgress: options.onReceiveProgress,
                      onSendProgress: options.onSendProgress,
                      queryParameters: options.queryParameters);
                  handler.resolve(resp);
                } on DioError catch (error) {
                  handler.reject(error);
                }
              } else {
                print("Error refreshing token: ${response.statusCode}");
                //logout();
                // signIn
                handler.reject(error);
              }
            } else {
              print("Error refreshing token: ${response.statusCode}");
              // logout();
              // signIn
              handler.reject(error);
            }
          } else {
            handler.reject(error);
          }
        } else {
          print("dio err ${error.response?.data}");
          BaseService().showError(error.response?.data['message']);
          // snackBarKey.currentState?..hideCurrentSnackBar()..showSnackBar(errorSnackBar(
          //     message: error.response?.data['message']));
          handler.reject(error);
        }
      },
    ));
  }

  Future<Response> get({
    required String url,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.get(url,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          },
        ));
  }


  Future<Response> post({
    required String url,
    required Object requestBody,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.post(url,
        data: requestBody,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          },
        ));
  }

  Future<Response> put({
    required String url,
    required Object requestBody,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.put(url,
        data: requestBody,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          },
        ));
  }

  Future<Response> delete({
    required String url,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.delete(url,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          },
        ));
  }

  Future<Response> downLoad({
    required String url,
    required Object requestBody,
    Map<String, dynamic>? queryParameters,
  }) async {
    return await dio.post(url,
        data: requestBody,
        queryParameters: queryParameters,
        options: Options(
            method: 'post',
            responseType: ResponseType.bytes,
            followRedirects: false));
  }
}
