import 'dart:convert';
import 'dart:io';

import 'package:expenses/data/model/beneficiaries/beneficiary.dart';
import 'package:expenses/data/model/beneficiaries/beneficiary_details.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:open_app_file/open_app_file.dart';
import 'package:path_provider/path_provider.dart';

class BeneficiaryService {
  final base = BaseDio();

  Future<List<Beneficiary>> beneficiaryList() async {
    final response = await base.get(url: AppUrls.beneficiaryListEndPoint);
    if (response.statusCode == 200) {
      var data = response.data['data'] as List;
      return data.map((e) => Beneficiary.fromJson(e)).toList();
    } else {
      return throw Exception("error while loading beneficiaries ");
    }
  }

  Future<List<BeneficiaryDetails>> beneficiaryDetails(String name) async {
    final response =
        await base.get(url: '${AppUrls.beneficiaryDetailsEndPoint}${name}');


    if (response.statusCode == 200) {
      var data = response.data['data'] as List;
      return data.map((e) => BeneficiaryDetails.fromJson(e)).toList();
    } else {
      return throw Exception("error while loading beneficiary Details ");
    }
  }




  Future<String> downFile(String name,String filename,dynamic ids) async {
    String dir = (await getTemporaryDirectory()).path;
    File targetFile = File('$dir/$filename');
    // if (kIsWeb) {
    //   // on the web there's no reason to download the file
    //   return url;
    // }

    Map<String, dynamic> requestBody = {
      "reportName": "account_summary_${AppLocalizations.supportedLocales.first}",
      "fileName": "account_summary_${AppLocalizations.supportedLocales.first}",
      "paramters": {
        "beneficiary": name,
        "boxId": ids,
        "currentDate": DateFormat('yyyy-MM-dd hh:mm:ss a').format(DateTime.now())
      }
    };
    final response = await base.downLoad(
        url: '${AppUrls.reportEndPoint}', requestBody: jsonEncode(requestBody));
    if (response.statusCode == 200) {

    } else {
      return throw Exception("error while loading beneficiary Details ");
    }


    await targetFile.writeAsBytes(response.data);
    return targetFile.path;
  }

  Future<void> openFile(String filePath) async {
    final result = await OpenAppFile.open(filePath);
  }

}
