import 'package:expenses/data/model/dasboard/chart_category.dart';
import 'package:expenses/data/model/dasboard/fund_number.dart';
import 'package:expenses/data/model/dasboard/last_modified.dart';
import 'package:expenses/data/model/dasboard/max_beneficiary.dart';
import 'package:expenses/data/model/dasboard/max_box.dart';
import 'package:expenses/data/model/dasboard/user_summary.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/utils/constants/app_urls.dart';

class DashboardService {
  final base = BaseDio();

  Future<List<ChartCategory>> categoriesChart(bool isExpense) async {
    final response = await base.get(url: AppUrls.categoryChartEndPoint,
        queryParameters: {'isExpense': isExpense});
    if (response.statusCode == 200) {
      var data = response.data as List;
      return data.map((e) => ChartCategory.fromJson(e)).toList();
    } else {
      return throw Exception("error while loading chart categories ");
    }
  }

  Future<List<LastModified>> lastModifiedList() async {
    final response = await base.get(url: AppUrls.lastModifiedEndPoint);
    if (response.statusCode == 200) {
      var data = response.data as List;
      return data.map((e) => LastModified.fromJson(e)).toList();
    } else {
      return throw Exception("error while loading invoices ");
    }
  }


  Future<MaxBeneficiary> maxBeneficiary(bool isExpense) async {
    final response = await base.get(url: AppUrls.maxBeneficiaryEndPoint,
        queryParameters: {'isExpense': isExpense});
    if (response.statusCode == 200) {
      return  MaxBeneficiary.fromJson(response.data);
    } else {
      return throw Exception("error while loading max beneficiary");
    }
  }

  Future<MaxBox> maxBox(bool isExpense) async {
    final response = await base.get(url: AppUrls.maxBoxEndPoint,
        queryParameters: {'isExpense': isExpense});
    if (response.statusCode == 200) {
      return  MaxBox.fromJson(response.data);
    } else {
      return throw Exception("error while loading max box");
    }
  }

  Future<FundNumber> memberNumber() async {
    final response = await base.get(url: AppUrls.memberNumberEndPoint);
    if (response.statusCode == 200) {
      return  FundNumber.fromJson(response.data);
    } else {
      return throw Exception("error while loading member number");
    }
  }

  Future<FundNumber> boxNumber() async {
    final response = await base.get(url: AppUrls.boxNumberEndPoint);
    if (response.statusCode == 200) {
      return  FundNumber.fromJson(response.data);
    } else {
      return throw Exception("error while loading box number");
    }
  }

  Future<UserSummary> userSummary() async {
    final response = await base.get(url: AppUrls.userSummaryEndPoint);
    if (response.statusCode == 200) {
      return  UserSummary.fromJson(response.data);
    } else {
      return throw Exception("error while loading user summary");
    }
  }

  Future<List<MaxBeneficiary>> beneficiaryChart(bool isExpense) async {
    final response = await base.get(url: AppUrls.beneficiaryChartEndPoint,
        queryParameters: {'isExpense': isExpense});
    if (response.statusCode == 200) {
      var data = response.data as List;
      return data.map((e) => MaxBeneficiary.fromJson(e)).toList();
    } else {
      return throw Exception("error while loading chart beneficiary ");
    }
  }

  Future<List<MaxBox>> boxChart(bool isExpense) async {
    final response = await base.get(url: AppUrls.boxChartEndPoint,
        queryParameters: {'isExpense': isExpense});
    if (response.statusCode == 200) {
      var data = response.data as List;
      return data.map((e) => MaxBox.fromJson(e)).toList();
    } else {
      return throw Exception("error while loading chart box ");
    }
  }


}
