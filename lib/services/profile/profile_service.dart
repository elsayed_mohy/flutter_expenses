import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:flutter/cupertino.dart';
import 'package:get_storage/get_storage.dart';

import '../../data/model/user.dart';

class ProfileService  {
  final base = BaseDio();
  User user = userFromJson(GetStorage().read("user"));
  Future<Response> updateProfile(fullName,phoneNumber) async{
    Response response = await base.put(url: AppUrls.updateProfileEndPoint, requestBody: {
      "fullName":fullName,
      "phoneNumber":phoneNumber,
      "id":user.id,
      "imagePath":user.imagePath,
      "preferLanguage":user.preferLanguage,
    });
    return response;
  }


  Future<Response> changeUserPassword(oldPassword,newPassword,confirmPassword) async{
    Response response = await base.put(url: AppUrls.changeUserPasswordEndPoint, requestBody:
        <String,dynamic> {
        "userId": user.id,
        "oldPassword":oldPassword,
        "newPassword":newPassword,
        });
    print(response);
    return response;
  }
}
