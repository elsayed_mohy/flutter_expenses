import 'package:dio/dio.dart';
import 'package:expenses/data/model/beneficiaries/beneficiary.dart';
import 'package:expenses/data/model/box_model.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:expenses/view/widgets/funds/box.dart';

import '../../data/model/invitation_model.dart';

class InvitationService {
  final base = BaseDio();

  Future<List<Invitation>> getSentInvitations() async{
    Response response = await base.get(url: AppUrls.sentInvitationsEndPoint);
    var sentInvitations =  response.data['data'] as List;
    return sentInvitations.map((invitation)=> Invitation.fromJson(invitation)).toList();
  }
  Future<List<Invitation>> getReceivedInvitations() async{
    Response response = await base.get(url: AppUrls.receivedInvitationsEndPoint);
    var receivedInvitations =  response.data['data'] as List;
    return receivedInvitations.map((invitation)=> Invitation.fromJson(invitation)).toList();
  }

  void sendInvitation(requestBody) async{
    Response response = await base.post(url: AppUrls.sendInvitationEndPoint,requestBody:requestBody);
  }

  Future<Response> acceptInvitation(invitationId) async{
    Response response = await base.put(url: AppUrls.acceptInvitationEndPoint + invitationId,requestBody:{});
    return response;
  }
  Future<Response> rejectInvitation(invitationId) async{
    Response response = await base.put(url: AppUrls.rejectInvitationEndPoint + invitationId,requestBody:{});
    return response;
  }
  Future<Response> cancelInvitation(invitationId) async{
    Response response = await base.put(url: AppUrls.cancelInvitationEndPoint + invitationId,requestBody:{});
    return response;
  }
}
