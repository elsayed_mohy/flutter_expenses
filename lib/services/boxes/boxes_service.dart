import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:expenses/data/model/beneficiaries/beneficiary.dart';
import 'package:expenses/data/model/box/box_details_model.dart';
import 'package:expenses/data/model/box_model.dart';
import 'package:expenses/data/model/invoice/invoice_model.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:expenses/view/widgets/funds/box.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart';
import 'package:open_app_file/open_app_file.dart';
import 'package:path_provider/path_provider.dart';

import '../base_service.dart';

class BoxesService {
  final base = BaseDio();

  Future<List<BoxModel>> getBoxes() async{
    Response response = await base.post(url: AppUrls.boxesEndPoint, requestBody: {});
    var boxes =  response.data['data']['content'] as List;
    print(boxes);
    return boxes.map((box)=> BoxModel.fromJson(box)).toList();
  }
  Future<List<BoxModel>> getSubscriptionBoxes() async{
    Response response = await base.post(url: AppUrls.subscriptionBoxesEndPoint, requestBody: {});
    var boxes =  response.data['data']['content'] as List;
    return boxes.map((box)=> BoxModel.fromJson(box)).toList();
  }

  Future addBox(requestBody) async{
    Response response = await base.post(url: AppUrls.addEditBoxEndPoint, requestBody: requestBody);
    return response.data;
  }
  Future editBox(requestBody) async{
    Response response = await base.put(url: AppUrls.addEditBoxEndPoint, requestBody: requestBody);
    return response.data;
  }

  Future<List<Member>> getBoxMembers(boxId , {String? name}) async{
    Response response = await base.get(url: AppUrls.boxMembersEndPoint,queryParameters: {"boxId":boxId});
    print(response.data);
    var members =  response.data['data'] as List;
    return members.map((member)=> Member.fromJson(member)).toList();
  }

  Future toggleMemberType(memberId , boxId) async{
    Response response = await base.put(url: AppUrls.changeMemberTypeEndPoint,requestBody: {} ,queryParameters:
        {
          "memberId":memberId,
          "boxId":boxId
        }
    );
    print("response.data ${response.data}");
    print("response.requestOptions ${response.requestOptions.data}");
    return response.data;
  }

  Future sendInvitation(queryParameters) async{
    Response response = await base.post(url: AppUrls.sendInvitationEndPoint,queryParameters:queryParameters, requestBody: {});
  return response.data;
  }

  Future deleteBox(boxId) async{
    Response response = await base.delete(url: AppUrls.deleteBoxEndPoint,queryParameters: {"id":boxId});
    return response.data;
  }



  Future<BoxDetails> getBoxDetails(boxId) async{
    Response response = await base.get(url: "${AppUrls.boxDetailsEndPoint}${boxId}");
    var boxDetails =  response.data['data'];
    return BoxDetails.fromJson(boxDetails);
  }
  Future<List<InvoiceModel>> filterInvoices(requestBody, editable) async{
    Response response = await base.post(url:editable ?  AppUrls.invoicesFilterEndPoint : AppUrls.subInvoicesFilterEndPoint,requestBody: jsonEncode(requestBody));
    var invoices =  response.data['data']['content'] as List;
    return invoices.map((invoice)=> InvoiceModel.fromJson(invoice)).toList();
  }

  Future<List<String>> getInvoiceBeneficiary() async {
    final response = await base.get(url: AppUrls.invoiceBeneficiaryEndPoint);
    if (response.statusCode == 200) {
      var list =  response.data['data'] as List ;
      return list.map((e) => e.toString()).toList();
    } else {
      return throw Exception("error while loading invoice beneficiary");
    }
  }

  Future<List<String>> getInvoiceTag() async{
    Response response = await base.get(url: AppUrls.invoiceTagEndPoint);
    if (response.statusCode == 200) {
      var list =  response.data['data']['content'] as List ;
      return list.map((e) => e.toString()).toList();
    } else {
      return throw Exception("error while loading invoice tag");
    }
  }

  Future createInvoice(requestBody) async{
    Response response = await base.post(url: AppUrls.invoiceEndPoint,requestBody:requestBody);
    if(response.statusCode == 200){
      return response.data;
    }else{
      return throw Exception("error while creating invoice");
    }
  }

  Future updateInvoice(requestBody) async{
    Response response = await base.put(url: AppUrls.invoiceEndPoint,requestBody:requestBody);
    if(response.statusCode == 200){
    }else{
      return throw Exception("error while updating invoice");
    }
  }

  //

  Future<InvoiceModel> getInvoiceDetails(invoiceId) async{
    Response response = await base.get(url: '${AppUrls.invoiceEndPoint}/${invoiceId}');
    if(response.statusCode == 200){
      var invoiceDetails =  response.data['data'];
      return InvoiceModel.fromJson(invoiceDetails);
    }else{
      return throw Exception("error while loading invoice");
    }
  }

  Future deleteInvoice(invoiceId) async{
    Response response = await base.delete(url: AppUrls.deleteInvoiceEndPoint,queryParameters: {'id':invoiceId});
    return response.data;
  }

  Future deleteInvoiceAttach(AttachId) async{
    Response response = await base.delete(url: AppUrls.deleteInvoiceAttachEndPoint,queryParameters: {'id':AttachId,'pathId':'IMAGE_INVOICE_ATTACHMENT'});
    return response.data;
  }

  Future changeInvoiceType(invoiceId) async{
    Response response = await base.put(url: '${AppUrls.changeInvoiceTypeEndPoint}/${invoiceId}',requestBody:{});
    return response.data;
  }

  Future<String> downFile(String filename,dynamic boxId) async {
    String dir = (await getTemporaryDirectory()).path;
    File targetFile = File('$dir/$filename');
    // if (kIsWeb) {
    //   // on the web there's no reason to download the file
    //   return url;
    // }

    Map<String, dynamic> requestBody = {
      "reportName": "box_summary_${AppLocalizations.supportedLocales.first}",
      "fileName": "box_summary_${AppLocalizations.supportedLocales.first}",
      "paramters": {
        "boxId": boxId,
        "currentDate": DateFormat('yyyy-MM-dd hh:mm:ss a').format(DateTime.now())
      }
    };
    final response = await base.downLoad(
        url: '${AppUrls.reportEndPoint}', requestBody: jsonEncode(requestBody));
    if (response.statusCode == 200) {

    } else {
      return throw Exception("error while download box pdf ");
    }


    await targetFile.writeAsBytes(response.data);
    return targetFile.path;
  }

  Future<void> openFile(String filePath) async {
    final result = await OpenAppFile.open(filePath);
    print(result.message);
  }

  Future<void> saveImageToGallery(String imageUrl) async {
    AppLocalizations translate = await AppLocalizations.delegate.load(Locale(BaseService().getCurrentLanguage()));

    try {
      Dio dio = Dio();
      Response response = await dio.get(imageUrl, options: Options(responseType: ResponseType.bytes));
      Uint8List bytes = Uint8List.fromList(response.data);

      final result = await ImageGallerySaver.saveImage(bytes);

      if (result['isSuccess']) {
        // Image saved successfully
        BaseService().showSuccess(translate.savedImage);
        print('Image saved to gallery');
      } else {
        // An error occurred while saving the image
        print('Failed to save the image: ${result['errorMessage']}');
      }
    } catch (e) {
      // Handle any exceptions
      print('Error: $e');
    }
  }

}
