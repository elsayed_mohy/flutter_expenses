import '../theme.dart';
import 'colors.dart';

class UIContestants {
  static const darkWaveSvgPath =  "assets/images/ui/dark_vec_back.svg" ;
  static const lightWaveSvgPath =  "assets/images/ui/light_vec_back.svg";
}