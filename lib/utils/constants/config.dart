import 'package:expenses/services/base_service.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class Config {
  static formatDate(dateString) {
  String deviceLanguage = BaseService().getDeviceLanguage();
    String lang = GetStorage().read<String>('lang') !=null ? GetStorage().read<String>('lang').toString() : deviceLanguage;
    lang ??= 'en';
    DateTime date = DateFormat('E, dd MMM yyyy hh:mm:ss a','en_US').parse(dateString);
    if(lang == 'ar'){
      String formattedDate = DateFormat('yy/M/d , hh:mm a','ar_SA').format(date);
      return formattedDate;
    }
    if(lang == 'en'){
      String formattedDate = DateFormat('M/d/yy , hh:mm a','en_US').format(date);
      return formattedDate;
    }


  }
}
