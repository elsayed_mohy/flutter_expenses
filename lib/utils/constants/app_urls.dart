class AppUrls {
  // base prod
  //static const apiUrl = 'https://fund-expenses.com/backend/';
  // base develope
  static const apiUrl = 'http://44.195.14.16:5050/backend/';

  // baseImageUrl
  static const baseImageUrl =
      'https://dev-fund.s3.amazonaws.com/';
// **** AUTH MODULE ****
  static const logInEndPoint = '${apiUrl}v1/auth/login';
  static const registerEndPoint = '${apiUrl}v1/auth/register';
  static const logoutEndPoint = '${apiUrl}v1/auth/logout';
  static const refreshTokenEndPoint = '${apiUrl}v1/auth/refresh-access-token';
  static const forgetEndPoint = '${apiUrl}v1/auth/send-reset-link';
  static const checkResetEndPoint = '${apiUrl}v1/auth/check-reset-link';
  static const resetPasswordEndPoint = '${apiUrl}v1/auth/change-password';
  static const sendOTPEndPoint = '${apiUrl}v1/auth/send-otp';
  static const validateOTPEndPoint = '${apiUrl}v1/auth/validate-otp';
  static const countriesEndPoint = '${apiUrl}v1/country';

// **** BOX MODULE ****

  static const boxesEndPoint = '${apiUrl}v1/box/filter';
  static const subscriptionBoxesEndPoint = '${apiUrl}v1/box/subscription/filter';
  static const boxDetailsEndPoint = '${apiUrl}v1/box/';
  static const addEditBoxEndPoint = '${apiUrl}v1/box';
  static const deleteBoxEndPoint = '${apiUrl}v1/box';
  static const boxMembersEndPoint = '${apiUrl}v1/box/member/list';
  static const changeMemberTypeEndPoint = '${apiUrl}v1/box/member/change/type';


  // **** INVOICE MODULE ****

  static const invoicesFilterEndPoint = '${apiUrl}v1/invoice/filter';
  static const subInvoicesFilterEndPoint = '${apiUrl}v1/invoice/subscription/filter';
  static const invoiceBeneficiaryEndPoint = '${apiUrl}v1/invoice/beneficiary';
  static const invoiceTagEndPoint = '${apiUrl}v1/invoice/tag';
  static const invoiceEndPoint = '${apiUrl}v1/invoice';
  static const changeInvoiceTypeEndPoint = '${apiUrl}v1/invoice/type';
  static const deleteInvoiceEndPoint = '${apiUrl}v1/invoice?';
  static const deleteInvoiceAttachEndPoint = '${apiUrl}v1/upload/delete';


  // **** beneficiary MODULE ****
  static const beneficiaryListEndPoint = '${apiUrl}v1/invoice/beneficiary-list';
  static const beneficiaryDetailsEndPoint = '${apiUrl}v1/invoice/mobile/beneficiary/name/';

  //  **** dashboard MODULE ****
  static const boxNumberEndPoint = '${apiUrl}v1/dashboard/box-number';
  static const userSummaryEndPoint = '${apiUrl}v1/dashboard/user-summery';
  static const memberNumberEndPoint = '${apiUrl}v1/dashboard/member-number';
  static const lastModifiedEndPoint = '${apiUrl}v1/dashboard/last-modified';

  // queryParameters
  static const categoryChartEndPoint = '${apiUrl}v1/dashboard/chart-category?';
  static const boxChartEndPoint = '${apiUrl}v1/dashboard/chart-box?';
  static const beneficiaryChartEndPoint = '${apiUrl}v1/dashboard/chart-beneficiary?';
  static const maxBeneficiaryEndPoint = '${apiUrl}v1/dashboard/max-beneficiary?';
  static const maxBoxEndPoint = '${apiUrl}v1/dashboard/max-box?';



  //  **** profile MODULE ****
  static const updateProfileEndPoint= '${apiUrl}v1/user/user-profile';
  static const changeUserPasswordEndPoint = '${apiUrl}v1/user/change-my-password';

  //  **** invitations MODULE ****
  static const sentInvitationsEndPoint= '${apiUrl}v1/invitation/sent';
  static const receivedInvitationsEndPoint = '${apiUrl}v1/invitation/received';
  static const sendInvitationEndPoint= '${apiUrl}v1/invitation/send?';
  static const acceptInvitationEndPoint= '${apiUrl}v1/invitation/accept/';
  static const rejectInvitationEndPoint= '${apiUrl}v1/invitation/reject/';
  static const cancelInvitationEndPoint= '${apiUrl}v1/invitation/cancel/';
  static const invitationsCountEndPoint= '${apiUrl}v1/invitation/count-by-phoneNumber';


  // api/v1/report/pdf
  static const reportEndPoint= '${apiUrl}api/v1/report/pdf';
}
