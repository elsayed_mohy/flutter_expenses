import 'app_localizations.dart';

/// The translations for Arabic (`ar`).
class AppLocalizationsAr extends AppLocalizations {
  AppLocalizationsAr([String locale = 'ar']) : super(locale);

  @override
  String get phoneKey => 'أدخل  هاتفك';

  @override
  String get passwordKey => 'أدخل كلمة المرور';

  @override
  String get myFunds => 'الصناديق';

  @override
  String get profile => 'الملف الشخصي';

  @override
  String get dashboard => 'لوحة التحكم';

  @override
  String get settings => 'الإعدادات';

  @override
  String get beneficiaries => 'المستفيدين';

  @override
  String get invitations => 'الدعوات';

  @override
  String get boxNumber => 'عدد الصناديق';

  @override
  String get memberNumber => 'عدد الاعضاء';

  @override
  String get userSummary => 'ملخص حسابك';

  @override
  String get topBox => 'اعلي صندوق';

  @override
  String get topCategory => 'اعلي فئه';

  @override
  String get topBeneficiary => 'اعلي مستفيد';

  @override
  String get beneficiaryName => 'اسم المستفيد';

  @override
  String get boxName => 'اسم الصندوق';

  @override
  String get lastInvoices => 'اخر فواتير';

  @override
  String get analytics => 'الاحصائيات';

  @override
  String get income => 'دخل';

  @override
  String get expense => 'مصروفات';

  @override
  String get sent => 'الدعوات المرسلة';

  @override
  String get noSent => 'لا توجد دعوات مرسلة';

  @override
  String get received => 'الدعوات المستلمة';

  @override
  String get noReceived => 'لا توجد دعوات  مستلمة';

  @override
  String get invitationMessage => ' دعاك للإنضمام إلى صندوق المصروفات ';

  @override
  String get accept => 'قبول';

  @override
  String get reject => 'رفض';

  @override
  String get edit => 'تعديل';

  @override
  String get remove => 'حذف';

  @override
  String get cancel => 'إلغاء';

  @override
  String get notAvailableNow => 'الخدمة غير متوفرة الأن';

  @override
  String get subscriber => 'مشترك';

  @override
  String get admin => 'مشرف';

  @override
  String get subscriptions => 'الإشتراكات';

  @override
  String get report => 'كشف حساب';

  @override
  String get addFund => 'إضافة صندوق';

  @override
  String get editFund => 'تعديل الصندوق';

  @override
  String get fundName => 'إسم الصندوق';

  @override
  String get addMember => 'أضف عضو';

  @override
  String get addMemberPlaceholder => 'اكتب ما لا يقل عن 5 أرقام من رقم العضو';

  @override
  String get addBeneficiary => 'أضف مستفيد';

  @override
  String get currentBalance => 'الرصيد الحالي';

  @override
  String get searchFund => 'إبحث بإسم الصندوق';

  @override
  String get searchBeneficiary => 'إبحث بإسم المستفيد';

  @override
  String get members => 'الأعضاء';

  @override
  String get noFunds => 'لا توجد صناديق';

  @override
  String get noFundsFound => 'لم يتم العثور على صناديق';

  @override
  String get invoices => 'الفواتير';

  @override
  String get addInvoice => 'إضافة فاتورة';

  @override
  String get invoiceName => 'اسم الفاتورة';

  @override
  String get editInvoice => 'تعديل فاتورة';

  @override
  String get searchInvoice => 'إبحث بإسم الفاتورة';

  @override
  String get totalIncome => 'الدخل الكلى';

  @override
  String get totalExpense => 'المصروف الكلى';

  @override
  String get amount => 'القيمة';

  @override
  String get tag => 'الفئه';

  @override
  String get description => 'الوصف';

  @override
  String get deleteBox => 'حذف الصندوق';

  @override
  String get deleteInvoice => 'حذف الفاتورة';

  @override
  String get deleteBoxMsg => 'هل أنت متأكد أنك تريد حذف هذا الصندوق؟';

  @override
  String get deleteInvoiceMsg => 'هل أنت متأكد أنك تريد حذف هذه الفاتورة؟';

  @override
  String get descriptionRequired => 'الوصف مطلوب';

  @override
  String get tagRequired => 'الفئه مطلوبة';

  @override
  String get amountRequired => 'القيمة مطلوبة';

  @override
  String get beneficiaryRequired => 'المستفيد مطلوب';

  @override
  String get editProfile => 'تعديل البروفايل';

  @override
  String get notifications => 'الإشعارات';

  @override
  String get darkMode => 'الوضع الليلي';

  @override
  String get save => 'حفظ';

  @override
  String get signIn => 'دخول';

  @override
  String get loginToAccount => 'تسجيل الدخول';

  @override
  String get signUp => 'إنشاء حساب';

  @override
  String get createNewAccount => 'انشاء حساب';

  @override
  String get resetPassword => 'إعادة تعيين كلمة المرور';

  @override
  String get resetPasswordCode => 'أدخل الرمز لإعادة تعيين كلمة المرور الخاصة بك';

  @override
  String get changePassword => 'تغيير كلمة المرور';

  @override
  String get change => 'تغيير';

  @override
  String get changePasswordSuccess => 'تم تغيير كلمة المرور';

  @override
  String get username => 'اسم المستخدم';

  @override
  String get password => 'كلمة المرور';

  @override
  String get confirmPassword => 'تأكيد كلمة المرور';

  @override
  String get email => 'البريد الإلكتروني';

  @override
  String get code => 'الرمز';

  @override
  String get codeNotValid => 'رمز إعادة التعيين غير صالح';

  @override
  String get phone => 'رقم الهاتف';

  @override
  String get forgotPassword => 'نسيت كلمة المرور ؟';

  @override
  String get changeLanguage => 'تغيير اللغة';

  @override
  String get incorrectData => 'البيانات التى أدخلتها غير صحيحة';

  @override
  String get typeToReset => 'يرجى كتابة رقم الهاتف لإعادة تعيين كلمة المرور الخاصة بك';

  @override
  String get noAccount => 'ليس لديك حساب ؟';

  @override
  String get haveAccount => 'لديك حساب ؟';

  @override
  String get registerNow => 'سجل الان';

  @override
  String get verifyMobile => 'تأكيد الهاتف';

  @override
  String get verify => 'تأكيد';

  @override
  String get notReceived => 'لم تتلق الرمز؟';

  @override
  String get mobileVerificationTitle => 'تأكيد الهاتف';

  @override
  String get mobileVerificationMsg => 'تم إرسال رسالة تحتوي على رمز تنشيط إلى هاتفك المحمول';

  @override
  String get register => 'إنشاء حساب';

  @override
  String get terms => 'الشروط و الأحكام';

  @override
  String get logout => 'تسجيل الخروج';

  @override
  String get logoutConfirm => 'هل أنت متأكد أنك تريد تسجيل الخروج ؟';

  @override
  String get currentPassword => 'كلمة المرور الحالية';

  @override
  String get newPassword => 'كلمة المرور الجديدة';

  @override
  String get confirmNewPassword => 'تأكيد كلمة المرور الجديدة';

  @override
  String get sendResetCode => 'أرسل كود إعادة التعيين';

  @override
  String get successSentResetCode => 'تم إرسال الكود بنجاح, تفحص الرسائل القصيرة';

  @override
  String get invoiceType => 'نوع الفاتورة';

  @override
  String get value => 'القيمة';

  @override
  String get addIncome => 'أضف دخل';

  @override
  String get addExpense => 'أضف مصروف';

  @override
  String get send => 'أرسل';

  @override
  String get contacts => 'جهات الإتصال';

  @override
  String get download => 'تحميل';

  @override
  String get shareToWhatsapp => 'مشاركه لوتساب';

  @override
  String get view => 'عرض';

  @override
  String get approved => 'موافقة';

  @override
  String get rejected => 'مرفوض';

  @override
  String get canceled => 'ألغيت';

  @override
  String get pending => 'قيد الانتظار';

  @override
  String youInvitedMessage(Object boxName, Object username) {
    return 'قمت بدعوة $username للإنضمام للصندوق $boxName';
  }

  @override
  String invitedYouMessage(Object boxName, Object username) {
    return 'قام $username بدعوتك للإنضمام للصندوق $boxName';
  }

  @override
  String get onBoarding_1 => 'لا مزيد من فقدان الإيصالات';

  @override
  String get onBoarding_1_sub => 'قم بتحميل كافة الإيصالات مباشرةً في سجل النفقات.';

  @override
  String get onBoarding_1_desc => 'بإمكانك إرفاق نسخ من إيصالات الشراء أو الدفع مباشرةً في سجل المصروفات لتجنب فقدانها. أضف المرفقات بسرعة من أي هاتف عن طريق التقاط صورة للإيصال  وحفظها فى صندوق نفقات محدد . وفر الوقت وعزز الكفاءة من خلال حفظ سجل منظم وكامل لكافة المصروفات.';

  @override
  String get onBoarding_2 => 'قم بإدارة النفقات لكل صندوق ولكل مستفيد على حده';

  @override
  String get onBoarding_2_desc => 'احصل على نظرة عامة واضحة عن نفقات الصندوق أو حتى مستفيد معين من هذه النفقات مثل تاجر معين تتعامل معه أو عامل يقدم لك خدمة أو شركات الإتصالات حتى و مصروفاتك معها . قم بإدارة مهامك كمسئول بمتابعة سجلات المصروفات بسهولة داخل أعضاء الصندوق لمراقبة التكاليف وضمان السير نحو تحقيق الهدف وفي إطار الميزانية المتاحة.';

  @override
  String get onBoarding_3 => 'قم بإدارة النفقات  بالمشاركة والمناقشة حول نفقات الصندوق';

  @override
  String get onBoarding_3_desc => 'وذلك من خلال الدردشة الحية والفورية داخل هذا الصندوق بين جميع أعضاءة , حيث يمكن للجميع النقاش حلو بند معين من المصروفات وذلك فى نفس التطبيق لست مضطر لإنشاء مجموعات الدردشة فى برامج التواصل الاجتماعى للنقاش حلو الماليات الخاصة بصندوق  إتحاد ملاك العمارة مثلا لان فى هذا التطبيق سوف تجد كل هذه المميزات فى مكان واحد .';

  @override
  String get skip => 'تخطي';

  @override
  String get next => 'التالي';

  @override
  String get finish => 'إنهاء';

  @override
  String get search => 'ابحث';

  @override
  String get imageSaved => 'تم حفظ الصورة في المعرض';

  @override
  String get noMembers => 'لا يوجد أعضاء';

  @override
  String get noBeneficiaries => 'لا يوجد مستفيدين';

  @override
  String get noBoxes => 'لا توجد صناديق';

  @override
  String get invoiceImages => 'صور الفاتورة';

  @override
  String get more => 'المزيد';

  @override
  String get passwordChanged => 'تم تغيير كلمة المرور بنجاح';

  @override
  String get savedImage => 'تم حفظ الصورة في المعرض';
}
