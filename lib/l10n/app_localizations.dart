import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_ar.dart';
import 'app_localizations_en.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'l10n/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('ar'),
    Locale('en')
  ];

  /// No description provided for @phoneKey.
  ///
  /// In en, this message translates to:
  /// **'Enter your phone'**
  String get phoneKey;

  /// No description provided for @passwordKey.
  ///
  /// In en, this message translates to:
  /// **'Enter your password'**
  String get passwordKey;

  /// No description provided for @myFunds.
  ///
  /// In en, this message translates to:
  /// **'My Funds'**
  String get myFunds;

  /// No description provided for @profile.
  ///
  /// In en, this message translates to:
  /// **'Profile'**
  String get profile;

  /// No description provided for @dashboard.
  ///
  /// In en, this message translates to:
  /// **'Dashboard'**
  String get dashboard;

  /// No description provided for @settings.
  ///
  /// In en, this message translates to:
  /// **'Settings'**
  String get settings;

  /// No description provided for @beneficiaries.
  ///
  /// In en, this message translates to:
  /// **'Beneficiaries'**
  String get beneficiaries;

  /// No description provided for @invitations.
  ///
  /// In en, this message translates to:
  /// **'Invitations'**
  String get invitations;

  /// No description provided for @boxNumber.
  ///
  /// In en, this message translates to:
  /// **'Boxes Number'**
  String get boxNumber;

  /// No description provided for @memberNumber.
  ///
  /// In en, this message translates to:
  /// **'Members Number'**
  String get memberNumber;

  /// No description provided for @userSummary.
  ///
  /// In en, this message translates to:
  /// **'Account Summary'**
  String get userSummary;

  /// No description provided for @topBox.
  ///
  /// In en, this message translates to:
  /// **'Top Box'**
  String get topBox;

  /// No description provided for @topCategory.
  ///
  /// In en, this message translates to:
  /// **'Top Category'**
  String get topCategory;

  /// No description provided for @topBeneficiary.
  ///
  /// In en, this message translates to:
  /// **'Top Beneficiary'**
  String get topBeneficiary;

  /// No description provided for @beneficiaryName.
  ///
  /// In en, this message translates to:
  /// **'Beneficiary Name'**
  String get beneficiaryName;

  /// No description provided for @boxName.
  ///
  /// In en, this message translates to:
  /// **'Box Name'**
  String get boxName;

  /// No description provided for @lastInvoices.
  ///
  /// In en, this message translates to:
  /// **'Last Invoices'**
  String get lastInvoices;

  /// No description provided for @analytics.
  ///
  /// In en, this message translates to:
  /// **'Analytics'**
  String get analytics;

  /// No description provided for @income.
  ///
  /// In en, this message translates to:
  /// **'Income'**
  String get income;

  /// No description provided for @expense.
  ///
  /// In en, this message translates to:
  /// **'Expenses'**
  String get expense;

  /// No description provided for @sent.
  ///
  /// In en, this message translates to:
  /// **'Sent Invitations'**
  String get sent;

  /// No description provided for @noSent.
  ///
  /// In en, this message translates to:
  /// **'There Are No Sent Invitations'**
  String get noSent;

  /// No description provided for @received.
  ///
  /// In en, this message translates to:
  /// **'Received Invitations'**
  String get received;

  /// No description provided for @noReceived.
  ///
  /// In en, this message translates to:
  /// **'There Are No Received Invitations'**
  String get noReceived;

  /// No description provided for @invitationMessage.
  ///
  /// In en, this message translates to:
  /// **'has invited you to join'**
  String get invitationMessage;

  /// No description provided for @accept.
  ///
  /// In en, this message translates to:
  /// **'Accept'**
  String get accept;

  /// No description provided for @reject.
  ///
  /// In en, this message translates to:
  /// **'Reject'**
  String get reject;

  /// No description provided for @edit.
  ///
  /// In en, this message translates to:
  /// **'Edit'**
  String get edit;

  /// No description provided for @remove.
  ///
  /// In en, this message translates to:
  /// **'Delete'**
  String get remove;

  /// No description provided for @cancel.
  ///
  /// In en, this message translates to:
  /// **'Cancel'**
  String get cancel;

  /// No description provided for @notAvailableNow.
  ///
  /// In en, this message translates to:
  /// **'not available now'**
  String get notAvailableNow;

  /// No description provided for @subscriber.
  ///
  /// In en, this message translates to:
  /// **'Subscriber'**
  String get subscriber;

  /// No description provided for @admin.
  ///
  /// In en, this message translates to:
  /// **'Admin'**
  String get admin;

  /// No description provided for @subscriptions.
  ///
  /// In en, this message translates to:
  /// **'Subscriptions'**
  String get subscriptions;

  /// No description provided for @report.
  ///
  /// In en, this message translates to:
  /// **'account statement'**
  String get report;

  /// No description provided for @addFund.
  ///
  /// In en, this message translates to:
  /// **'Add Fund'**
  String get addFund;

  /// No description provided for @editFund.
  ///
  /// In en, this message translates to:
  /// **'Edit Fund'**
  String get editFund;

  /// No description provided for @fundName.
  ///
  /// In en, this message translates to:
  /// **'Fund Name'**
  String get fundName;

  /// No description provided for @addMember.
  ///
  /// In en, this message translates to:
  /// **'Add Member'**
  String get addMember;

  /// No description provided for @addMemberPlaceholder.
  ///
  /// In en, this message translates to:
  /// **'Type at least 5 digits of member\'s number'**
  String get addMemberPlaceholder;

  /// No description provided for @addBeneficiary.
  ///
  /// In en, this message translates to:
  /// **'Add Beneficiary'**
  String get addBeneficiary;

  /// No description provided for @currentBalance.
  ///
  /// In en, this message translates to:
  /// **'Current Balance'**
  String get currentBalance;

  /// No description provided for @searchFund.
  ///
  /// In en, this message translates to:
  /// **'Search By Fund Name'**
  String get searchFund;

  /// No description provided for @searchBeneficiary.
  ///
  /// In en, this message translates to:
  /// **'Search By Beneficiary Name'**
  String get searchBeneficiary;

  /// No description provided for @members.
  ///
  /// In en, this message translates to:
  /// **'Members'**
  String get members;

  /// No description provided for @noFunds.
  ///
  /// In en, this message translates to:
  /// **'There are no Funds'**
  String get noFunds;

  /// No description provided for @noFundsFound.
  ///
  /// In en, this message translates to:
  /// **'no Funds found'**
  String get noFundsFound;

  /// No description provided for @invoices.
  ///
  /// In en, this message translates to:
  /// **'Invoices'**
  String get invoices;

  /// No description provided for @addInvoice.
  ///
  /// In en, this message translates to:
  /// **'Add Invoice'**
  String get addInvoice;

  /// No description provided for @invoiceName.
  ///
  /// In en, this message translates to:
  /// **'Invoice name'**
  String get invoiceName;

  /// No description provided for @editInvoice.
  ///
  /// In en, this message translates to:
  /// **'Edit Invoice'**
  String get editInvoice;

  /// No description provided for @searchInvoice.
  ///
  /// In en, this message translates to:
  /// **'Search By Invoice Name'**
  String get searchInvoice;

  /// No description provided for @totalIncome.
  ///
  /// In en, this message translates to:
  /// **'Total Income'**
  String get totalIncome;

  /// No description provided for @totalExpense.
  ///
  /// In en, this message translates to:
  /// **'Total Expense'**
  String get totalExpense;

  /// No description provided for @amount.
  ///
  /// In en, this message translates to:
  /// **'Amount'**
  String get amount;

  /// No description provided for @tag.
  ///
  /// In en, this message translates to:
  /// **'Category'**
  String get tag;

  /// No description provided for @description.
  ///
  /// In en, this message translates to:
  /// **'Description'**
  String get description;

  /// No description provided for @deleteBox.
  ///
  /// In en, this message translates to:
  /// **'Delete Box'**
  String get deleteBox;

  /// No description provided for @deleteInvoice.
  ///
  /// In en, this message translates to:
  /// **'Delete Invoice'**
  String get deleteInvoice;

  /// No description provided for @deleteBoxMsg.
  ///
  /// In en, this message translates to:
  /// **'Are you sure you want to delete this box ?'**
  String get deleteBoxMsg;

  /// No description provided for @deleteInvoiceMsg.
  ///
  /// In en, this message translates to:
  /// **'Are you sure you want to delete this invoice ?'**
  String get deleteInvoiceMsg;

  /// No description provided for @descriptionRequired.
  ///
  /// In en, this message translates to:
  /// **'Description is required'**
  String get descriptionRequired;

  /// No description provided for @tagRequired.
  ///
  /// In en, this message translates to:
  /// **'Category is required'**
  String get tagRequired;

  /// No description provided for @amountRequired.
  ///
  /// In en, this message translates to:
  /// **'Amount is required'**
  String get amountRequired;

  /// No description provided for @beneficiaryRequired.
  ///
  /// In en, this message translates to:
  /// **'Beneficiary is required'**
  String get beneficiaryRequired;

  /// No description provided for @editProfile.
  ///
  /// In en, this message translates to:
  /// **'Edit Profile'**
  String get editProfile;

  /// No description provided for @notifications.
  ///
  /// In en, this message translates to:
  /// **'Notifications'**
  String get notifications;

  /// No description provided for @darkMode.
  ///
  /// In en, this message translates to:
  /// **'Dark Mode'**
  String get darkMode;

  /// No description provided for @save.
  ///
  /// In en, this message translates to:
  /// **'Save'**
  String get save;

  /// No description provided for @signIn.
  ///
  /// In en, this message translates to:
  /// **'Sign in'**
  String get signIn;

  /// No description provided for @loginToAccount.
  ///
  /// In en, this message translates to:
  /// **'Login'**
  String get loginToAccount;

  /// No description provided for @signUp.
  ///
  /// In en, this message translates to:
  /// **'Sign up'**
  String get signUp;

  /// No description provided for @createNewAccount.
  ///
  /// In en, this message translates to:
  /// **'Create New'**
  String get createNewAccount;

  /// No description provided for @resetPassword.
  ///
  /// In en, this message translates to:
  /// **'Reset Password'**
  String get resetPassword;

  /// No description provided for @resetPasswordCode.
  ///
  /// In en, this message translates to:
  /// **'Enter code to reset your password'**
  String get resetPasswordCode;

  /// No description provided for @changePassword.
  ///
  /// In en, this message translates to:
  /// **'Change Password'**
  String get changePassword;

  /// No description provided for @change.
  ///
  /// In en, this message translates to:
  /// **'Change'**
  String get change;

  /// No description provided for @changePasswordSuccess.
  ///
  /// In en, this message translates to:
  /// **'Password has been changed'**
  String get changePasswordSuccess;

  /// No description provided for @username.
  ///
  /// In en, this message translates to:
  /// **'Full Name'**
  String get username;

  /// No description provided for @password.
  ///
  /// In en, this message translates to:
  /// **'Password'**
  String get password;

  /// No description provided for @confirmPassword.
  ///
  /// In en, this message translates to:
  /// **'Confirm Password'**
  String get confirmPassword;

  /// No description provided for @email.
  ///
  /// In en, this message translates to:
  /// **'Email'**
  String get email;

  /// No description provided for @code.
  ///
  /// In en, this message translates to:
  /// **'Code'**
  String get code;

  /// No description provided for @codeNotValid.
  ///
  /// In en, this message translates to:
  /// **'Reset Code Is Not Valid'**
  String get codeNotValid;

  /// No description provided for @phone.
  ///
  /// In en, this message translates to:
  /// **'Phone Number'**
  String get phone;

  /// No description provided for @forgotPassword.
  ///
  /// In en, this message translates to:
  /// **'Forgot Password ?'**
  String get forgotPassword;

  /// No description provided for @changeLanguage.
  ///
  /// In en, this message translates to:
  /// **'Change Language'**
  String get changeLanguage;

  /// No description provided for @incorrectData.
  ///
  /// In en, this message translates to:
  /// **'The data you entered are incorrect'**
  String get incorrectData;

  /// No description provided for @typeToReset.
  ///
  /// In en, this message translates to:
  /// **'Please Type Your Phone Number To Reset Your Password'**
  String get typeToReset;

  /// No description provided for @noAccount.
  ///
  /// In en, this message translates to:
  /// **'Don\'t have an account ?'**
  String get noAccount;

  /// No description provided for @haveAccount.
  ///
  /// In en, this message translates to:
  /// **'Already have an account ?'**
  String get haveAccount;

  /// No description provided for @registerNow.
  ///
  /// In en, this message translates to:
  /// **'Register Now'**
  String get registerNow;

  /// No description provided for @verifyMobile.
  ///
  /// In en, this message translates to:
  /// **'Verify Mobile'**
  String get verifyMobile;

  /// No description provided for @verify.
  ///
  /// In en, this message translates to:
  /// **'Verify'**
  String get verify;

  /// No description provided for @notReceived.
  ///
  /// In en, this message translates to:
  /// **'Did not receive the code ?'**
  String get notReceived;

  /// No description provided for @mobileVerificationTitle.
  ///
  /// In en, this message translates to:
  /// **'Mobile Verification'**
  String get mobileVerificationTitle;

  /// No description provided for @mobileVerificationMsg.
  ///
  /// In en, this message translates to:
  /// **'A message containing an activation code has been sent to your mobile'**
  String get mobileVerificationMsg;

  /// No description provided for @register.
  ///
  /// In en, this message translates to:
  /// **'New Account'**
  String get register;

  /// No description provided for @terms.
  ///
  /// In en, this message translates to:
  /// **'Terms And Conditions'**
  String get terms;

  /// No description provided for @logout.
  ///
  /// In en, this message translates to:
  /// **'Logout'**
  String get logout;

  /// No description provided for @logoutConfirm.
  ///
  /// In en, this message translates to:
  /// **'Are you sure you want to log out ?'**
  String get logoutConfirm;

  /// No description provided for @currentPassword.
  ///
  /// In en, this message translates to:
  /// **'Current Password'**
  String get currentPassword;

  /// No description provided for @newPassword.
  ///
  /// In en, this message translates to:
  /// **'New Password'**
  String get newPassword;

  /// No description provided for @confirmNewPassword.
  ///
  /// In en, this message translates to:
  /// **'Confirm New Password'**
  String get confirmNewPassword;

  /// No description provided for @sendResetCode.
  ///
  /// In en, this message translates to:
  /// **'Send reset code'**
  String get sendResetCode;

  /// No description provided for @successSentResetCode.
  ///
  /// In en, this message translates to:
  /// **'Reset code sent successfully ,check sms'**
  String get successSentResetCode;

  /// No description provided for @invoiceType.
  ///
  /// In en, this message translates to:
  /// **'Invoice Type'**
  String get invoiceType;

  /// No description provided for @value.
  ///
  /// In en, this message translates to:
  /// **'Value'**
  String get value;

  /// No description provided for @addIncome.
  ///
  /// In en, this message translates to:
  /// **'Add Income'**
  String get addIncome;

  /// No description provided for @addExpense.
  ///
  /// In en, this message translates to:
  /// **'Add Expense'**
  String get addExpense;

  /// No description provided for @send.
  ///
  /// In en, this message translates to:
  /// **'Send'**
  String get send;

  /// No description provided for @contacts.
  ///
  /// In en, this message translates to:
  /// **'Contacts'**
  String get contacts;

  /// No description provided for @download.
  ///
  /// In en, this message translates to:
  /// **'Download'**
  String get download;

  /// No description provided for @shareToWhatsapp.
  ///
  /// In en, this message translates to:
  /// **'Share to whatsapp'**
  String get shareToWhatsapp;

  /// No description provided for @view.
  ///
  /// In en, this message translates to:
  /// **'View'**
  String get view;

  /// No description provided for @approved.
  ///
  /// In en, this message translates to:
  /// **'Approved'**
  String get approved;

  /// No description provided for @rejected.
  ///
  /// In en, this message translates to:
  /// **'Rejected'**
  String get rejected;

  /// No description provided for @canceled.
  ///
  /// In en, this message translates to:
  /// **'Canceled'**
  String get canceled;

  /// No description provided for @pending.
  ///
  /// In en, this message translates to:
  /// **'Pending'**
  String get pending;

  /// No description provided for @youInvitedMessage.
  ///
  /// In en, this message translates to:
  /// **'you invited {username} to join {boxName}'**
  String youInvitedMessage(Object boxName, Object username);

  /// No description provided for @invitedYouMessage.
  ///
  /// In en, this message translates to:
  /// **'{username} invited you to join {boxName}'**
  String invitedYouMessage(Object boxName, Object username);

  /// No description provided for @onBoarding_1.
  ///
  /// In en, this message translates to:
  /// **'No more losing receipts'**
  String get onBoarding_1;

  /// No description provided for @onBoarding_1_sub.
  ///
  /// In en, this message translates to:
  /// **'Upload all receipts directly into the expense history.'**
  String get onBoarding_1_sub;

  /// No description provided for @onBoarding_1_desc.
  ///
  /// In en, this message translates to:
  /// **'You can attach copies of purchase or payment receipts directly to the expense record to avoid losing them. Quickly add attachments from any phone by taking a picture of the receipt and saving it to a specific expense box. Save time and increase efficiency by keeping an organized and complete record of all expenses.'**
  String get onBoarding_1_desc;

  /// No description provided for @onBoarding_2.
  ///
  /// In en, this message translates to:
  /// **'Manage expenses per fund and per beneficiary separately'**
  String get onBoarding_2;

  /// No description provided for @onBoarding_2_desc.
  ///
  /// In en, this message translates to:
  /// **'Get a clear overview of the fund\'s expenses, or even a specific beneficiary of these expenses, such as a specific merchant you deal with, a worker who provides you with a service, or even telecommunications companies and your expenses with them. Manage your duties as an administrator by easily following up expense records within the fund members to monitor costs and ensure progress towards achieving the goal and within the available budget.'**
  String get onBoarding_2_desc;

  /// No description provided for @onBoarding_3.
  ///
  /// In en, this message translates to:
  /// **'Manage expenses by sharing and discussing fund expenses'**
  String get onBoarding_3;

  /// No description provided for @onBoarding_3_desc.
  ///
  /// In en, this message translates to:
  /// **'And that is through live and instant chat within this fund among all its members, where everyone can discuss a specific item of expenses in the same application. All these features in one place.'**
  String get onBoarding_3_desc;

  /// No description provided for @skip.
  ///
  /// In en, this message translates to:
  /// **'Skip'**
  String get skip;

  /// No description provided for @next.
  ///
  /// In en, this message translates to:
  /// **'Next'**
  String get next;

  /// No description provided for @finish.
  ///
  /// In en, this message translates to:
  /// **'Finish'**
  String get finish;

  /// No description provided for @search.
  ///
  /// In en, this message translates to:
  /// **'Search'**
  String get search;

  /// No description provided for @imageSaved.
  ///
  /// In en, this message translates to:
  /// **'Image saved to gallery'**
  String get imageSaved;

  /// No description provided for @noMembers.
  ///
  /// In en, this message translates to:
  /// **'No members found'**
  String get noMembers;

  /// No description provided for @noBeneficiaries.
  ///
  /// In en, this message translates to:
  /// **'No Beneficiaries found'**
  String get noBeneficiaries;

  /// No description provided for @noBoxes.
  ///
  /// In en, this message translates to:
  /// **'No Boxes found'**
  String get noBoxes;

  /// No description provided for @invoiceImages.
  ///
  /// In en, this message translates to:
  /// **'Invoice Images'**
  String get invoiceImages;

  /// No description provided for @more.
  ///
  /// In en, this message translates to:
  /// **'More'**
  String get more;

  /// No description provided for @passwordChanged.
  ///
  /// In en, this message translates to:
  /// **'successfully changed password'**
  String get passwordChanged;

  /// No description provided for @savedImage.
  ///
  /// In en, this message translates to:
  /// **'Image saved to gallery'**
  String get savedImage;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['ar', 'en'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'ar': return AppLocalizationsAr();
    case 'en': return AppLocalizationsEn();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
