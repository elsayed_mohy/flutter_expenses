import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get phoneKey => 'Enter your phone';

  @override
  String get passwordKey => 'Enter your password';

  @override
  String get myFunds => 'My Funds';

  @override
  String get profile => 'Profile';

  @override
  String get dashboard => 'Dashboard';

  @override
  String get settings => 'Settings';

  @override
  String get beneficiaries => 'Beneficiaries';

  @override
  String get invitations => 'Invitations';

  @override
  String get boxNumber => 'Boxes Number';

  @override
  String get memberNumber => 'Members Number';

  @override
  String get userSummary => 'Account Summary';

  @override
  String get topBox => 'Top Box';

  @override
  String get topCategory => 'Top Category';

  @override
  String get topBeneficiary => 'Top Beneficiary';

  @override
  String get beneficiaryName => 'Beneficiary Name';

  @override
  String get boxName => 'Box Name';

  @override
  String get lastInvoices => 'Last Invoices';

  @override
  String get analytics => 'Analytics';

  @override
  String get income => 'Income';

  @override
  String get expense => 'Expenses';

  @override
  String get sent => 'Sent Invitations';

  @override
  String get noSent => 'There Are No Sent Invitations';

  @override
  String get received => 'Received Invitations';

  @override
  String get noReceived => 'There Are No Received Invitations';

  @override
  String get invitationMessage => 'has invited you to join';

  @override
  String get accept => 'Accept';

  @override
  String get reject => 'Reject';

  @override
  String get edit => 'Edit';

  @override
  String get remove => 'Delete';

  @override
  String get cancel => 'Cancel';

  @override
  String get notAvailableNow => 'not available now';

  @override
  String get subscriber => 'Subscriber';

  @override
  String get admin => 'Admin';

  @override
  String get subscriptions => 'Subscriptions';

  @override
  String get report => 'account statement';

  @override
  String get addFund => 'Add Fund';

  @override
  String get editFund => 'Edit Fund';

  @override
  String get fundName => 'Fund Name';

  @override
  String get addMember => 'Add Member';

  @override
  String get addMemberPlaceholder => 'Type at least 5 digits of member\'s number';

  @override
  String get addBeneficiary => 'Add Beneficiary';

  @override
  String get currentBalance => 'Current Balance';

  @override
  String get searchFund => 'Search By Fund Name';

  @override
  String get searchBeneficiary => 'Search By Beneficiary Name';

  @override
  String get members => 'Members';

  @override
  String get noFunds => 'There are no Funds';

  @override
  String get noFundsFound => 'no Funds found';

  @override
  String get invoices => 'Invoices';

  @override
  String get addInvoice => 'Add Invoice';

  @override
  String get invoiceName => 'Invoice name';

  @override
  String get editInvoice => 'Edit Invoice';

  @override
  String get searchInvoice => 'Search By Invoice Name';

  @override
  String get totalIncome => 'Total Income';

  @override
  String get totalExpense => 'Total Expense';

  @override
  String get amount => 'Amount';

  @override
  String get tag => 'Category';

  @override
  String get description => 'Description';

  @override
  String get deleteBox => 'Delete Box';

  @override
  String get deleteInvoice => 'Delete Invoice';

  @override
  String get deleteBoxMsg => 'Are you sure you want to delete this box ?';

  @override
  String get deleteInvoiceMsg => 'Are you sure you want to delete this invoice ?';

  @override
  String get descriptionRequired => 'Description is required';

  @override
  String get tagRequired => 'Category is required';

  @override
  String get amountRequired => 'Amount is required';

  @override
  String get beneficiaryRequired => 'Beneficiary is required';

  @override
  String get editProfile => 'Edit Profile';

  @override
  String get notifications => 'Notifications';

  @override
  String get darkMode => 'Dark Mode';

  @override
  String get save => 'Save';

  @override
  String get signIn => 'Sign in';

  @override
  String get loginToAccount => 'Login';

  @override
  String get signUp => 'Sign up';

  @override
  String get createNewAccount => 'Create New';

  @override
  String get resetPassword => 'Reset Password';

  @override
  String get resetPasswordCode => 'Enter code to reset your password';

  @override
  String get changePassword => 'Change Password';

  @override
  String get change => 'Change';

  @override
  String get changePasswordSuccess => 'Password has been changed';

  @override
  String get username => 'Full Name';

  @override
  String get password => 'Password';

  @override
  String get confirmPassword => 'Confirm Password';

  @override
  String get email => 'Email';

  @override
  String get code => 'Code';

  @override
  String get codeNotValid => 'Reset Code Is Not Valid';

  @override
  String get phone => 'Phone Number';

  @override
  String get forgotPassword => 'Forgot Password ?';

  @override
  String get changeLanguage => 'Change Language';

  @override
  String get incorrectData => 'The data you entered are incorrect';

  @override
  String get typeToReset => 'Please Type Your Phone Number To Reset Your Password';

  @override
  String get noAccount => 'Don\'t have an account ?';

  @override
  String get haveAccount => 'Already have an account ?';

  @override
  String get registerNow => 'Register Now';

  @override
  String get verifyMobile => 'Verify Mobile';

  @override
  String get verify => 'Verify';

  @override
  String get notReceived => 'Did not receive the code ?';

  @override
  String get mobileVerificationTitle => 'Mobile Verification';

  @override
  String get mobileVerificationMsg => 'A message containing an activation code has been sent to your mobile';

  @override
  String get register => 'New Account';

  @override
  String get terms => 'Terms And Conditions';

  @override
  String get logout => 'Logout';

  @override
  String get logoutConfirm => 'Are you sure you want to log out ?';

  @override
  String get currentPassword => 'Current Password';

  @override
  String get newPassword => 'New Password';

  @override
  String get confirmNewPassword => 'Confirm New Password';

  @override
  String get sendResetCode => 'Send reset code';

  @override
  String get successSentResetCode => 'Reset code sent successfully ,check sms';

  @override
  String get invoiceType => 'Invoice Type';

  @override
  String get value => 'Value';

  @override
  String get addIncome => 'Add Income';

  @override
  String get addExpense => 'Add Expense';

  @override
  String get send => 'Send';

  @override
  String get contacts => 'Contacts';

  @override
  String get download => 'Download';

  @override
  String get shareToWhatsapp => 'Share to whatsapp';

  @override
  String get view => 'View';

  @override
  String get approved => 'Approved';

  @override
  String get rejected => 'Rejected';

  @override
  String get canceled => 'Canceled';

  @override
  String get pending => 'Pending';

  @override
  String youInvitedMessage(Object boxName, Object username) {
    return 'you invited $username to join $boxName';
  }

  @override
  String invitedYouMessage(Object boxName, Object username) {
    return '$username invited you to join $boxName';
  }

  @override
  String get onBoarding_1 => 'No more losing receipts';

  @override
  String get onBoarding_1_sub => 'Upload all receipts directly into the expense history.';

  @override
  String get onBoarding_1_desc => 'You can attach copies of purchase or payment receipts directly to the expense record to avoid losing them. Quickly add attachments from any phone by taking a picture of the receipt and saving it to a specific expense box. Save time and increase efficiency by keeping an organized and complete record of all expenses.';

  @override
  String get onBoarding_2 => 'Manage expenses per fund and per beneficiary separately';

  @override
  String get onBoarding_2_desc => 'Get a clear overview of the fund\'s expenses, or even a specific beneficiary of these expenses, such as a specific merchant you deal with, a worker who provides you with a service, or even telecommunications companies and your expenses with them. Manage your duties as an administrator by easily following up expense records within the fund members to monitor costs and ensure progress towards achieving the goal and within the available budget.';

  @override
  String get onBoarding_3 => 'Manage expenses by sharing and discussing fund expenses';

  @override
  String get onBoarding_3_desc => 'And that is through live and instant chat within this fund among all its members, where everyone can discuss a specific item of expenses in the same application. All these features in one place.';

  @override
  String get skip => 'Skip';

  @override
  String get next => 'Next';

  @override
  String get finish => 'Finish';

  @override
  String get search => 'Search';

  @override
  String get imageSaved => 'Image saved to gallery';

  @override
  String get noMembers => 'No members found';

  @override
  String get noBeneficiaries => 'No Beneficiaries found';

  @override
  String get noBoxes => 'No Boxes found';

  @override
  String get invoiceImages => 'Invoice Images';

  @override
  String get more => 'More';

  @override
  String get passwordChanged => 'successfully changed password';

  @override
  String get savedImage => 'Image saved to gallery';
}
