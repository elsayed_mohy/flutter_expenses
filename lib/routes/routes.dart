import 'package:expenses/logic/bindings/auth_binding.dart';
import 'package:expenses/logic/bindings/home_binding.dart';
import 'package:expenses/view/screens/auth/forgot_password/reset_password_screen.dart';
import 'package:expenses/view/screens/auth/login_screen.dart';
import 'package:expenses/view/screens/auth/sign_up/create_account_screen.dart';
import 'package:expenses/view/screens/auth/sign_up/phone_step_screen.dart';
import 'package:expenses/view/screens/auth/sign_up/verify_otp_screen.dart';
import 'package:expenses/view/screens/user_layout/funds/add_edit_invoice_screen.dart';
import 'package:expenses/view/screens/user_layout/funds/box_details_screen.dart';
import 'package:expenses/view/screens/user_layout/user_layout.dart';
import 'package:expenses/view/widgets/auth/forgot_password_screen.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

import '../view/screens/onBoarding/on_boarding_page.dart';

class AppRoutes {
  static const welcomeRoute = Routes.welcomeScreen;
  static const homeRoute = Routes.home;
  static const loginScreen = Routes.loginScreen;
  static final routes = [
    GetPage(
      name: Routes.welcomeScreen,
      page: () => OnboardingPage(),
    ),
    GetPage(
      name: Routes.signUpPhoneStepScreen,
      page: () => PhoneStep(register: true),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.signUpVerifyOTPScreen,
      page: () => VerifyOTP(register: true),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.forgotPasswordPhoneStepScreen,
      page: () => PhoneStep(register: false),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.forgotPasswordVerifyOTPScreen,
      page: () => VerifyOTP(register: false),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.createAccountScreen,
      page: () => CreateAccountScreen(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.loginScreen,
      page: () => LoginScreen(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.resetPasswordScreen,
      page: () => ResetPasswordScreen(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: Routes.home,
      page: () => UserLayout(),
      binding: HomeBinding(),
      children:[
        GetPage(name: Routes.boxDetailsScreen, page: () => BoxDetailsScreen()),
        GetPage(name: Routes.addEditInvoiceScreen, page: () => AddEditInvoiceScreen()),
      ]
    ),
  ];
}

class Routes {
  static const welcomeScreen = "/welcomeScreen";
  static const loginScreen = "/loginScreen";
  static const signUpPhoneStepScreen = "/signUpPhoneStepScreen";
  static const signUpVerifyOTPScreen = "/signUpVerifyOTPScreen";
  static const createAccountScreen = "/createAccountScreen";
  static const resetPasswordScreen = "/resetPasswordScreen";
  static const forgotPasswordPhoneStepScreen = "/forgotPasswordPhoneStepScreen";
  static const forgotPasswordVerifyOTPScreen = "/forgotPasswordVerifyOTPScreen";
  static const home = "/home";
  static const profileScreen = "/profileScreen";
  static const editProfileScreen = "/editProfileScreen";
  static const boxDetailsScreen = "/boxDetailsScreen";
  static const addEditInvoiceScreen = "/addEditInvoiceScreen";
}
