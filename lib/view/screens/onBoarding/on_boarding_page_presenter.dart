import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/model/onboarding.dart';
import '../../../l10n/app_localizations.dart';
import '../../widgets/shared/text_utils.dart';

class OnboardingPagePresenter extends StatefulWidget {
  final List<OnboardingPageModel> pages;
  final VoidCallback? onSkip;
  final VoidCallback? onFinish;

  const OnboardingPagePresenter(
      {Key? key, required this.pages, this.onSkip, this.onFinish})
      : super(key: key);

  @override
  State<OnboardingPagePresenter> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPagePresenter> {
  // Store the currently visible page
  int _currentPage = 0;
  // Define a controller for the pageview
  final PageController _pageController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedContainer(
        duration: const Duration(milliseconds: 250),
        // color: widget.pages[_currentPage].bgColor,
        child: Column(
          children: [
            Expanded(
              // Pageview to render each page
              child: PageView.builder(
                controller: _pageController,
                itemCount: widget.pages.length,
                onPageChanged: (idx) {
                  // Change current page when pageview changes
                  setState(() {
                    _currentPage = idx;
                  });
                },
                itemBuilder: (context, idx) {
                  final item = widget.pages[idx];
                  return Column(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(32.0),
                          child:  SvgPicture.asset(
                            item.imageUrl,
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Column(children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child:TextUtils(text: item.title,fontWeight: FontWeight.bold,
                              fontSize: 18,align: TextAlign.center,)

                            ),
                            Container(
                              constraints:
                              const BoxConstraints(maxWidth: 350),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 24.0, vertical: 8.0),
                              child:
                  TextUtils(text: item.description,
                  align: TextAlign.center,)

                            )
                          ]))
                    ],
                  );
                },
              ),
            ),

            // Current page indicator
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: widget.pages
                  .map((item) => AnimatedContainer(
                duration: const Duration(milliseconds: 250),
                width: _currentPage == widget.pages.indexOf(item)
                    ? 30
                    : 8,
                height: 8,
                margin: const EdgeInsets.all(2.0),
                decoration: BoxDecoration(
                    color: ColorsManager.main,
                    borderRadius: BorderRadius.circular(10.0)),
              ))
                  .toList(),
            ),

            // Bottom buttons
            SizedBox(
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      style: TextButton.styleFrom(
                          visualDensity: VisualDensity.comfortable,
                          foregroundColor: ColorsManager.disabled,
                          textStyle: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      onPressed: () {
                        widget.onSkip?.call();
                        GetStorage().write('initialized', true);
                      },
                      child:  Text(AppLocalizations.of(context)!.skip)),
                  TextButton(
                    style: TextButton.styleFrom(
                        visualDensity: VisualDensity.comfortable,
                        foregroundColor: ColorsManager.main,
                        textStyle: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold)),
                    onPressed: () {
                      if (_currentPage == widget.pages.length - 1) {
                        widget.onFinish?.call();
                        GetStorage().write('initialized', true);
                      } else {
                        _pageController.animateToPage(_currentPage + 1,
                            curve: Curves.easeInOutCubic,
                            duration: const Duration(milliseconds: 250));
                      }
                    },
                    child: Row(
                      children: [
                        Text(
                          _currentPage == widget.pages.length - 1
                              ? AppLocalizations.of(context)!.finish
                              : AppLocalizations.of(context)!.next,
                        ),
                        const SizedBox(width: 8),
                        Icon(_currentPage == widget.pages.length - 1
                            ? Icons.done
                            : Icons.arrow_forward),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}