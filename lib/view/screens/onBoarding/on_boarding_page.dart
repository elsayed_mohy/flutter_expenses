import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/model/onboarding.dart';
import '../../../l10n/app_localizations.dart';
import '../../../routes/routes.dart';
import 'on_boarding_page_presenter.dart';

class OnboardingPage extends StatelessWidget {
  const OnboardingPage({Key? key}) : super(key: key);

  navigateToLogin() {
    Get.offAllNamed(Routes.loginScreen);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: OnboardingPagePresenter(
          onFinish: navigateToLogin,
          onSkip: navigateToLogin,
          pages: [
        OnboardingPageModel(
          title: AppLocalizations.of(context)!.onBoarding_1,
          description: AppLocalizations.of(context)!.onBoarding_1_desc,
          imageUrl: 'assets/images/ui/invoices.svg',
          // bgColor: Colors.indigo,
        ),
        OnboardingPageModel(
          title: AppLocalizations.of(context)!.onBoarding_2,
          description: AppLocalizations.of(context)!.onBoarding_2_desc,
          imageUrl: 'assets/images/ui/mobile.svg',
          // bgColor: const Color(0xff1eb090),
        ),
        OnboardingPageModel(
          title: AppLocalizations.of(context)!.onBoarding_3,
          description:
          AppLocalizations.of(context)!.onBoarding_3_desc,
          imageUrl: 'assets/images/ui/analysis.svg',

          // bgColor: const Color(0xfffeae4f),
        ),
      ]),
    );
  }
}