import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../l10n/app_localizations.dart';
import '../../../../logic/controller/auth_controller.dart';
import '../../../../routes/routes.dart';
import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';
import '../../../widgets/auth/auth_button.dart';
import '../../../widgets/auth/auth_logo.dart';
import '../../../widgets/auth/auth_text_form_field.dart';
import '../../../widgets/auth/have_account_container.dart';
import '../../../widgets/shared/text_utils.dart';

class ResetPasswordScreen extends StatefulWidget {
  ResetPasswordScreen({super.key});

  @override
  State<ResetPasswordScreen> createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<ResetPasswordScreen> {
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmNewPasswordController =
  TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool newObscureText = true;
  bool confirmNewObscureText = true;
  final controller = Get.find<AuthController>();
  final storage = GetStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ThemesManager().isSavedDarkMode()
              ? ColorsManager.svgDark
              : ColorsManager.svgLight,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AuthLogo(),
              SizedBox(height: 10),
              Container(
                width: double.infinity,
                height: MediaQuery
                    .of(context)
                    .size
                    .height / 1.6,
                child: Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: TextUtils(
                                text: AppLocalizations.of(context)!.resetPassword,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 25),
                        AuthTextFormField(
                          controller: newPasswordController,
                          prefixIcon: CupertinoIcons.lock_fill,
                          suffixIcon: IconButton(
                              icon: Icon(newObscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              onPressed: () {
                                setState(() {
                                  newObscureText = !newObscureText;
                                });
                              }),
                          obscureText: newObscureText,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'New Password is required';
                            }
                            return null;
                          },
                          hintText: AppLocalizations.of(context)!.newPassword,
                        ),
                        const SizedBox(height: 20),
                        AuthTextFormField(
                          controller: confirmNewPasswordController,
                          obscureText: confirmNewObscureText,
                          prefixIcon: CupertinoIcons.lock_fill,
                          suffixIcon: IconButton(
                              icon: Icon(confirmNewObscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                                size: 20,),
                              onPressed: () {
                                setState(() {
                                  confirmNewObscureText = !confirmNewObscureText;
                                });
                              }),
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'New Password is required';
                            }
                            return null;
                          },
                          hintText: AppLocalizations.of(context)!.confirmNewPassword,
                        ),
                        const SizedBox(height: 30),
                        const SizedBox(height: 30),
                        GetBuilder<AuthController>(
                            init: AuthController(),
                            builder: (AuthController controller) {
                              return AuthButton(
                                text: AppLocalizations.of(context)!.save,
                                onPressed: () {
                                  var phoneNumber = storage.read("phoneNumber");
                                  var otp = storage.read("otp");
                                  controller.resetPassword(
                                      phoneNumber: phoneNumber,
                                      newPassword: newPasswordController.text,
                                      otp: otp);
                                },
                              );
                            })

                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
