import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../l10n/app_localizations.dart';
import '../../../../logic/controller/auth_controller.dart';
import '../../../../routes/routes.dart';
import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';
import '../../../widgets/auth/auth_button.dart';
import '../../../widgets/auth/auth_logo.dart';
import '../../../widgets/auth/sign_up/otp_form_field.dart';
import '../../../widgets/shared/text_utils.dart';

class VerifyOTP extends StatelessWidget {
  VerifyOTP({super.key, required this.register});
  final bool register;

  final TextEditingController firstController = TextEditingController();
  final TextEditingController secondController = TextEditingController();
  final TextEditingController thirdController = TextEditingController();
  final TextEditingController fourthController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final storage = GetStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ThemesManager().isSavedDarkMode() ? ColorsManager.darkGrey : Colors.white,

        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          "assets/images/ui/otp.svg",
                          height: 200,
                        ),
                        const SizedBox(height: 25),
                        TextUtils(
                          text: AppLocalizations.of(context)!.mobileVerificationTitle,
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                        const SizedBox(height: 10),
                        SizedBox(
                          width: 300,
                          child: TextUtils(
                            text:
                            AppLocalizations.of(context)!.mobileVerificationMsg,
                            align: TextAlign.center,
                            color: ColorsManager.disabled,
                          ),
                        ),
                        const SizedBox(height: 10),
                        TextUtils(
                          text: storage.read("fullPhoneNumber").toString(),
                          fontWeight: FontWeight.w500,
                        ),
                        const SizedBox(height: 25),
                        Directionality(
                          textDirection: TextDirection.ltr,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              OTPFormField(
                                controller: firstController,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'required';
                                  }
                                  return null;
                                },
                              ),
                              OTPFormField(
                                controller: secondController,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'required';
                                  }
                                  return null;
                                },
                              ),
                              OTPFormField(
                                controller: thirdController,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'required';
                                  }
                                  return null;
                                },
                              ),
                              OTPFormField(
                                controller: fourthController,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return 'required';
                                  }
                                  return null;
                                },
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 50),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(vertical: 20.0),
                            child: GetBuilder<AuthController>(
                                init: AuthController(),
                                builder: (AuthController controller) {
                                  return
                                    AuthButton(
                                      text: AppLocalizations.of(context)!.verify,
                                      onPressed: () {
                                        var dialCode = storage.read("dialCode");
                                        var phoneNumber = storage.read("phoneNumber");
                                        var otp = firstController.text + secondController.text + thirdController.text +fourthController.text;
                                        controller.validateOTP(dialCode: dialCode, phoneNumber: phoneNumber, otp: otp, register: register);
                                      },
                                    );
                                }),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
