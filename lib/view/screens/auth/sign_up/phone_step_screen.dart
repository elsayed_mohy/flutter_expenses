import 'package:expenses/logic/controller/auth_controller.dart';
import 'package:expenses/routes/routes.dart';
import 'package:expenses/services/base_service.dart';
import 'package:expenses/view/widgets/auth/auth_button.dart';
import 'package:expenses/view/widgets/auth/auth_text_form_field.dart';
import 'package:expenses/view/widgets/auth/have_account_container.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl_phone_field/countries.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

import '../../../../l10n/app_localizations.dart';
import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';
import '../../../widgets/auth/auth_logo.dart';

class PhoneStep extends StatelessWidget {
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController dialCodeController = TextEditingController(text: '+20');
  final formKey = GlobalKey<FormState>();
  final storage = GetStorage();
  bool acceptConditions = false;
  FocusNode focusNode = FocusNode();
  PhoneStep({Key? key, required this.register}) : super(key: key);
  final bool register;
  String? countryCode;
  final _country =
  countries.firstWhere((element) => element.code == 'EG');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ThemesManager().isSavedDarkMode()
              ? ColorsManager.svgDark
              : ColorsManager.svgLight,
        ),
        body:                          GetBuilder<AuthController>(
            init: AuthController(),
            builder: (AuthController controller) {
              return SingleChildScrollView(
                child:
                Column(
                  children: [
                    AuthLogo(),
                    SizedBox(height: 10),
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height / 1.7,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 25, right: 25),
                        child: Form(
                          key:formKey,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: TextUtils(
                                      text:register ?  AppLocalizations.of(context)!.createNewAccount : AppLocalizations.of(context)!.forgotPassword,
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  // const SizedBox(width: 5),
                                  // TextUtils(
                                  //   text: 'up',
                                  //   color:
                                  //   Get.isDarkMode ? ColorsManager.disabled : ColorsManager.darkGrey,
                                  //   fontSize: 35,
                                  //   fontWeight: FontWeight.w500,
                                  // ),
                                ],
                              ),
                              if(!register)
                                Column(children: [
                                  const SizedBox(height: 10),
                                   TextUtils(
                                    text:AppLocalizations.of(context)!.typeToReset,
                                  ),
                                ],),
                              const SizedBox(height: 25),
                              IntlPhoneField(
                                controller: phoneController,
                                focusNode: focusNode,
                                languageCode:BaseService().getCurrentLanguage(),
                                disableLengthCheck: true,
                                cursorColor:ThemesManager().isSavedDarkMode() ? ColorsManager.secondary : ColorsManager.darkGrey,
                                initialCountryCode: "EG",
                                decoration: InputDecoration(
                                  fillColor: ColorsManager.disabled.withOpacity(0.07),
                                  filled: true,
                                  hintText:  AppLocalizations.of(context)!.phone,
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.transparent,
                                    ),
                                    borderRadius: BorderRadius.circular(10),

                                  ),
                                  hintStyle: const TextStyle(
                                      color: ColorsManager.disabled,
                                      fontSize: 14
                                  ),
                                  labelStyle:const TextStyle(
                                      color: ColorsManager.darkGrey,
                                      fontSize: 14
                                  ) ,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: ColorsManager.secondary,
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: ColorsManager.warning,
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: ColorsManager.warning,
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                onChanged: (phone) {
                                    storage.write("fullPhoneNumber", phone.completeNumber);
                                    storage.write("phoneNumber", phone.number);
countryCode = phone.countryCode;
                                },
                                onCountryChanged: (country) {
                                  dialCodeController.text = country.dialCode;
                                },
                              ),
                              const SizedBox(height: 40),
                              AuthButton(
                                text:  AppLocalizations.of(context)!.verifyMobile,
                                onPressed: () {
                                  storage.write("dialCode", countryCode);
                                  controller.sendOTP(dialCode: countryCode, phoneNumber: phoneController.text, register: register);
                                },
                              ),

                            ],
                          ),
                        ),
                      ),
                    ),
                    if(register)
                    Column(children: [
                      const SizedBox(height: 18),
                      HaveAccountContainer(
                        text:  AppLocalizations.of(context)!.haveAccount,
                        authTypeText:  AppLocalizations.of(context)!.signIn,
                        onPressed: () {
                          Get.offNamed(Routes.loginScreen);
                        },
                      )
                    ]),
                  ],
                ) ,
              );

            }
        )


    );
  }
}
