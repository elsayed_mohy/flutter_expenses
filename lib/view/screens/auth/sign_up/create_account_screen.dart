import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../l10n/app_localizations.dart';
import '../../../../logic/controller/auth_controller.dart';
import '../../../../routes/routes.dart';
import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';
import '../../../widgets/auth/auth_button.dart';
import '../../../widgets/auth/auth_logo.dart';
import '../../../widgets/auth/auth_text_form_field.dart';
import '../../../widgets/auth/have_account_container.dart';
import '../../../widgets/shared/text_utils.dart';

class CreateAccountScreen extends StatefulWidget {
  CreateAccountScreen({super.key});

  @override
  State<CreateAccountScreen> createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  final TextEditingController nameController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  final TextEditingController confirmPasswordController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  bool obscureText = true;
  bool confirmObscureText = true;

  final controller = Get.find<AuthController>();
  final storage = GetStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ThemesManager().isSavedDarkMode()
              ? ColorsManager.svgDark
              : ColorsManager.svgLight,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AuthLogo(),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: TextUtils(
                              text: AppLocalizations.of(context)!.createNewAccount,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 25),
                      AuthTextFormField(
                        controller: nameController,
                        obscureText: false,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'full name is required';
                          }
                          return null;
                        },
                        prefixIcon:
                        CupertinoIcons.person_solid,
                        hintText: AppLocalizations.of(context)!.username,
                      ),
                      const SizedBox(height: 20),
                      AuthTextFormField(
                        prefixIcon:
                        CupertinoIcons.lock_fill,

                        controller: passwordController,
                        suffixIcon: IconButton(
                            icon: Icon(obscureText
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                obscureText = !obscureText;
                              });
                            }),
                        obscureText: obscureText,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Password is required';
                          }
                          return null;
                        },
                        hintText: AppLocalizations.of(context)!.password,
                      ),
                      const SizedBox(height: 20),
                      AuthTextFormField(
                        controller: confirmPasswordController,
                        obscureText: confirmObscureText,
                        prefixIcon:
                        CupertinoIcons.lock_fill,
                        suffixIcon: IconButton(
                            icon: Icon(confirmObscureText
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                confirmObscureText = !confirmObscureText;
                              });
                            }),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Password is required';
                          }
                          return null;
                        },
                        hintText: AppLocalizations.of(context)!.confirmPassword,
                      ),
                      const SizedBox(height: 30),
                      GetBuilder<AuthController>(
                          init: AuthController(),
                          builder: (AuthController controller) {
                            return AuthButton(
                              text: AppLocalizations.of(context)!.save,
                              onPressed: () {
                                var dialCode = storage.read("dialCode");
                                var phoneNumber = storage.read("phoneNumber");
                                var otp = storage.read("otp");
                                controller.register(
                                    fullName: nameController.text,
                                    dialCode: dialCode,
                                    phoneNumber: phoneNumber,
                                    password: passwordController.text,
                                    preferLanguage: "ar",
                                    otp: otp);
                                //Get.offAllNamed(Routes.home);
                              },
                            );
                          })

                    ],
                  ),
                ),
              ),
              const SizedBox(height: 18),
              HaveAccountContainer(
                text: AppLocalizations.of(context)!.haveAccount,
                authTypeText: AppLocalizations.of(context)!.signIn,
                onPressed: () {
                  Get.offNamed(Routes.loginScreen);
                },
              )
            ],
          ),
        ));
  }
}
