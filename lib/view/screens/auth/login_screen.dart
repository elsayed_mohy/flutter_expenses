import 'package:expenses/logic/controller/auth_controller.dart';
import 'package:expenses/routes/routes.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/auth/auth_button.dart';
import 'package:expenses/view/widgets/auth/auth_logo.dart';
import 'package:expenses/view/widgets/auth/auth_text_form_field.dart';
import 'package:expenses/view/widgets/auth/have_account_container.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../l10n/app_localizations.dart';
import '../../../utils/theme.dart';
import '../../widgets/shared/background_paint.dart';

class LoginScreen extends StatelessWidget {
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final controller = Get.find<AuthController>();

  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 0,
            backgroundColor: ThemesManager().isSavedDarkMode()
                ? ColorsManager.svgDark
                : ColorsManager.svgLight),
        body: SingleChildScrollView(
          child: Column(
            children: [
              AuthLogo(),
              SizedBox(height: 10),
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.58,
                child: Padding(
                  padding: const EdgeInsets.only(left: 25, right: 25),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: TextUtils(
                                text: AppLocalizations.of(context)!
                                    .loginToAccount,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 25),
                        AuthTextFormField(
                          keyboardType: TextInputType.number,
                          controller: phoneController,
                          obscureText: false,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'phone is required';
                            }
                            return null;
                          },
                          prefixIcon: Icons.phone,
                          hintText: AppLocalizations.of(context)!.phone,
                        ),
                        const SizedBox(height: 20),
                        GetBuilder<AuthController>(builder: (_) {
                          return AuthTextFormField(
                            controller: passwordController,
                            obscureText: controller.isVisibility ? false : true,
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'password is required';
                              }
                              return null;
                            },
                            prefixIcon: Icons.lock,
                            hintText: AppLocalizations.of(context)!.password,
                            suffixIcon: IconButton(
                              onPressed: () {
                                controller.visibility();
                              },
                              icon: controller.isVisibility
                                  ? Icon(
                                      Icons.visibility,
                                      color: ColorsManager.main,
                                    )
                                  : Icon(
                                      Icons.visibility_off,
                                      color: ColorsManager.main,
                                    ),
                            ),
                          );
                        }),
                        const SizedBox(height: 10),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Get.toNamed(
                                      Routes.forgotPasswordPhoneStepScreen);
                                },
                                child: TextUtils(
                                  text: AppLocalizations.of(context)!
                                      .forgotPassword,
                                  color: ColorsManager.main,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ]),
                        const SizedBox(height: 20),
                        GetBuilder<AuthController>(builder: (_) {
                          return AuthButton(
                            text: AppLocalizations.of(context)!.signIn,
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                controller.login(
                                    phone: phoneController.text,
                                    password: passwordController.text);
                              }

                              //Get.offAllNamed(Routes.home);
                            },
                          );
                        })
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 18),
              HaveAccountContainer(
                text: AppLocalizations.of(context)!.noAccount,
                authTypeText: AppLocalizations.of(context)!.signUp,
                onPressed: () {
                  Get.offNamed(Routes.signUpPhoneStepScreen);
                },
              )
            ],
          ),
        ));
  }
}
