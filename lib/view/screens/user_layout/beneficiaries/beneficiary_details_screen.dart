import 'package:expenses/data/model/beneficiaries/beneficiary_details.dart';
import 'package:expenses/data/model/invoice/invoice_model.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/beneficiary_controller.dart';
import 'package:expenses/view/widgets/auth/auth_button.dart';
import 'package:expenses/view/widgets/funds/custom_chip_list.dart';
import 'package:expenses/view/widgets/funds/invoices_list.dart';
import 'package:expenses/view/widgets/shared/accent_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../widgets/beneficiaries/beneficiary_invoices_list.dart';
import '../../../widgets/shared/secondary_button.dart';

class BeneficiaryDetailsScreen extends StatelessWidget {
  final String beneficiaryName;
  final controller = Get.find<BeneficiaryController>();

  BeneficiaryDetailsScreen({super.key, required this.beneficiaryName});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(beneficiaryName),
        actions: buildAppBarActions(context),
      ),
      backgroundColor: context.theme.backgroundColor,
      body: GetBuilder<BeneficiaryController>(
        builder: (_) {
      if (controller.isLoadingBeneficiaryDetails) {
        return Center(
          child: CircularProgressIndicator(
            color: Colors.blue,
          ),
        );
      } else {
        List<BeneficiaryDetails> list = controller.beneficiaryDetailsList;
        List<Invoice> invoices = controller.invoices;
        return SingleChildScrollView(
          child: Column(
            children: [
              CustomChipList(list: list),
              SizedBox(height: 15,),
              BeneficiaryInvoicesList(invoices: invoices,),
            ]

          ),
        );
      }
      // var subBoxes = controller.getSubscriptionBoxes();
        },
      ),
    );
  }

  List<Widget> buildAppBarActions(BuildContext context) {

      return [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            width: 110,
            height: 30,
            child: AccentButton(text: AppLocalizations.of(context)!.report, onPressed: () {
              _load(controller.beneficiaryDetailsList, beneficiaryName);
            }),
          ),
        )
      ];
    }

  _load(List<BeneficiaryDetails> list,String beneficiary) {
    List ids = list.map((o) => o.id).toSet().toList();
    controller.downloadPdf(beneficiary,ids);
  }

}
