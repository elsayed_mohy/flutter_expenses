import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/beneficiary_controller.dart';
import 'package:expenses/view/widgets/beneficiaries/beneficiary.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../utils/constants/colors.dart';
import '../../../widgets/shared/text_utils.dart';

class BeneficiariesScreen extends StatefulWidget {
  BeneficiariesScreen({super.key});

  @override
  State<BeneficiariesScreen> createState() => _BeneficiariesScreenState();
}

class _BeneficiariesScreenState extends State<BeneficiariesScreen> {
  final controller = Get.find<BeneficiaryController>();
  bool isSearching = false;
  final searchTextController = TextEditingController();

  void startSearch() {
    controller.filteredBeneficiariesList = controller.beneficiariesList;
    ModalRoute.of(context)!
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: stopSearching));

    setState(() {
      isSearching = true;
    });
  }

  void stopSearching() {
    clearSearch();

    setState(() {
      isSearching = false;
    });
  }

  void clearSearch() {
    setState(() {
      controller.filteredBeneficiariesList = controller.beneficiariesList;
      searchTextController.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: isSearching ? BackButton() : null,
        title: isSearching ? buildSearchField() : buildAppBarTitle(),
        actions: buildAppBarActions(),
      ),
      body: Container(
        width: double.maxFinite,
        child: Obx(() {
          if (controller.isLoading.value) {
            return Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
              ),
            );
          } else {
            var list = isSearching
                ? controller.filteredBeneficiariesList
                : controller.beneficiariesList;
            return controller.beneficiariesList.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) =>
                        Beneficiary(
                          beneficiary: list[index].beneficiary,
                          totalInCome: list[index].totalInCome,
                          totalOutCome: list[index].totalOutCome,
                        ),
                    itemCount: isSearching
                        ? controller.filteredBeneficiariesList.length
                        : controller.beneficiariesList.length)
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: double.infinity,
                        child: Image.asset(
                          'assets/images/ui/empty-folder.png',
                          height: 150,
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      TextUtils(
                        text: AppLocalizations.of(context)!.noBeneficiaries,
                        fontSize: 17,
                      ),
                    ],
                  );
          }
        }),
      ),
    );
  }

  Widget buildAppBarTitle() {
    return Text(AppLocalizations.of(context)!.beneficiaries);
  }

  List<Widget> buildAppBarActions() {
    if (isSearching) {
      return [
        IconButton(
          onPressed: () {
            clearSearch();
            Navigator.pop(context);
          },
          icon: Icon(CupertinoIcons.search),
        ),
      ];
    } else {
      return [
        IconButton(
          onPressed: startSearch,
          icon: Icon(
            CupertinoIcons.search,
          ),
        ),
      ];
    }
  }

  Widget buildSearchField() {
    return TextField(
      controller: searchTextController,
      cursorColor: ColorsManager.disabled,
      decoration: InputDecoration(
        hintText: AppLocalizations.of(context)!.searchBeneficiary,
        border: InputBorder.none,
      ),
      onChanged: (searchedCharacter) {
        controller.filterBeneficiaryList(searchedCharacter);
      },
    );
  }
}
