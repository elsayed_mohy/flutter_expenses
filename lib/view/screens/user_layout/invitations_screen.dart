import 'package:expenses/data/model/invitation_model.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/invitation_controller.dart';
import 'package:expenses/view/widgets/invitation_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';
import '../../widgets/shared/text_utils.dart';

class InvitationsScreen extends StatefulWidget {
  InvitationsScreen({super.key});

  @override
  State<InvitationsScreen> createState() => _InvitationsScreenState();
}

class _InvitationsScreenState extends State<InvitationsScreen>
    with TickerProviderStateMixin {
  late TabController tabController;
  final controller = Get.find<InvitationController>();

  // final controller = Get.put(InvitationController());
  RxList<Invitation> receivedInvitations = <Invitation>[].obs;
  RxList<Invitation> sentInvitations = <Invitation>[].obs;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.invitations),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: Column(
          children: [
            TabBar(
              indicatorColor: ThemesManager().isSavedDarkMode()
                  ? ColorsManager.secondary
                  : ColorsManager.main,
              controller: tabController,
              tabs: [
                Text(AppLocalizations.of(context)!.received),
                Text(AppLocalizations.of(context)!.sent),
              ],
              onTap: (index) {
                if (index == 1) {
                  setState(() {
                    sentInvitations.value =
                        controller.sentInvitationsList.value;
                  });
                }
              },
            ),
            Expanded(child: Obx(() {
              var receivedInvitations =
                  controller.receivedInvitationsList.value;
              if (controller.isInvitationsLoading.isTrue) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return Container(
                  width: double.maxFinite,
                  child: TabBarView(controller: tabController, children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: receivedInvitations.isNotEmpty
                          ? ListView.builder(
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) =>
                                  InvitationCard(
                                    invitation: receivedInvitations[index],
                                    sent: false,
                                  ),
                              itemCount: receivedInvitations.length)
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: double.infinity,
                                  child: Image.asset(
                                    'assets/images/ui/empty-folder.png',
                                    height: 150,
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                TextUtils(
                                  text: AppLocalizations.of(context)!
                                      .noReceived,
                                  fontSize: 17,
                                ),
                              ],
                            ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 12.0),
                      child: sentInvitations.isNotEmpty
                          ? ListView.builder(
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) =>
                                  InvitationCard(
                                    invitation: sentInvitations[index],
                                    sent: true,
                                  ),
                              itemCount: sentInvitations.length)
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: double.infinity,
                                  child: Image.asset(
                                    'assets/images/ui/empty-folder.png',
                                    height: 150,
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                TextUtils(
                                  text: AppLocalizations.of(context)!
                                      .noSent,
                                  fontSize: 17,
                                ),
                              ],
                            ),
                    ),
                  ]),
                );
              }
            })),
          ],
        ),
      ),
    );
  }
}
