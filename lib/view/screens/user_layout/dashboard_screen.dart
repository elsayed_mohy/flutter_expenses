import "package:expenses/l10n/app_localizations.dart";
import "package:expenses/view/widgets/dashboard/cat_chart.dart";
import "package:expenses/view/widgets/dashboard/dashboard_box_number_card.dart";
import "package:expenses/view/widgets/dashboard/dashboard_member_number_card.dart";
import "package:expenses/view/widgets/dashboard/dashboard_top_beneficiary_card.dart";
import "package:expenses/view/widgets/dashboard/dashboard_top_box_card.dart";
import "package:expenses/view/widgets/dashboard/dashboard_user_summary_card.dart";
import "package:expenses/view/widgets/dashboard/max_beneficiarybar_chart.dart";
import "package:expenses/view/widgets/dashboard/max_box_bar_chart.dart";
import "package:expenses/view/widgets/shared/background_paint.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";

import "../../../logic/controller/dashboard_controller.dart";
import "../../../utils/constants/colors.dart";
import "../../../utils/theme.dart";
import "../../widgets/dashboard/latest_transactions.dart";
import "../../widgets/dashboard/statics_by_category.dart";
import "../../widgets/shared/wave_background.dart";

class DashboardScreen extends GetView<DashboardController> {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.dashboard),
        // backgroundColor: ThemesManager().isSavedDarkMode()
        //     ? ColorsManager.svgDark
        //     : ColorsManager.svgLight,
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Stack(
                children: [
                  BackgroundPaint(height: 250,),
                  Column(
                    children: [
                      SizedBox(
                          height: 220,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                DashboardUserSummaryCard(),
                                DashboardTopBeneficiaryCard(),
                                DashboardTopBoxCard(),
                                DashboardBoxNumberCard(),
                                DashboardMemberNumberCard(),
                              ],
                            ),
                          )),
                      SizedBox(
                        height: 20,
                      ),
                      LatestTransactions(),
                      SizedBox(
                        height: 20,
                      ),
            GetBuilder<DashboardController>(
                init: DashboardController(),
                builder: (DashboardController controller) {
                  if(controller.isLoadingBeneficiaryChart){
                    return Center(child: CircularProgressIndicator(color: Colors.blue,),);
                  }
                  return MaxBeneficiaryBarChart(
                    title: AppLocalizations.of(context)!.topBeneficiary,
                    barColor: ColorsManager.success, data: controller.beneficiaryChartList,
                  );
                }
            ),
                      SizedBox(
                        height: 10,
                      ),
                      GetBuilder<DashboardController>(
                          init: DashboardController(),
                          builder: (DashboardController controller) {
                            if(controller.isLoadingBoxChart){
                              return Center(child: CircularProgressIndicator(color: Colors.blue,),);
                            }
                            return MaxBoxBarChart(
                              title: AppLocalizations.of(context)!.topBox,
                              barColor: ColorsManager.warning, data: controller.boxChartList,
                            );
                          }
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      GetBuilder<DashboardController>(
                          init: DashboardController(),
                          builder: (DashboardController controller) {
                            if(controller.isLoadingBoxChart){
                              return Center(child: CircularProgressIndicator(color: Colors.blue,),);
                            }
                            return CatChart(
                              title: AppLocalizations.of(context)!.topCategory,
                              barColor: ColorsManager.warning, data: controller.categoriesChartList,
                            );
                          }
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      //CatChart
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
