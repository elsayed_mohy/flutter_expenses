import 'package:expenses/data/model/user.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/profile_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/theme.dart';
import 'package:expenses/view/widgets/profile/change_language_bottom_sheet.dart';
import 'package:expenses/view/widgets/profile/change_password_bottom_sheet.dart';
import 'package:expenses/view/widgets/profile/edit_profile_bottom_sheet.dart';
import 'package:expenses/view/widgets/profile/logout_bottom_sheet.dart';
import 'package:expenses/view/widgets/shared/get_bottom_sheet.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../utils/constants/app_urls.dart';
import '../../../widgets/shared/custom_list_tile.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({super.key});

  final controller = Get.find<ProfileController>();

  late User user = controller.user;

  editProfile() {
    getBottomSheet(400, EditProfileBottomSheet());
  }

  changePassword() {
    getBottomSheet(550, ChangePasswordBottomSheet());
  }

  changeLanguage() {
    getBottomSheet(350, ChangeLanguageBottomSheet());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.theme.backgroundColor,
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.more),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 5, right: 20, left: 20),
        child: GetBuilder<ProfileController>(
          builder: (_) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 100,
                      height: 100,
                      child: Stack(
                        fit: StackFit.expand,
                        children: [
                          Container(
                            padding: EdgeInsets.zero,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              shape: BoxShape.circle,
                            ),
                            child:
                            Image.network(
                              controller.user.imagePath != null
                                  ? AppUrls.baseImageUrl +
                                  controller.user.imagePath
                                  : '',
                              fit: BoxFit.cover,
                              errorBuilder: (BuildContext context,
                                  Object exception,
                                  StackTrace? stackTrace) {
                                return Image.asset(
                                  'assets/images/avatar/avatar1.png',
                                  fit: BoxFit.cover,
                                );
                              },
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: CircleAvatar(
                              radius: 14,
                              backgroundColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                              child: Container(
                                margin: const EdgeInsets.all(2.0),
                                decoration: const BoxDecoration(
                                    color: ColorsManager.main,
                                    shape: BoxShape.circle),
                                child: IconButton(
                                  icon: Icon(Icons.edit),
                                  iconSize: 18,
                                  splashRadius: 25,
                                  padding: EdgeInsets.zero,
                                  color: Colors.white,
                                  onPressed: () {
                                    controller.pickProfileImage();
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Column(
                        children: [
                          TextUtils(
                            text: user.fullName,
                            fontWeight: FontWeight.w500,
                            fontSize: 22,
                          ),
                          const SizedBox(height: 2),
                          TextUtils(
                            text: user.phoneNumber,
                            fontSize: 13,
                          ),
                          // const _ProfileInfoRow()
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border(
                      top: BorderSide(
                        //                    <--- top side
                        color: ColorsManager.disabled.withOpacity(0.5),
                        width: 1.0,
                      ),
                    )),
                    // constraints: const BoxConstraints(maxWidth: 350),
                    child: ListView(
                      children: [
                         CustomListTile(
                            title: AppLocalizations.of(context)!.editProfile,
                            icon: CupertinoIcons.person,
                        action:editProfile
                        ),
                        CustomListTile(
                            title: AppLocalizations.of(context)!.changePassword,
                            icon: CupertinoIcons.lock,
                            action:changePassword
                        ),
                        CustomListTile(
                            title: AppLocalizations.of(context)!.changeLanguage,
                            icon: CupertinoIcons.text_bubble,
                            action:changeLanguage),
                        CustomListTile(
                          title: AppLocalizations.of(context)!.notifications,
                          icon: CupertinoIcons.bell,
                          action: () {},
                        ),

                        CustomListTile(
                            title: AppLocalizations.of(context)!.darkMode,
                            icon: CupertinoIcons.moon,
                            trailing: Switch(
                              value: ThemesManager().isSavedDarkMode(),
                              onChanged: (value) {
                                ThemesManager().changeTheme();
                              },
                            ),
                            action: () {}),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Flex(direction: Axis.horizontal, children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextButton(
                        onPressed: () {
                          getBottomSheet(200, LogoutBottomSheet());
                        },
                        child: Row(
                          children: [
                            Icon(Icons.output_rounded,
                                color: ColorsManager.warning),
                            SizedBox(width: 5),
                            TextUtils(
                              text: AppLocalizations.of(context)!.logout,
                              color: ColorsManager.warning,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
                )
              ],
            );
          }
        ),
      ),
    );
  }
}
