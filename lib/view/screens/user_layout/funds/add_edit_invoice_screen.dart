import 'dart:io';

import 'package:expenses/data/model/invoice/invoice_model.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/theme.dart';
import 'package:expenses/view/widgets/shared/add_button.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:open_app_file/open_app_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../widgets/auth/auth_button.dart';
import '../../../widgets/auth/auth_text_form_field.dart';
import '../../../widgets/funds/box_details_popup_menu.dart';

enum attachActions { delete, save, share }

class AddEditInvoiceScreen extends StatelessWidget {
  File? image;
  final c = Get.find<BoxController>();

  AddEditInvoiceScreen({super.key});

  final TextEditingController amountController = TextEditingController();
  final TextEditingController noteController = TextEditingController();
  final TextEditingController beneficiaryController = TextEditingController();
  final TextEditingController tagController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  // final controller = Get.find<InvoiceController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(c.isInvoiceInUpdate
              ? AppLocalizations.of(context)!.editInvoice
              : AppLocalizations.of(context)!.addInvoice),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: GetBuilder<BoxController>(
              init: BoxController(),
              builder: (controller) {
                if (controller.isInvoiceItemsLoading ||
                    controller.isInvoiceLoading) {
                  return Center(
                    child: CircularProgressIndicator(
                      color: Colors.blue,
                    ),
                  );
                } else {
                  if (controller.isInvoiceInUpdate) {
                    var invoiceDetails = controller.invoiceModel;
                  }

                  if (controller.isInvoiceInUpdate) {
                    amountController.text =
                        controller.invoiceModel.amount.toString();
                    noteController.text = controller.invoiceModel.notes;
                    beneficiaryController.text =
                        controller.invoiceModel.beneficiary;
                    tagController.text = controller.invoiceModel.tag;
                  }

                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Form(
                          key: formKey,
                          child: Column(
                            children: [
                              const SizedBox(height: 25),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                child: AuthTextFormField(
                                  controller: amountController,
                                  obscureText: false,
                                  keyboardType: TextInputType.number,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return AppLocalizations.of(context)!
                                          .amountRequired;
                                    }
                                    return null;
                                  },
                                  prefixIcon: Icons.attach_money,
                                  hintText:
                                      AppLocalizations.of(context)!.amount,
                                ),
                              ),
                              const SizedBox(height: 20),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                child: _textForm(
                                    hintText: AppLocalizations.of(context)!
                                        .addBeneficiary,
                                    list: controller.invoiceBeneficiaryList,
                                    inputHint: AppLocalizations.of(context)!
                                        .addBeneficiary,
                                    validateMessage:
                                        AppLocalizations.of(context)!
                                            .beneficiaryRequired,
                                    icon: Icons.person,
                                    txtEditingController: beneficiaryController,
                                    valueOnEdit: controller.isInvoiceInUpdate
                                        ? controller.invoiceModel.beneficiary
                                        : ''),
                              ),
                              const SizedBox(height: 20),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                child: _textForm(
                                    hintText: AppLocalizations.of(context)!.tag,
                                    list: controller.invoiceTagList,
                                    inputHint:
                                        AppLocalizations.of(context)!.tag,
                                    validateMessage:
                                        AppLocalizations.of(context)!
                                            .tagRequired,
                                    icon: Icons.category,
                                    txtEditingController: tagController,
                                    valueOnEdit: controller.isInvoiceInUpdate
                                        ? controller.invoiceModel.tag
                                        : ''),
                              ),
                              const SizedBox(height: 20),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                child: AuthTextFormField(
                                  controller: noteController,
                                  obscureText: false,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return AppLocalizations.of(context)!
                                          .descriptionRequired;
                                    }
                                    return null;
                                  },
                                  prefixIcon: Icons.notes,
                                  hintText:
                                      AppLocalizations.of(context)!.description,
                                ),
                              ),
                              const SizedBox(height: 15),
                              if (controller.isInvoiceInUpdate)
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 25),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      TextUtils(
                                        text: AppLocalizations.of(context)!
                                            .invoiceImages,
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                      ),
                                      SizedBox(height: 8),
                                      SizedBox(
                                        height: 80,
                                        child: Row(
                                          children: [
                                            AddButton(action: () {
                                              controller.pickImage(
                                                  controller.invoiceModel.id);
                                            }),
                                            SizedBox(width: 15),
                                            Expanded(
                                              child: ListView.separated(
                                                shrinkWrap: true,
                                                clipBehavior:
                                                    Clip.antiAliasWithSaveLayer,
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemBuilder: (BuildContext
                                                            context,
                                                        int index) =>
                                                    invoiceImage(controller
                                                        .invoiceModel
                                                        .attachments[index]),
                                                itemCount: controller
                                                    .invoiceModel
                                                    .attachments
                                                    .length,
                                                separatorBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return SizedBox(width: 15);
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              const SizedBox(height: 15),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 25),
                                child: AuthButton(
                                  text: AppLocalizations.of(context)!.save,
                                  onPressed: () {
                                    if (formKey.currentState!.validate()) {
                                      controller.invoice(
                                          amount: double.parse(
                                              amountController.text),
                                          beneficiary:
                                              beneficiaryController.text,
                                          tag: tagController.text,
                                          note: noteController.text,
                                          isUpdate:
                                              controller.isInvoiceInUpdate);
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                }
              }),
        ));
  }

  Widget _textForm(
      {required String hintText,
      required List<String> list,
      required String inputHint,
      required String validateMessage,
      required IconData icon,
      required TextEditingController txtEditingController,
      required String valueOnEdit}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextUtils(
          text: hintText,
          align: TextAlign.start,
          fontWeight: FontWeight.w500,
        ),
        SizedBox(
          height: 10,
        ),
        Autocomplete<String>(
          optionsBuilder: (TextEditingValue textEditingValue) {
            if (textEditingValue.text.isEmpty) {
              return List.empty();
            } else {
              Iterable<String> lst =  list.where((element) => element
                  .toLowerCase()
                  .contains(textEditingValue.text.toLowerCase()));
              if(lst.isNotEmpty){
                return lst;
              }else{
                Set<String> set = {};
                set.add(textEditingValue.text.toLowerCase());
                List<String> stList = set.toList();
                return stList.where((element) => element
                    .toLowerCase()
                    .contains(textEditingValue.text.toLowerCase()));
              }
            }
          },
          onSelected: (option) {
            txtEditingController.text = option;
          },
          fieldViewBuilder:
              (context, txtEditingController, focusNode, onFieldSubmitted) {
            txtEditingController.text =
                valueOnEdit.isNotEmpty ? valueOnEdit : "";
            return TextFormField(
              focusNode: focusNode,
              controller: txtEditingController,
              obscureText: false,
              cursorColor: ThemesManager().isSavedDarkMode()
                  ? ColorsManager.main
                  : ColorsManager.darkGrey,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  icon,
                  color: ColorsManager.main,
                  size: 20,
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: Colors.transparent,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                filled: true,
                suffixIconColor: ColorsManager.main,
                hintText: inputHint,
                hintStyle: const TextStyle(
                    color: ColorsManager.disabled, fontSize: 14),
                labelStyle: const TextStyle(
                    color: ColorsManager.darkGrey, fontSize: 10),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorsManager.main,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorsManager.warning,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorsManager.warning,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return validateMessage;
                }
                return null;
              },
            );
          },
        ),
      ],
    );
  }

  Future pickImage() async {
    try {
      XFile? image = await ImagePicker()
          .pickImage(source: ImageSource.gallery, imageQuality: 30);
      if (image == null) return;
      final imageTemp = File(image.path);
      //ConvertImage(imageTemporary);
    } on PlatformException catch (e) {
      print(e);
    }
  }

  Widget invoiceImage(Attachment attachment) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            padding: EdgeInsets.zero,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            decoration: BoxDecoration(
              color: ColorsManager.disabled,
              shape: BoxShape.circle,
            ),
            child: Image.network(
              "${AppUrls.baseImageUrl}${attachment.imagePath}",
              fit: BoxFit.cover,
              errorBuilder: (BuildContext context, Object exception,
                  StackTrace? stackTrace) {
                return Image.asset(
                  'assets/images/avatar/avatar1.png',
                  fit: BoxFit.cover,
                );
              },
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: CircleAvatar(
              radius: 13,
              backgroundColor: ColorsManager.darkDisabled,
              child: PopupMenuButton<attachActions>(
                padding: EdgeInsets.zero,
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: attachActions.delete,
                    child: TextUtils(
                      text: AppLocalizations.of(context)!.remove,
                    ),
                  ),
                  PopupMenuItem(
                    value: attachActions.save,
                    child: TextUtils(
                      text: AppLocalizations.of(context)!.download,
                    ),
                  ),
                  PopupMenuItem(
                    value: attachActions.share,
                    child: TextUtils(
                      text: AppLocalizations.of(context)!.shareToWhatsapp,
                    ),
                  ),
                ],
                onSelected: (value) async {
                  switch (value) {
                    case attachActions.delete:
                      c.deleteInvoiceAttach(attachment.id);
                      break;
                    case attachActions.save:
                      c.f(AppUrls.baseImageUrl + attachment.imagePath);
                      // link.href = environment.baseImageUrl + at.imagePath;
                      break;
                    case attachActions.share:
                      print("share");
                      shareToWhatsApp(
                          text: 'Check out this link',
                          imageUrl:
                              AppUrls.baseImageUrl + attachment.imagePath);
                      break;
                  }
                  // var subBoxes = controller.getSubscriptionBoxes();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void shareToWhatsApp({required String text, required String imageUrl}) async {
    // Uri url = Uri.parse('whatsapp://send?text=${Uri.encodeFull(text)}&image=${Uri.encodeFull(imageUrl)}');
    //
    // if (await canLaunchUrl(url)) {
    //   await launchUrl(url);
    // } else {
    //   throw 'Could not launch $url';
    // }
    await Share.share(imageUrl);
  }
}
