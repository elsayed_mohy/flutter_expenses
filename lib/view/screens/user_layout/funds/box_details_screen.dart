import 'package:expenses/data/model/box/box_details_model.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/utils/constants/ui.dart';
import 'package:expenses/view/widgets/funds/box_details_card.dart';
import 'package:expenses/view/widgets/funds/invoice.dart';
import 'package:expenses/view/widgets/funds/invoice_type_card.dart';
import 'package:expenses/view/widgets/funds/invoices_list.dart';
import 'package:expenses/view/widgets/shared/add_button.dart';
import 'package:expenses/view/widgets/shared/card_container.dart';
import 'package:expenses/view/widgets/shared/wave_background.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../routes/routes.dart';
import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';
import '../../../widgets/funds/box_details_popup_menu.dart';
import '../../../widgets/shared/background_paint.dart';
import '../../../widgets/shared/side_background_paint.dart';
import '../../../widgets/shared/text_utils.dart';

class BoxDetailsScreen extends StatefulWidget {
  BoxDetailsScreen({super.key});

  @override
  State<BoxDetailsScreen> createState() => _BoxDetailsScreenState();
}

class _BoxDetailsScreenState extends State<BoxDetailsScreen> {
  final controller = Get.find<BoxController>();

  bool isIncome = true;

  bool isExpense = false;

  setIsIncome() {
    setState(() {
      isExpense = false;
      isIncome = true;
      controller.invoiceIsExpense(isExpense: false);
      controller.getInvoiceItems();
      controller.invoiceInUpdate(isInUpdate: false);
      Get.toNamed(
          Routes.home + Routes.addEditInvoiceScreen);
    });

  }

  setIsExpense() {
    setState(() {
      isIncome = false;
      isExpense = true;
      controller.invoiceIsExpense(isExpense: true);
      controller.getInvoiceItems();
      controller.invoiceInUpdate(isInUpdate: false);
      Get.toNamed(
          Routes.home + Routes.addEditInvoiceScreen);
    });

  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BoxController>(builder: (_) {
      if (controller.isBoxLoading) {
        return Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      } else {
        var boxDetails = controller.boxDetails;
        return Scaffold(
          appBar: AppBar(
            title: Text(controller.boxDetails.name ?? ''),
            actions: [BoxDetailsPopupMenu(editable: boxDetails.edetable,)],
          ),
          body: SingleChildScrollView(
            child: Stack(children: [
              BackgroundPaint(),
              Column(
                children: [
                  if(boxDetails.edetable)
                  Padding(
                    padding: const EdgeInsets.only(top: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InvoiceTypeCard(
                          selected: isIncome,
                          income: true, action: setIsIncome, value: boxDetails.totalIncome,
                        ),
                        InvoiceTypeCard(
                          selected: isExpense,
                          income: false, action: setIsExpense, value: boxDetails.totalExpense,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                      height: 115,
                      margin: EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        boxShadow: [
                          BoxShadow(
                            color: ColorsManager.neonBlue.withOpacity(0.4),
                            blurRadius: 40,
                            offset: Offset(0, 10),
                            spreadRadius: 0,
                          )
                        ],
                      ),

                      child:  ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        child: Stack(
                          children: [
                            Container(
                            color: ColorsManager.basic,),
                            SideBackgroundPaint(),
                            Container(
                              color: Color.fromRGBO(35, 35, 35, 0.1)),
                            Container(
                              width: double.infinity,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10.0,horizontal: 12),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                        Row(
                                          children: [
                                            Icon(CupertinoIcons.money_dollar,color: Colors.white,size: 25,),

                                            SizedBox(width: 10),
                                            TextUtils(text: "${AppLocalizations.of(context)!.currentBalance} :",
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500,),
                                            SizedBox(width: 10),
                                            TextUtils(text: boxDetails.currentBalance.toString(),
                                              color: Colors.white,
                                              fontSize: 22,
                                              fontWeight: FontWeight.w500,),
                                          ],
                                        ),
                                        SizedBox(height: 10),
                                        Row(
                                          children: [
                                            Icon(CupertinoIcons.person_3_fill,color: Colors.white,size: 25),
                                            SizedBox(width: 10),
                                            TextUtils(text: "${AppLocalizations.of(context)!.members} :",
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500,),

                                            SizedBox(width: 10),
                                            TextUtils(text: boxDetails.members.length.toString(),
                                              color: ColorsManager.white,
                                              fontSize: 22,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ],
                                        ),
                                      ],),
                                    ),
                                    // Expanded(
                                    //   child: Column(children: [
                                    //     Row(
                                    //       children: [
                                    //         TextUtils(text: "${AppLocalizations.of(context)!.totalIncome} :",
                                    //           color: Colors.white,
                                    //           fontWeight: FontWeight.w500,),
                                    //         SizedBox(width: 10),
                                    //         TextUtils(text: boxDetails.totalIncome.toString(),
                                    //           color: ColorsManager.success,
                                    //           fontSize: 22,
                                    //           fontWeight: FontWeight.w500,),
                                    //       ],
                                    //     ),
                                    //     SizedBox(height: 20),
                                    //     Row(
                                    //       children: [
                                    //         TextUtils(text: "${AppLocalizations.of(context)!.totalExpense} :",
                                    //           color: Colors.white,
                                    //           fontWeight: FontWeight.w500,),
                                    //         SizedBox(width: 10),
                                    //         TextUtils(text: boxDetails.totalExpense.toString(),
                                    //           color: ColorsManager.warning,
                                    //           fontSize: 22,
                                    //           fontWeight: FontWeight.w500,),
                                    //       ],
                                    //     ),
                                    //   ],),
                                    // ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  InvoicesList(
                    invoicesList: controller.invoicesList,
                    boxId:boxDetails.id,
                  ),
                ],
              ),
            ]),
          ),
        );
      }
    });
  }
}
