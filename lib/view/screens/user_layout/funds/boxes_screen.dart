import "package:expenses/data/model/box_model.dart";
import "package:expenses/l10n/app_localizations.dart";
import "package:expenses/logic/controller/box_controller.dart";
import "package:expenses/utils/constants/colors.dart";
import "package:expenses/view/widgets/auth/auth_button.dart";
import 'package:expenses/view/widgets/funds/box.dart';
import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";

import "../../../../utils/theme.dart";
import "../../../widgets/funds/add_box_bottom_sheet.dart";
import "../../../widgets/funds/edit_box_bottom_sheet.dart";
import "../../../widgets/shared/get_bottom_sheet.dart";
import "../../../widgets/shared/primary_button.dart";
import "../../../widgets/shared/text_utils.dart";

class BoxesScreen extends StatefulWidget {
  const BoxesScreen({super.key});

  @override
  State<BoxesScreen> createState() => _BoxesScreenState();
}

class _BoxesScreenState extends State<BoxesScreen>
    with TickerProviderStateMixin {
  late TabController tabController;

  // final controller = Get.find<BoxController>();
  final controller = Get.put(BoxController());
  bool isSearching = false;
  bool isAdmin = true;
  final searchTextController = TextEditingController();

  // List<BoxModel> boxes = [];
  List<BoxModel> subscriptionBoxes = [];

  void startSearch() {
    ModalRoute.of(context)!
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: stopSearching));

    setState(() {
      controller.filteredSubBoxesList.assignAll(controller.subBoxesList);
      controller.filteredBoxesList.assignAll(controller.boxesList);
      isSearching = true;
    });
  }

  void stopSearching() {
    clearSearch();

    setState(() {
      isSearching = false;
    });
  }

  void clearSearch() {
    setState(() {
      searchTextController.clear();
      controller.filteredSubBoxesList.assignAll(controller.subBoxesList);
      controller.filteredBoxesList.assignAll(controller.boxesList);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: isSearching ? BackButton() : null,
        title: isSearching ? buildSearchField() : buildAppBarTitle(),
        actions: buildAppBarActions(),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 13.0),
        child: Column(
          children: [
            TabBar(
              indicatorColor: ThemesManager().isSavedDarkMode()
                  ? ColorsManager.secondary
                  : ColorsManager.main,
              // padding: EdgeInsets.symmetric(vertical: 10),
              controller: tabController,
              tabs: [
                Text(AppLocalizations.of(context)!.myFunds),
                Text(AppLocalizations.of(context)!.subscriptions),
              ],
              onTap: (index) {
                if (index == 1) {
                  setState(() {
                    isAdmin = false;
                    subscriptionBoxes = controller.subBoxesList;
                  });
                } else {
                  setState(() {
                    isAdmin = true;
                  });
                }
              },
            ),
            Expanded(child: Obx(
              () {
                var boxes = isSearching
                    ? controller.filteredBoxesList
                    : controller.boxesList;
                if (controller.isLoading.value) {
                  return Center(
                    child: CircularProgressIndicator(
                      color: Colors.blue,
                    ),
                  );
                } else {
                  return Container(
                    width: double.maxFinite,
                    child: TabBarView(controller: tabController, children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: controller.boxesList.isNotEmpty
                            ? SingleChildScrollView(
                                child: Column(
                                  children: [
                                    if (isSearching == false)
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 12),
                                        child: SizedBox(
                                          width: 180,
                                          height: 45,
                                          child: AuthButton(
                                              text:
                                                  AppLocalizations.of(context)!
                                                      .addFund,
                                              onPressed: () {
                                                getBottomSheet(
                                                    350, AddBoxBottomSheet());
                                              }),
                                        ),
                                      ),

                                      ListView.builder(
                                          physics: ClampingScrollPhysics(),
                                          shrinkWrap: true,
                                          itemBuilder: (BuildContext context,
                                                  int index) =>
                                              Box(
                                                box: boxes[index],
                                              ),
                                          itemCount: boxes.length),
                                  ],
                                ),
                              )
                            : Center(
                                child: SingleChildScrollView(
                                  child: Column(
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: double.infinity,
                                            child: Image.asset(
                                              'assets/images/ui/empty-box.png',
                                              height: 150,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          TextUtils(
                                            text: AppLocalizations.of(context)!
                                                .noFunds,
                                            fontSize: 17,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10.0, vertical: 12),
                                        child: SizedBox(
                                          width: 180,
                                          height: 45,
                                          child: AuthButton(
                                              text:
                                                  AppLocalizations.of(context)!
                                                      .addFund,
                                              onPressed: () {
                                                getBottomSheet(
                                                    350, AddBoxBottomSheet());
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: controller.subBoxesList.isEmpty
                            ? Center(
                                child: SingleChildScrollView(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        child: Image.asset(
                                          'assets/images/ui/empty-box.png',
                                          height: 150,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      TextUtils(
                                        text: AppLocalizations.of(context)!
                                            .noFunds,
                                        fontSize: 17,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : SingleChildScrollView(
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    itemBuilder: (BuildContext context,
                                            int index) =>
                                        Box(
                                            box: isSearching
                                                ? controller
                                                    .filteredSubBoxesList[index]
                                                : subscriptionBoxes[index]),
                                    itemCount: isSearching
                                        ? controller.filteredSubBoxesList.length
                                        : subscriptionBoxes.length),
                              ),
                      ),
                    ]),
                  );
                }
                // var subBoxes = controller.getSubscriptionBoxes();
              },
            )),
          ],
        ),
      ),
    );
  }

  Widget buildAppBarTitle() {
    return Text(AppLocalizations.of(context)!.myFunds);
  }

  List<Widget> buildAppBarActions() {
    if (isSearching) {
      return [
        IconButton(
          onPressed: () {
            clearSearch();
            Navigator.pop(context);
          },
          icon: Icon(CupertinoIcons.search),
        ),
      ];
    } else {
      return [
        // IconButton(
        //   onPressed: () {
        //     getBottomSheet(350, AddBoxBottomSheet());
        //   },
        //   icon: Icon(CupertinoIcons.add_circled),
        // ),
        IconButton(
          onPressed: startSearch,
          icon: Icon(
            CupertinoIcons.search,
          ),
        ),
      ];
    }
  }

  Widget buildSearchField() {
    return TextField(
      controller: searchTextController,
      cursorColor: ColorsManager.disabled,
      decoration: InputDecoration(
        hintText: AppLocalizations.of(context)!.searchFund,
        border: InputBorder.none,
      ),
      onChanged: (searchedCharacter) {
        isAdmin
            ? controller.filterBoxesList(searchedCharacter)
            : controller.filterSubBoxesList(searchedCharacter);
      },
    );
  }
}

// class BoxTabs extends GetxController with GetSingleTickerProviderStateMixin {
//   late TabController tabController;
//   final String firstTab;
//   final String secondTab;
//
//   BoxTabs(this.firstTab ,this.secondTab);
//
//   final List<Tab> tabs = <Tab>[
//     Tab(
//       child:
//       Text(AppLocalizations.of(newContext)!.myFunds),
//     ),
//     Tab(
//       child:
//       Text(AppLocalizations.of(context)!.subscriptions),
//     )
//
//   ];
//
// }
