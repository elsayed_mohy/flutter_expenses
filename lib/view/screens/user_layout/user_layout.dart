import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/screens/user_layout/dashboard_screen.dart';
import 'package:expenses/view/screens/user_layout/funds/boxes_screen.dart';
import 'package:expenses/view/screens/user_layout/invitations_screen.dart';
import 'package:expenses/view/screens/user_layout/profile/profile_screen.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

import '../../../utils/theme.dart';
import 'beneficiaries/beneficiaries_screen.dart';

class UserLayout extends StatefulWidget {
  const UserLayout({super.key});

  static final List<Widget> _widgetOptions = <Widget>[
    DashboardScreen(),
    InvitationsScreen(),
    BoxesScreen(),
    BeneficiariesScreen(),
    ProfileScreen(),
  ];

  @override
  State<UserLayout> createState() => _UserLayoutState();
}

class _UserLayoutState extends State<UserLayout> {
  int currentTab = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.theme.colorScheme.background,
      // appBar: AppBar(
      //   elevation: 0,
      //   actions: [
      //     Padding(
      //       padding: const EdgeInsets.all(8.0),
      //       child: Icon(LineIcons.user),
      //     ),],
      //   title: const Text('username'),
      // ),
      body: Center(
        child: UserLayout._widgetOptions.elementAt(currentTab),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: ColorsManager.main,
        elevation: 0,
        onPressed: () {
          setState(() {
            currentTab = 2;
          });
        },
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomAppBar(
        color:ThemesManager().isSavedDarkMode() ? ColorsManager.svgDark :Colors.white,
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: SizedBox(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 0;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon( currentTab == 0 ? CupertinoIcons.square_grid_2x2_fill : CupertinoIcons.square_grid_2x2),
                          TextUtils( text: AppLocalizations.of(context)!.dashboard,fontSize: 11)

                          //const Padding(padding: EdgeInsets.all(10))
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 3;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(currentTab == 3 ? CupertinoIcons.person_3_fill : CupertinoIcons.person_3),
                          TextUtils( text: AppLocalizations.of(context)!.beneficiaries,fontSize: 11)
                          //const Padding(padding: EdgeInsets.only(left: 10))
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(child: SizedBox()),
                Expanded(
                  child: Center(
                    child: InkWell(
                        focusColor: Colors.transparent,
                        hoverColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          setState(() {
                            currentTab = 1;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(currentTab == 1 ? CupertinoIcons.mail_solid : CupertinoIcons.mail),
                            TextUtils( text: AppLocalizations.of(context)!.invitations,fontSize: 11)
                            //const Padding(padding: EdgeInsets.only(right: 10))
                          ],
                        )),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: InkWell(
                      focusColor: Colors.transparent,
                      splashColor:Colors.transparent,
                      hoverColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      onTap: () {
                        setState(() {
                          currentTab = 4;
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(currentTab == 4 ? CupertinoIcons.circle_grid_3x3_fill : CupertinoIcons.circle_grid_3x3),
                          TextUtils( text: AppLocalizations.of(context)!.more,fontSize: 11)

                          //const Padding(padding: EdgeInsets.only(left: 10))
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
