import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/theme.dart';
import 'package:expenses/view/widgets/funds/add_member_bottom_sheet.dart';
import 'package:expenses/view/widgets/funds/edit_box_bottom_sheet.dart';
import 'package:expenses/view/widgets/funds/members_bottom_sheet.dart';
import 'package:expenses/view/widgets/shared/primary_button.dart';
import 'package:expenses/view/widgets/shared/secondary_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../shared/get_bottom_sheet.dart';
import '../shared/text_utils.dart';

enum BoxActions { edit, delete, statement, addMember, members }

class BoxDetailsPopupMenu extends StatelessWidget {
  BoxDetailsPopupMenu({super.key, required this.editable});
  final bool editable;

  final controller = Get.find<BoxController>();

  openConfirmDeleteDialog(BuildContext context) {
    Get.defaultDialog(
      title: AppLocalizations.of(context)!.deleteBox,
      onConfirm: () {},
      contentPadding: EdgeInsets.all(12),
      buttonColor: ColorsManager.main,
      middleText: AppLocalizations.of(context)!.deleteBoxMsg,
      cancel: SecondaryButton(
        onPressed: () {
          Get.back();
        },
        text: AppLocalizations.of(context)!.cancel,
      ),
      confirm: PrimaryButton(
        onPressed: () {
          controller.deleteBox();
        },
        text: AppLocalizations.of(context)!.remove,
        disabled: false,
      ),
      confirmTextColor: ThemesManager().isSavedDarkMode()
          ? ColorsManager.white
          : ColorsManager.white,
      cancelTextColor: ThemesManager().isSavedDarkMode()
          ? ColorsManager.white
          : ColorsManager.darkGrey,
      onCancel: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<BoxActions>(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      itemBuilder: (context) => [

        PopupMenuItem(
          value: BoxActions.statement,
          child: TextUtils(
            text: AppLocalizations.of(context)!.report,
          ),
        ),
        PopupMenuItem(
          value: BoxActions.members,
          child: TextUtils(
            text: AppLocalizations.of(context)!.members,
          ),
        ),
        if(editable)
        PopupMenuItem(
          value: BoxActions.addMember,
          child: TextUtils(
            text: AppLocalizations.of(context)!.addMember,
          ),
        ),
        if(editable)
          PopupMenuItem(
            value: BoxActions.edit,
            child: TextUtils(
              text: AppLocalizations.of(context)!.edit,
            ),
          ),
        if(editable)
          PopupMenuItem(
            value: BoxActions.delete,
            child: TextUtils(
              text: AppLocalizations.of(context)!.remove,
            ),
          ),
      ],
      onSelected: (value) async {
        switch (value) {
          case BoxActions.edit:
            getBottomSheet(350, EditBoxBottomSheet());
            break;
          case BoxActions.delete:
            openConfirmDeleteDialog(context);
            break;
          case BoxActions.statement:
            controller.downloadPdf();
            break;
          case BoxActions.addMember:
            getBottomSheet(
                MediaQuery.of(context).size.height,
                FractionallySizedBox(
                  widthFactor: 0.99, // between 0 and 1
                  heightFactor: 0.9,
                  child: AddMemberBottomSheet(),
                ));
            break;
          case BoxActions.members:
            controller.getBoxMembers();
            getBottomSheet(
                MediaQuery.of(context).size.height,
                FractionallySizedBox(
                  widthFactor: 0.99, // between 0 and 1
                  heightFactor: 0.9,
                  child: MembersBottomSheet(),
                ));
            break;
        }
        // var subBoxes = controller.getSubscriptionBoxes();
      },
    );
  }
}
