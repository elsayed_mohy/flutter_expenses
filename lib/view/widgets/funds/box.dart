import 'package:expenses/data/model/box_model.dart';
import 'package:expenses/routes/routes.dart';
import 'package:expenses/view/widgets/shared/card_container.dart';
import 'package:expenses/view/widgets/shared/card_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../l10n/app_localizations.dart';
import '../../../logic/controller/box_controller.dart';
import '../../../utils/constants/colors.dart';
import '../shared/text_utils.dart';

class Box extends StatelessWidget {
  Box({super.key, required this.box});

  final controller = Get.find<BoxController>();

  BoxModel box;

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: InkWell(
        onTap: () {
          controller.getBoxDetails(box.id, box.edetable);
          // controller.filterInvoices(box.id);
          Get.toNamed("${Routes.home}${Routes.boxDetailsScreen}");
        },
        child: CardContainer(
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextUtils(
                            text: box.name,
                            fontSize: 20,
                            fontWeight: FontWeight.w500),
                        if(box.highestTag != null && box.highestTag != '')
                        Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 7),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color:
                                    ColorsManager.secondary.withOpacity(0.25)),
                            child:
                                TextUtils(text: box.highestTag, fontSize: 10)),
                      ],
                    ),
                    SizedBox(height: 12),
                    Row(
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              TextUtils(
                                  text: AppLocalizations.of(context)!
                                      .currentBalance,
                                  color: ColorsManager.disabled),
                              SizedBox(
                                width: 20,
                              ),
                              TextUtils(
                                text: box.currentBalance.toString(),
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 1,horizontal: 7),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: ColorsManager.success,width: 2),
                              borderRadius: BorderRadius.circular(10)),
                          child: TextUtils(
                            text: box.totalIncome.toString(),
                            fontSize: 13,
                            color: ColorsManager.success,
                            fontWeight: FontWeight.w600,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 6),
                    Row(
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              TextUtils(
                                  text: AppLocalizations.of(context)!.members,
                                  color: ColorsManager.disabled),
                              SizedBox(
                                width: 20,
                              ),
                              TextUtils(
                                text: "${box.numberOfMembers}",
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 1,horizontal: 7),
                          decoration: BoxDecoration(
                              border:
                              Border.all(color: ColorsManager.warning,width: 2),
                              borderRadius: BorderRadius.circular(10)),
                          child: TextUtils(
                            text: box.totalExpense.toString(),
                            fontSize: 13,
                            color: ColorsManager.warning,
                            fontWeight: FontWeight.w600,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
