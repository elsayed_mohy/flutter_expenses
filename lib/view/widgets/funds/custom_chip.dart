import 'package:expenses/data/model/beneficiaries/beneficiary_details.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../logic/controller/beneficiary_controller.dart';
import '../../../utils/theme.dart';

class CustomChip extends StatelessWidget {
   CustomChip({super.key, required this.isSelected, required this.name});
  final bool isSelected;
  final String name;

 Color color = ThemesManager().isSavedDarkMode() ? ColorsManager.white : ColorsManager.darkGrey;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BeneficiaryController>(
        init:BeneficiaryController(),
    builder: (BeneficiaryController controller) {
          return
     Container(
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        // shadows: [
        //   BoxShadow(
        //     color:Color.fromRGBO(0, 0, 0, 0.07),
        //     blurRadius: 30,
        //     offset: Offset(0, 16),
        //     spreadRadius: 0,
        //   )
        // ],
      ),
      child: GestureDetector(
        onTap: (){
          controller.filterInvoice(name);
        },
        child: Chip(
          elevation: 0,
          padding: EdgeInsets.symmetric(horizontal: 10,vertical: 12),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          backgroundColor:isSelected
              ? ColorsManager.main
              : ColorsManager.disabled.withOpacity(0.15),
          label: TextUtils(
            color: isSelected
                ? ColorsManager.white
                : color,
            text: name,
          ), //Text
        ),
      ),
    );
  });
  }
}
