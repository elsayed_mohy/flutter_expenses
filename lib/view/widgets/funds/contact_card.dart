import 'package:contacts_service/contacts_service.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import "package:flutter/material.dart";
import 'package:get/get.dart';
import '../../../utils/constants/colors.dart';
import '../shared/card_container.dart';
import '../shared/text_utils.dart';

class ContactCard extends StatefulWidget {
   ContactCard({super.key, required this.contact});
   final Contact contact;

  @override
  State<ContactCard> createState() => _ContactCardState();
}

class _ContactCardState extends State<ContactCard> {
  bool checked = false;
  @override
  Widget build(BuildContext context) {
    return CardContainer(child: GetBuilder<BoxController>(
      init: BoxController(),
      builder: (BoxController controller) {
        return CheckboxListTile(
          title:
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextUtils(
                                  text: widget.contact.displayName ?? '',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500),
                              SizedBox(height: 8,),
                              TextUtils(
                                fontSize: 11,
                                text: widget.contact.phones!.isEmpty ? "" : "${widget.contact.phones?[0].value}",
                                color: ColorsManager.disabled,
                                fontWeight: FontWeight.bold,)
                            ],
                          ),
          value: controller.selectedContactList.contains(widget.contact),
          onChanged: (newValue) {
            setState(() {
              controller.updateSelectedContactList(widget.contact);
              checked = controller.selectedContactList.contains(widget.contact);
            });
          },
          controlAffinity: ListTileControlAffinity.platform,
        );
      }
    ));
  }
}
