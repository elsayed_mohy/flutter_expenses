import 'package:contacts_service/contacts_service.dart';
import 'package:expenses/data/model/box/box_details_model.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/view/widgets/shared/primary_button.dart';
import "package:flutter/material.dart";
import 'package:get/get.dart';
import '../../../l10n/app_localizations.dart';
import '../../../utils/constants/colors.dart';
import '../shared/card_container.dart';
import '../shared/card_image.dart';
import '../shared/text_utils.dart';

class MemberCard extends StatelessWidget {
  MemberCard({super.key, required this.member});
   final Member member;

  @override
  Widget build(BuildContext context) {
    return CardContainer(child: GetBuilder<BoxController>(
      init: BoxController(),
      builder: (BoxController controller) {
        return
          CardContainer(
            child: Row(
              children: [
                CardImage(imagePath: "assets/images/avatar/avatar1.png"),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextUtils(
                          text: member.member.fullName,
                          fontSize: 17,
                          fontWeight: FontWeight.w500),
                      SizedBox(height: 10,),
                      TextUtils(
                          text:member.admin == false  ? AppLocalizations.of(context)!.subscriber :AppLocalizations.of(context)!.admin ,
                          fontSize: 11,
                          fontWeight: FontWeight.w500),
                    ],
                  ),
                ),
                PrimaryButton(text:AppLocalizations.of(context)!.change , onPressed: (){
                  controller.toggleMemberType(member);
                }, disabled: member.admin == false)
              ],
            ),
          );
      }
    ));

  }
}
