import "package:expenses/data/model/beneficiaries/beneficiary_details.dart";
import "package:expenses/logic/controller/beneficiary_controller.dart";
import "package:expenses/view/widgets/funds/custom_chip.dart";
import "package:expenses/view/widgets/shared/add_button.dart";
import "package:expenses/view/widgets/shared/text_utils.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";

class CustomChipList extends StatelessWidget {
   CustomChipList({super.key,required this.list});

  final List<BeneficiaryDetails> list;
  final controller = Get.find<BeneficiaryController>();
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 70,
          child: Row(
            children: [
              SizedBox(width: 15),
              Expanded(
                child: ListView.separated(
                  shrinkWrap: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) => CustomChip(
                    isSelected: list[index].id == controller.currentBox.id ? true : false, name: list[index].name,
                  ),
                  itemCount: list.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(width: 15);
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
