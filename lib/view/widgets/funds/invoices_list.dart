import 'package:expenses/data/model/invoice/invoice_model.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/view/widgets/funds/invoice.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';

class InvoicesList extends StatelessWidget {
   InvoicesList({super.key, required this.invoicesList,required this.boxId});
    final List<InvoiceModel> invoicesList;
    final int boxId;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: TextUtils(
            text: AppLocalizations.of(context)!.invoices,
            fontWeight: FontWeight.w500,
            fontSize: 22,
          ),
        ),
        SizedBox(height: 15,),
        ListView.builder(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),

            itemBuilder: (BuildContext context, int index) => Invoice(invoice: invoicesList[index], boxId: boxId,),
            itemCount: invoicesList.length),
      ],
    );
  }
}
