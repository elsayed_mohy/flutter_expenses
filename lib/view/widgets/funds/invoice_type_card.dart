import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../utils/theme.dart';

class InvoiceTypeCard extends StatelessWidget {
   InvoiceTypeCard({super.key, required this.selected, required this.income,required this.action, required this.value});
  final bool selected;
  final bool income;
  final Function action;
  final double value;

  final Color secColor = ThemesManager().isSavedDarkMode() ? ColorsManager.darkDisabled : ColorsManager.white;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        action();
      },
      child: Container(
        width: 144,
        height: 140,
        decoration: ShapeDecoration(
          color:income  ? ColorsManager.secondary : secColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(14),
          ),
          shadows: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.13),
              blurRadius: 60,
              offset: Offset(8, 30),
              spreadRadius: 0,
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(income ? CupertinoIcons.tray_arrow_down_fill :CupertinoIcons.tray_arrow_up_fill,
                  color: income ? ColorsManager.darkDisabled : ColorsManager.warning,
              size: 25,),
              SizedBox(height: 10,),
              TextUtils(text:income ? AppLocalizations.of(context)!.addIncome : AppLocalizations.of(context)!.addExpense,
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: income ? ColorsManager.darkDisabled : ColorsManager.warning,),
              SizedBox(height: 15,),
              TextUtils(text:value.toString(),
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: income ? ColorsManager.darkDisabled : ColorsManager.warning,)
            ],
          ),
        ),
      ),
    );
  }
}
