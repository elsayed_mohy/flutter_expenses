import "package:expenses/l10n/app_localizations.dart";
import "package:expenses/logic/controller/box_controller.dart";
import "package:expenses/routes/routes.dart";
import "package:expenses/utils/constants/config.dart";
import "package:expenses/utils/theme.dart";
import "package:expenses/view/widgets/shared/primary_button.dart";
import "package:expenses/view/widgets/shared/secondary_button.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";

import "../../../data/model/invoice/invoice_model.dart";
import "../../../utils/constants/colors.dart";
import "../shared/card_container.dart";
import "../shared/text_utils.dart";
enum InvActions { delete, type }
class Invoice extends StatelessWidget {
   Invoice({super.key, required this.invoice, required this.boxId});
  final InvoiceModel invoice;
  final int boxId;
  final controller = Get.find<BoxController>();
   openConfirmDeleteDialog(BuildContext context) {
     Get.defaultDialog(title: AppLocalizations.of(context)!.deleteInvoice,
       onConfirm: (){},
       contentPadding: EdgeInsets.all(12),
       buttonColor : ColorsManager.main,
       middleText: AppLocalizations.of(context)!.deleteInvoiceMsg,
       cancel: SecondaryButton(onPressed: (){
         Get.back();
       }, text: AppLocalizations.of(context)!.cancel,),
       confirm: PrimaryButton(onPressed: (){
         controller.deleteInvoice(invoice.id);
       }, text: AppLocalizations.of(context)!.remove, disabled: false,),
       confirmTextColor: ThemesManager().isSavedDarkMode() ? ColorsManager.white : ColorsManager.white,
       cancelTextColor: ThemesManager().isSavedDarkMode() ? ColorsManager.white : ColorsManager.darkGrey,
       onCancel: (){},
     );
   }
  @override
  Widget build(BuildContext context) {
    // print("boxId:${boxId}");
    // print("invoice:${boxId}");
    return Ink(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),),
      child: InkWell(
        onTap: () {
          controller.getInvoiceDetails(invoice.id);
          controller.getInvoiceItems();
          controller.invoiceInUpdate(isInUpdate: true);
          Get.toNamed("${Routes.home}${Routes.addEditInvoiceScreen}");
        },
        child: CardContainer(
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: TextUtils(
                              text: invoice.notes,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        ),
                        TextUtils(
                            text: invoice.amount.toString(),
                            color:invoice.expense ? ColorsManager.warning :  ColorsManager.success,
                          fontWeight: FontWeight.w500,

                        ),
                        PopupMenuButton<InvActions>(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                          ),

                          itemBuilder: (context) => [
                            PopupMenuItem(
                              value: InvActions.delete,
                              child: TextUtils(
                                text: AppLocalizations.of(context)!.remove,
                              ),
                            ),
                            PopupMenuItem(
                              value: InvActions.type,
                              child: TextUtils(
                                color: invoice.expense ? ColorsManager.success : ColorsManager.warning ,
                                text: invoice.expense ?  AppLocalizations.of(context)!.income :  AppLocalizations.of(context)!.expense,
                              ),
                            )
                          ],
                          onSelected: (value) async {
                            switch (value) {
                              case InvActions.delete:
                                openConfirmDeleteDialog(context);
                                break;
                              case InvActions.type:
                                controller.changeInvoiceType(invoice);
                                break;
                            }
                            // var subBoxes = controller.getSubscriptionBoxes();
                          },
                        )
                      ],
                    ),
                    SizedBox(height: 18,),
                    Row(
                      children: [
                        TextUtils(
                            text: invoice.beneficiary,
                            color: ColorsManager.disabled,
                          fontWeight: FontWeight.bold,),
                        Spacer(),
                        TextUtils(
                            text: Config.formatDate(invoice.createdDate).toString(),
                            color: ColorsManager.disabled,
                          fontSize: 12,),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
