import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/logic/controller/profile_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../l10n/app_localizations.dart';
import '../auth/auth_button.dart';
import '../auth/auth_text_form_field.dart';
import '../shared/text_utils.dart';

class AddBoxBottomSheet extends StatelessWidget {
  AddBoxBottomSheet({super.key});

  final TextEditingController nameController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final controller = Get.find<BoxController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: TextUtils(
            text: AppLocalizations.of(context)!.addFund,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: 30),
        Container(
          width: double.infinity,
          // height: MediaQuery.of(context).size.height / 1.6,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  AuthTextFormField(
                    controller: nameController,
                    obscureText: false,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'box name is required';
                      }
                      return null;
                    },
                    hintText: AppLocalizations.of(context)!.fundName,
                  ),
                  const SizedBox(height: 30),
                  GetBuilder<BoxController>(
                    builder: (BoxController controller){
                      return AuthButton(
                        text: AppLocalizations.of(context)!.save,
                        onPressed: () {
                          if(formKey.currentState!.validate()){
                             controller.addBox(nameController.text);
                          }
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
