import 'package:expenses/logic/controller/box_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../l10n/app_localizations.dart';
import '../../../services/base_service.dart';
import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';
import '../shared/accent_button.dart';
import '../shared/card_container.dart';
import '../shared/primary_button.dart';
import '../shared/text_utils.dart';
import 'contact_card.dart';

class AddMemberBottomSheet extends StatefulWidget {
  const AddMemberBottomSheet({super.key});

  @override
  State<AddMemberBottomSheet> createState() => _AddMemberBottomSheetState();
}

class _AddMemberBottomSheetState extends State<AddMemberBottomSheet> {
  List<Contact> contacts = [];
  final controller = Get.find<BoxController>();
  bool loading = false;
  final deviceLanguage = BaseService().getCurrentLanguage();

  getContacts() async {
    setState(() {
      loading = true;
    });
    PermissionStatus status = await Permission.contacts.request();
    if (status.isGranted) {
      contacts = await ContactsService.getContacts(withThumbnails: false);
      setState(() {
        loading = false;
      });
    } else {
      print("no permission");
    }
  }

  @override
  void initState() {
    getContacts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        controller.clearSelectedContactList();
        return true;
      },
      child: GetBuilder<BoxController>(
          init: BoxController(),
          builder: (BoxController controller) {
            List<Widget> widgetList = List<Widget>.from(
              controller.selectedContactList.map((item) => selectedContactChip(
                  item)), // Replace Text() with your desired widget
            );
            return Scaffold(
                appBar: AppBar(
                  title: Text(AppLocalizations.of(context)!.contacts),
                  // actions: [
                  //   if (controller.selectedContactList.isNotEmpty)
                  //     Padding(
                  //       padding: const EdgeInsets.all(10),
                  //       child: AccentButton(
                  //           text: AppLocalizations.of(context)!.send,
                  //           onPressed: () {
                  //             controller.sendInvitation();
                  //           }),
                  //     ),
                  //   if (controller.selectedContactList.isEmpty) Container(),
                  //   // sendButton((){
                  //   //   print("send");
                  //   // })
                  // ],
                ),
                body: loading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          if (controller.selectedContactList.isNotEmpty)
                            Wrap(
                              spacing: 8.0,
                              runSpacing: 8.0,
                              children: widgetList,
                            ),
                          Expanded(
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ContactCard(contact: contacts[index]),
                                itemCount: contacts.length),
                          ),

                        ],
                      ),
            floatingActionButton:controller.selectedContactList.isNotEmpty ?
            FloatingActionButton(
              backgroundColor: ColorsManager.main,
                child: Icon(deviceLanguage == "en" ? CupertinoIcons.arrow_right : CupertinoIcons.arrow_left),
                onPressed: (){
              controller.sendInvitation();
            }) : Container(),
            );
          }),
    );
  }

  Widget selectedContactChip(Contact contact) {
    return Container(
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
      ),
      child: Chip(
          elevation: 0,
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          backgroundColor: ColorsManager.disabled.withOpacity(0.15),
          label: TextUtils(
            text: contact.displayName!,
            fontSize: 10,
          ),
          onDeleted: () {
            setState(() {
            controller.updateSelectedContactList(contact);
            });
            // Perform any action you want here when the user clicks on the delete icon
          },
      ),
    );
  }

}
