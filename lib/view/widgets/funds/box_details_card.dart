import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../utils/constants/colors.dart';
import '../../../utils/theme.dart';
import '../shared/text_utils.dart';

class BoxDetailsCard extends StatelessWidget {
  BoxDetailsCard({super.key, this.data, required this.icon, required this.title});

  final data;
  final String title;
  final IconData icon;
  final Color cardColor = ThemesManager().isSavedDarkMode()
      ? ColorsManager.darkDisabled
      : Colors.white;
  final Color textColor = ThemesManager().isSavedDarkMode()
      ? ColorsManager.white
      : ColorsManager.darkDisabled;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 124,
      height: 124,
      margin: EdgeInsets.symmetric(horizontal: 10),
      decoration: ShapeDecoration(
        color: cardColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14),
        ),
        shadows: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.04),
            blurRadius: 60,
            offset: Offset(8, 30),
            spreadRadius: 0,
          )
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Icon(
            //   icon,
            //   color: textColor,
            //   size: 20,
            // ),
            SizedBox(
              height: 10,
            ),
            TextUtils(text: title, fontSize: 15, color: textColor),
            SizedBox(
              height: 10,
            ),
            TextUtils(text: data.toString(), fontSize: 16, color: textColor,
            fontWeight: FontWeight.w500,)
          ],
        ),
      ),
    );
  }
}
