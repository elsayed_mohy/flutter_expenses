import 'package:expenses/data/model/box/box_details_model.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/view/widgets/funds/member_card.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../l10n/app_localizations.dart';

class MembersBottomSheet extends StatefulWidget {
  const MembersBottomSheet({super.key});

  @override
  State<MembersBottomSheet> createState() => _MembersBottomSheetState();
}

class _MembersBottomSheetState extends State<MembersBottomSheet> {
   final controller = Get.find<BoxController>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async{
        return true;
      },
      child:        GetBuilder<BoxController>(
          init: BoxController(),
          builder: (BoxController controller) {
            print(controller.membersList);
            return Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context)!.members),
        ),
        body:
            Column(
                mainAxisSize:MainAxisSize.max,
              children: [
                if(controller.membersList.isNotEmpty)
                Expanded(
                  child: ListView.builder(
                      physics: ScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) => MemberCard(
                          member : controller.membersList[index]),
                      itemCount: controller.membersList.length),
                ),
                if(controller.membersList.isEmpty)
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: double.infinity,
                        child: Image.asset(
                          'assets/images/ui/empty-folder.png',
                          height: 150,
                        ),
                      ),
                      SizedBox(height: 15,),
                      TextUtils(text:AppLocalizations.of(context)!.noMembers,fontSize: 17,),
                    ],
                  ),
                )
              ],
            )


      );}),
    );
  }
}


