import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../routes/routes.dart';
import '../../../utils/constants/colors.dart';
import '../shared/text_utils.dart';
import 'auth_button.dart';
import 'auth_text_form_field.dart';
import 'have_account_container.dart';
class ForgotPasswordScreen extends StatelessWidget {
   ForgotPasswordScreen({super.key});
  final TextEditingController emailController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Image.asset("assets/images/logo/logoIcon.png",
                    width: 60,
                    height: 60,),
                ),
                SizedBox(height: 50),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 1.6,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: Form(
                      key:formKey,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              TextUtils(
                                text: 'Forgot',
                                color: ColorsManager.main,
                                fontSize: 35,
                                fontWeight: FontWeight.w500,
                              ),
                              const SizedBox(width: 5),
                              TextUtils(
                                text: 'password',
                                color:
                                Get.isDarkMode ? ColorsManager.disabled : ColorsManager.darkGrey,
                                fontSize: 35,
                                fontWeight: FontWeight.w500,
                              ),
                            ],
                          ),
                          const SizedBox(height: 25),
                          AuthTextFormField(
                            controller: emailController,
                            obscureText: false,
                            validator: (value) {
                              if(value.toString().length <= 2){
                                return "err on name";
                              }else{
                                return null;
                              }
                            },
                            prefixIcon:
                            Icons.person,

                            hintText: 'Email address',
                          ),
                          const SizedBox(height: 40),
                          AuthButton(
                            text: 'Send Reset Link',
                            onPressed: () {
                              print("Verify Mobile");
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 18),
                HaveAccountContainer(
                  text: 'Back to ',
                  authTypeText: 'Sign in',
                  onPressed: () {
                    Get.offNamed(Routes.loginScreen);
                  },
                )
              ],
            ),
          )),
    );
  }
}
