import 'package:expenses/services/base_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../utils/theme.dart';
import '../shared/background_paint.dart';

class AuthLogo extends StatelessWidget {
  const AuthLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return               Stack(
      children: [
        BackgroundPaint(height: 130,),
        Container(
          width: double.infinity,
          child: SvgPicture.asset(
            "assets/images/logo/${ThemesManager().isSavedDarkMode() ? 'light' :'dark'}-${BaseService().getCurrentLanguage()}-logo.svg",
            height: 50,
          ),
        ),
      ],
    );
  }
}
