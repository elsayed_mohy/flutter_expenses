import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';

class OTPFormField extends StatefulWidget {
   OTPFormField({super.key, required this.controller, required this.validator});
  final TextEditingController controller;
  final Function validator;

  @override
  State<OTPFormField> createState() => _OTPFormFieldState();
}

class _OTPFormFieldState extends State<OTPFormField> {
  FocusNode _focusNode = FocusNode();
  bool _isFocused = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusNode.removeListener(_onFocusChange);
    _focusNode.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      _isFocused = _focusNode.hasFocus;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // height: 68,
      width: 64,
      child: TextFormField(
        focusNode: _focusNode,
        maxLength: 1,
        onChanged: (value) {
          if(value.length == 1) {
            _focusNode.nextFocus();
          }
        },
        controller: widget.controller,
        keyboardType: TextInputType.number,
        textAlign: TextAlign.center,
        inputFormatters: [
          LengthLimitingTextInputFormatter(1),
          FilteringTextInputFormatter.digitsOnly
        ],
        validator: (value) => widget.validator(value),
        cursorColor:ThemesManager().isSavedDarkMode() ? ColorsManager.main : ColorsManager.darkGrey,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: Colors.transparent,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          fillColor:_isFocused ? ColorsManager.main.withOpacity(0.07) : ColorsManager.disabled.withOpacity(0.07),
          filled: true,
          suffixIconColor: ColorsManager.main,
          hintText: "0",
          hintStyle: const TextStyle(
              color: ColorsManager.disabled,
              fontSize: 14
          ),
          labelStyle:const TextStyle(
              color: ColorsManager.darkGrey,
              fontSize: 10
          ) ,
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: ColorsManager.main,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: ColorsManager.warning,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: ColorsManager.warning,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
