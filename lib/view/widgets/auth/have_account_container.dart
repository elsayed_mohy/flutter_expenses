import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';

class HaveAccountContainer extends StatelessWidget {
  final String text;
  final String authTypeText;
  final Function() onPressed;

  const HaveAccountContainer(
      {Key? key,
      required this.text,
      required this.onPressed,
      required this.authTypeText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextUtils(
            text: text,
            color: ColorsManager.disabled,
            fontWeight: FontWeight.normal,
          ),
          TextButton(
            onPressed: onPressed,
            child: TextUtils(
              text: authTypeText,
              color: ColorsManager.main,
              fontWeight: FontWeight.w500,
            ),
          )
        ],
      ),
    );
  }
}
