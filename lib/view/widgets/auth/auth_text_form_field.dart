import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';

import '../../../utils/theme.dart';

class AuthTextFormField extends StatefulWidget {
  final TextEditingController controller;
  final bool obscureText;
  final Function validator;
  final IconData? prefixIcon;
  final Widget? suffixIcon;
  final TextInputType? keyboardType;
  final String hintText;

  const AuthTextFormField({
    Key? key,
    required this.controller,
    required this.obscureText,
    required this.validator,
     this.prefixIcon,
     this.suffixIcon,
    required this.hintText, this.keyboardType,
  }) : super(key: key);

  @override
  State<AuthTextFormField> createState() => _AuthTextFormFieldState();
}

class _AuthTextFormFieldState extends State<AuthTextFormField> {
  FocusNode _focusNode = FocusNode();
  bool _isFocused = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusNode.removeListener(_onFocusChange);
    _focusNode.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      _isFocused = _focusNode.hasFocus;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // TextUtils(text: widget.hintText,align: TextAlign.start,fontWeight: FontWeight.w500,),
        // SizedBox(height: 10,),
        TextFormField(
          focusNode: _focusNode,
          controller: widget.controller,
          obscureText: widget.obscureText,
          cursorColor:ThemesManager().isSavedDarkMode() ? ColorsManager.main : ColorsManager.darkGrey,
          decoration: InputDecoration(
            prefixIcon:Icon(
              widget.prefixIcon,
              color: ColorsManager.main,
            size: 20,),
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            fillColor:_isFocused ? ColorsManager.main.withOpacity(0.07) : ColorsManager.disabled.withOpacity(0.07),
            filled: true,
            suffixIcon: widget.suffixIcon,
            suffixIconColor: ColorsManager.main,
            hintText: widget.hintText,
            hintStyle: const TextStyle(
              color: ColorsManager.disabled,
              fontSize: 14
            ),
            labelStyle:const TextStyle(
                color: ColorsManager.darkGrey,
                fontSize: 10
            ) ,
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: ColorsManager.main,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: ColorsManager.warning,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: ColorsManager.warning,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          keyboardType: widget.keyboardType ?? TextInputType.text,
          validator: (value) => widget.validator(value),
        ),
      ],
    );
  }
}
