import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/theme.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CheckWidget extends StatelessWidget {
  const CheckWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        InkWell(
          onTap: () {},
          child: Container(
            height: 35,
            width: 35,
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Get.isDarkMode ?
            Icon(
              Icons.done,
              color: ColorsManager.main,
            ):
            Icon(
              Icons.done,
              color: Colors.pink,
            ),
          ),
        ),
        SizedBox(width: 10),
        TextUtils(
          text: 'i accept terms & conditions',
          fontSize: 18,
          fontWeight: FontWeight.w500,
          color:  Get.isDarkMode ? Colors.black : Colors.white ,
        )
      ],
    );
  }
}
