import 'package:expenses/services/base_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  final String title;
  final IconData icon;
  final Function action;
  final Widget? trailing;
  final deviceLanguage = BaseService().getCurrentLanguage();
   CustomListTile(
      {Key? key, required this.title, required this.icon, this.trailing, required this.action})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(

      title: Text(title,style: TextStyle(
        fontSize: 14,
      ),),
      leading: Icon(icon,size: 22,),
      trailing: trailing ?? Icon(deviceLanguage == "en" ? CupertinoIcons.chevron_right : CupertinoIcons.chevron_left , size: 16,),
      onTap:  () {
        action();
      },

    );
  }
}