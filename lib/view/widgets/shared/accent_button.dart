import 'package:expenses/utils/theme.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/constants/colors.dart';

class AccentButton extends StatelessWidget {
  final String text;
  final Function() onPressed;

  const AccentButton({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 50,
      decoration: ShapeDecoration(
        color: ColorsManager.vividSkyBlue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        shadows: [
          BoxShadow(
            color:Color(0x335D5ED9) ,
            blurRadius: 30,
            offset: Offset(0, 16),
            spreadRadius: 0,
          )
        ],
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor:ColorsManager.basic,
          minimumSize: const Size(120, 55),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        onPressed:onPressed,
        child: TextUtils(
          text: text,
          fontSize: 12,
          fontWeight: FontWeight.w500,
          color: Colors.white,
          align: TextAlign.center,
        ),
      ),
    );
  }
}
