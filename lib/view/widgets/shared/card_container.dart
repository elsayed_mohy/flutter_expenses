import 'package:flutter/material.dart';

class CardContainer extends StatelessWidget {
  const CardContainer({super.key, required this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        // padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.08),
              blurRadius: 45,
              offset: Offset(15, 0),
              spreadRadius: 0,
            )
            // box-shadow: rgba(0, 0, 0, 0.05) 0px 1px 2px 0px;
          ],
        ),
        child: Card(child: Padding(padding:EdgeInsets.all(10),child: child)));
  }
}
