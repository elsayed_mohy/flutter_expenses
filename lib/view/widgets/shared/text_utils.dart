import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../utils/theme.dart';

class TextUtils extends StatelessWidget {
  final String text;
  final double? fontSize;
  final FontWeight? fontWeight;
  final Color? color;
  final TextAlign? align;

  const TextUtils(
      {Key? key,
      required this.text,
       this.fontSize,
        this.fontWeight,
       this.color, this.align})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color ??( ThemesManager().isSavedDarkMode() ? ColorsManager.white : ColorsManager.darkGrey),
        fontSize: fontSize ?? 13,
        fontWeight: fontWeight ?? FontWeight.normal,
      ),

        textAlign: align ?? TextAlign.start,
    );
  }
}
