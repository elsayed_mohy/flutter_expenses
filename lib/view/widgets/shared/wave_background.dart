import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

import '../../../utils/constants/ui.dart';
import '../../../utils/theme.dart';

class WaveBackground extends StatelessWidget {
  const WaveBackground({super.key, this.top});
  final double? top;

  @override
  Widget build(BuildContext context) {
    return           Positioned.fill(
        left: 0,
        right: 0,
        top: top ?? -MediaQuery.of(context).size.height * 0.95,
        child: SvgPicture.asset(
            ThemesManager().isSavedDarkMode() ? UIContestants.darkWaveSvgPath : UIContestants.lightWaveSvgPath
            ,fit :BoxFit.fitWidth)
    );
  }
}
