import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../utils/theme.dart';

class AddButton extends StatelessWidget {
  const AddButton({super.key, required this.action});
  final Function action;

  @override
  Widget build(BuildContext context) {
   return InkWell(
     onTap: (){
       action();
     },
     child: Container(
        width: 52,
        height: 52,
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
            side: BorderSide(
              width: 0.50,
              strokeAlign: BorderSide.strokeAlignCenter,
              color: Color(0xFFC7C7C7),
            ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        child: Icon(CupertinoIcons.add,
         color: ThemesManager().isSavedDarkMode() ? ColorsManager.white : ColorsManager.disabled,),
      ),
   );
  }
}
