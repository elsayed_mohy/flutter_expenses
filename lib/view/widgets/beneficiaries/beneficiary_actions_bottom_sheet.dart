import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../utils/constants/colors.dart';
import '../shared/text_utils.dart';

class BeneficiaryActionsBottomSheet extends StatelessWidget {
  const BeneficiaryActionsBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    return         Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          TextUtils(text: "Beneficiary Actions",fontSize: 20,fontWeight: FontWeight.w500,color: ColorsManager.warning,),
          SizedBox(height: 15),

          Container(
            decoration: BoxDecoration(
                border: Border(
                  top: BorderSide( //                    <--- top side
                    color:ColorsManager.disabled.withOpacity(0.5),
                    width: 1.0,
                  ),
                )
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 20,left: 12,right: 12),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          if(Get.isBottomSheetOpen ?? false){
                            Get.back();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(100, 50),
                            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 30),
                            shape:RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            backgroundColor:
                            ColorsManager.darkDisabled),
                        child: TextUtils(text: 'Edit', fontSize: 16,color: ColorsManager.white,),
                      ),
                      SizedBox(width: 20),

                      ElevatedButton(
                        onPressed: () {
                          // controller.logout();
                        },
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(100, 50),
                            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 30),
                            shape:RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            backgroundColor:
                            ColorsManager.main),
                        child: TextUtils(text: 'Account Statement', fontSize: 16,color: ColorsManager.white,),
                      ),

                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
