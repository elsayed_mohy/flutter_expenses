import "package:expenses/data/model/beneficiaries/beneficiary_details.dart";
import "package:expenses/l10n/app_localizations.dart";
import "package:expenses/logic/controller/beneficiary_controller.dart";
import "package:expenses/services/beneficiaries/beneficiary_service.dart";
import "package:expenses/utils/constants/colors.dart";
import "package:expenses/view/screens/user_layout/beneficiaries/beneficiary_details_screen.dart";
import "package:expenses/view/widgets/shared/card_container.dart";
import "package:expenses/view/widgets/shared/text_utils.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";

import "../shared/card_image.dart";

enum BeneficiaryActions { view, statement }

class Beneficiary extends StatelessWidget {
  final String beneficiary;
  final dynamic totalInCome;
  final dynamic totalOutCome;
  final controller = Get.find<BeneficiaryController>();

  Beneficiary(
      {super.key,
      required this.beneficiary,
      required this.totalInCome,
      required this.totalOutCome});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if(beneficiary.isNotEmpty){
          controller.beneficiaryDetails(beneficiary);
          Get.to(() => BeneficiaryDetailsScreen(
              beneficiaryName: beneficiary));
        }
      },
      child: CardContainer(
        child: Row(
          children: [
            CardImage(imagePath: "assets/images/avatar/avatar1.png"),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextUtils(
                          text: beneficiary,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                      // IconButton(
                      //     onPressed: () {
                      //getBottomSheet(170,BeneficiaryActionsBottomSheet());
                      //       },
                      //     icon: Icon(
                      //       Icons.more_horiz,
                      //     )),
                      PopupMenuButton<BeneficiaryActions>(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                        ),

                        itemBuilder: (context) => [
                          PopupMenuItem(
                            value: BeneficiaryActions.view,
                            child: TextUtils(
                              text: AppLocalizations.of(context)!.view,
                            ),
                          ),
                          PopupMenuItem(
                            value: BeneficiaryActions.statement,
                            child: TextUtils(
                              text: AppLocalizations.of(context)!.report,
                            ),
                          )
                        ],
                        onSelected: (value) async {
                          switch (value) {
                            case BeneficiaryActions.view:
                              print("edit");
                              if(beneficiary.isNotEmpty){
                                controller.beneficiaryDetails(beneficiary);
                                Get.to(() => BeneficiaryDetailsScreen(
                                    beneficiaryName: beneficiary));
                              }
                              break;
                            case BeneficiaryActions.statement:
                              if (beneficiary.isNotEmpty) {
                                BeneficiaryService()
                                    .beneficiaryDetails(beneficiary)
                                    .then((value) {
                                  print(value);
                                  _load(value,beneficiary);
                                });
                              }
                              break;
                          }
                          // var subBoxes = controller.getSubscriptionBoxes();
                        },
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          TextUtils(
                              text: AppLocalizations.of(context)!.totalIncome,
                              color: ColorsManager.disabled),
                          SizedBox(
                            width: 20,
                          ),
                          TextUtils(
                            text: totalInCome.toString(),
                            color: ColorsManager.success,
                            fontWeight: FontWeight.w500,
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          TextUtils(
                              text: AppLocalizations.of(context)!.totalExpense,
                              color: ColorsManager.disabled),
                          SizedBox(
                            width: 20,
                          ),
                          TextUtils(
                            text: totalOutCome.toString(),
                            color: ColorsManager.warning,
                            fontWeight: FontWeight.w500,
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _load(List<BeneficiaryDetails> list,String beneficiary) {
    List ids = list.map((o) => o.id).toSet().toList();
    controller.downloadPdf(beneficiary,ids);
  }
}
