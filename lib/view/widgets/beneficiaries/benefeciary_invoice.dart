import "package:expenses/data/model/beneficiaries/beneficiary_details.dart";
import "package:flutter/material.dart";

import "../../../data/model/invoice/invoice_model.dart";
import "../../../utils/constants/colors.dart";
import "../shared/card_container.dart";
import "../shared/text_utils.dart";

class BeneficiaryInvoice extends StatelessWidget {
  const BeneficiaryInvoice({super.key, required this.invoice});
  final Invoice invoice;

  @override
  Widget build(BuildContext context) {
    return CardContainer(
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: TextUtils(
                          text: invoice.notes,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    ),
                    TextUtils(
                        text: invoice.amount.toString(),
                        color:invoice.expense ? ColorsManager.warning :  ColorsManager.success,
                      fontWeight: FontWeight.w500,

                    ),
                  ],
                ),
                SizedBox(height: 18,),
                Row(
                  children: [
                    TextUtils(
                        text: invoice.beneficiary,
                        color: ColorsManager.disabled,
                      fontWeight: FontWeight.bold,),
                    Spacer(),
                    TextUtils(
                        text: invoice.createdDate,
                        color: ColorsManager.disabled,
                      fontSize: 12,),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
