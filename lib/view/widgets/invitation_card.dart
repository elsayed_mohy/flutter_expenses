import "package:expenses/data/model/invitation_model.dart";
import "package:expenses/l10n/app_localizations.dart";
import "package:expenses/utils/constants/colors.dart";
import "package:expenses/view/widgets/shared/card_container.dart";
import "package:expenses/view/widgets/shared/card_image.dart";
import "package:expenses/view/widgets/shared/text_utils.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";

import "../../logic/controller/invitation_controller.dart";

class InvitationCard extends StatelessWidget {
   InvitationCard(
      {super.key, required this.invitation, required this.sent});

  final Invitation invitation;
  final bool sent;
  @override
  Widget build(BuildContext context) {
    return CardContainer(
      child: Row(
        children: [
          CardImage(imagePath: "assets/images/avatar/avatar1.png",
            height: 80,
            width: 75,),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextUtils(
                        text: "${invitation.user.fullName}",
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                sent ? sentInvitationBody(invitation,context) : receivedInvitationBody(invitation,context),

                // SizedBox(height: 5),

              ],
            ),
          ),
          sent ? sentInvitationActions(invitation,context) : receivedInvitationActions(
              invitation,context),
        ],
      ),
    );
  }
}

Widget receivedInvitationBody(Invitation invitation,BuildContext context) {
  return TextUtils(
    text:AppLocalizations.of(context)!.invitedYouMessage(invitation.box.name, invitation.user.fullName),
    color: ColorsManager.disabled,
    fontSize: 11,
  );
}

Widget sentInvitationBody(Invitation invitation,BuildContext context) {
  return TextUtils(
    text:AppLocalizations.of(context)!.youInvitedMessage(invitation.box.name, invitation.user.fullName),
    // 'you invited ${invitation.user.fullName} to join ${invitation.box.name}',
    color: ColorsManager.disabled,
    fontSize: 11,
  );
}


Widget receivedInvitationActions(Invitation invitation,BuildContext context) {
  return GetBuilder<InvitationController>(
      init: InvitationController(),
    builder: (controller) {
      return invitation.status.code != "PENDING" ? Container(
          padding:
          EdgeInsets.symmetric(vertical: 5, horizontal: 7),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: ColorsManager.secondary.withOpacity(0.25)),
          child: TextUtils(text: '${invitation.status.name}', fontSize: 9)) :   Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          TextButton(
            onPressed: () {
              controller.acceptInvitation((invitation.id).toString());
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                backgroundColor:
                ColorsManager.main),
            child: TextUtils(
              text: AppLocalizations.of(context)!.accept, fontSize: 10, color: ColorsManager.white,),
          ),
          SizedBox(width: 10),
          TextButton(
            onPressed: () {
              controller.acceptInvitation((invitation.id).toString());
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                backgroundColor:
                ColorsManager.darkDisabled),
            child: TextUtils(
              text: AppLocalizations.of(context)!.reject, fontSize: 10, color: ColorsManager.white,),
          ),
        ],
      );
    }
  );
}


Widget sentInvitationActions(Invitation invitation,BuildContext context) {
  return GetBuilder<InvitationController>(
    init: InvitationController(),
    builder: (controller) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              padding:
                  EdgeInsets.symmetric(vertical: 5, horizontal: 7),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: ColorsManager.secondary.withOpacity(0.25)),
              child: TextUtils(text: '${invitation.status.name}', fontSize: 9)),
          invitation.status.code == "PENDING" ? Column(children: [
            SizedBox(height: 8),
            TextButton(
              onPressed: () {
                controller.cancelInvitation((invitation.id).toString());
              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  backgroundColor:
                  ColorsManager.darkDisabled),
              child: TextUtils(
                text: AppLocalizations.of(context)!.cancel, fontSize: 10, color: ColorsManager.white,),
            ),
          ],) :Container()
        ],
      );
    }
  );
}