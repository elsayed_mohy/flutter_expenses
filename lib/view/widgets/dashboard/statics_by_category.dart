import 'package:expenses/data/model/dasboard/chart_category.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../data/mock_data.dart';
import '../../../../data/model/expense.dart';
import '../../../../data/model/pie_data.dart';
import 'category_box.dart';
import 'expense_widget.dart';

class StaticsByCategory extends StatefulWidget {
  const StaticsByCategory({Key? key}) : super(key: key);

  @override
  State<StaticsByCategory> createState() => _StaticsByCategoryState();
}

class _StaticsByCategoryState extends State<StaticsByCategory> {
  int touchedIndex = -1;
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: DashboardController(),
      builder: (controller) {
        return CategoryBox(
          suffix: Container(),
          title: "Statistics by category",
          children: [
            _pieChart(
              controller.categoriesChartList
                  .map(
                    (e) => PieData(value: e.elementSum, color: ColorsManager.success),
                  )
                  .toList(),
            ),
            _otherExpanses(controller.categoriesChartList),
          ],
        );
      }
    );
  }

  Widget _otherExpanses(List<ChartCategory> categoriesChartList) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      decoration: BoxDecoration(
        // color: ColorsManager.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        // controller: _scrollController,
        padding: const EdgeInsets.all(2),
        children: categoriesChartList
            .map((ChartCategory e) => ExpenseWidget(category: e))
            .toList(),
      ),
    );
  }

  Widget _pieChart(List<PieData> data) {
    return SizedBox(
      height: 180,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: TextUtils(
              text:
              touchedIndex == -1
                  ? "100%"
                  : "${data[touchedIndex].value.toStringAsFixed(0)}%",
              fontWeight: FontWeight.w500,
              fontSize: 18,
              align: TextAlign.center,


            ),
          ),
          PieChart(
            PieChartData(
              sections: data
                  .map(
                    (e) => PieChartSectionData(
                      color: e.color,
                      value: e.value,
                      radius: touchedIndex == data.indexOf(e) ? 35.0 : 25.0,
                      title: '',
                    ),
                  )
                  .toList(),
              pieTouchData: PieTouchData(
                  touchCallback: (FlTouchEvent event, pieTouchResponse) {
                setState(() {
                  if (!event.isInterestedForInteractions ||
                      pieTouchResponse == null ||
                      pieTouchResponse.touchedSection == null) {
                    touchedIndex = -1;
                    return;
                  }
                  touchedIndex =
                      pieTouchResponse.touchedSection!.touchedSectionIndex;
                });
              }),
              borderData: FlBorderData(
                show: false,
              ),
              sectionsSpace: 0,
              centerSpaceRadius: 50,
            ),
            swapAnimationDuration: const Duration(milliseconds: 150),
            swapAnimationCurve: Curves.linear,
          ),
        ],
      ),
    );
  }
}
