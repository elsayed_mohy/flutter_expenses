import 'package:expenses/data/model/dasboard/max_box.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../../data/mock_data.dart';
import '../shared/responsive.dart';

enum _actions { expenses, income }

class MaxBoxBarChart extends StatelessWidget {
  final String title;
  final Color barColor;
  final List<MaxBox> data;
  final controller = Get.find<DashboardController>();

  MaxBoxBarChart({
    Key? key,
    required this.title,
    required this.barColor,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              // PopupMenuButton<_actions>(
              //   shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(15),
              //   ),
              //   itemBuilder: (context) => [
              //     PopupMenuItem(
              //       value: _actions.expenses,
              //       child: TextUtils(
              //         text: AppLocalizations.of(context)!.expense,
              //       ),
              //     ),
              //     PopupMenuItem(
              //       value: _actions.income,
              //       child: TextUtils(
              //         text: AppLocalizations.of(context)!.income,
              //       ),
              //     )
              //   ],
              //   onSelected: (value) async {
              //     switch (value) {
              //       case _actions.expenses:
              //         controller.boxChart(isExpense: true);
              //         controller.updateBoxChartColor(ColorsManager.warning);
              //         break;
              //       case _actions.income:
              //         controller.boxChart(isExpense: false);
              //         controller.updateBoxChartColor(ColorsManager.success);
              //         break;
              //     }
              //     // var subBoxes = controller.getSubscriptionBoxes();
              //   },
              // ),
              Obx(() => ToggleButtons(
                    isSelected: controller.boxChartIsSelected.value,
                    // color: Colors.black,
                    // selectedColor: Colors.blue,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: TextUtils(
                            text: AppLocalizations.of(context)!.expense),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: TextUtils(
                            text: AppLocalizations.of(context)!.income),
                      ),
                    ],
                    onPressed: (int index) {
                      controller.changeBoxChartTabIndex(index);
                      if (index == 0) {
                        controller.boxChart(isExpense: true);
                        controller.updateBoxChartColor(ColorsManager.warning);
                      }
                      if (index == 1) {
                        controller.boxChart(isExpense: false);
                        controller.updateBoxChartColor(ColorsManager.success);
                      }
                    },
                  ))
            ],
          ),
        ),
        SfCartesianChart(
          primaryXAxis: CategoryAxis(),
          // title: ChartTitle(text: AppLocalizations.of(context)!.topBox),
          // Enable legend
          // legend: Legend(isVisible: true),
          series: <CartesianSeries<MaxBox, dynamic>>[
            ColumnSeries<MaxBox, dynamic>(
                color: controller.boxChartColor,
                dataSource: data,
                animationDuration: 20,
                xValueMapper: (MaxBox box, _) => box.name,
                yValueMapper: (MaxBox box, _) => box.elementSum,
                dataLabelSettings: DataLabelSettings(isVisible: true)),
          ],
        ),
      ],
    );
  }
}
