import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/card_container.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../utils/theme.dart';

class DashboardInfoCard extends StatelessWidget {
  DashboardInfoCard({super.key});

  final Color secColor = ThemesManager().isSavedDarkMode() ? ColorsManager.white : ColorsManager.darkGrey;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: CardContainer(
        child: Container(
          width: 114,
          height: 154,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14),
            ),
            shadows: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.04),
                blurRadius: 60,
                offset: Offset(8, 30),
                spreadRadius: 0,
              )
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(CupertinoIcons.home,
                    color:secColor,
                size: 25,),
                SizedBox(height: 10,),
                TextUtils(text: "Total Salary",fontSize: 14),
                SizedBox(height: 20,),
                TextUtils(text: "1000",fontSize: 17,fontWeight: FontWeight.w500,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
