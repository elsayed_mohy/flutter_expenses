import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';

class CategoryBox extends StatelessWidget {
  final List<Widget> children;
  final Widget suffix;
  final String title;

  const CategoryBox({
    Key? key,
    required this.suffix,
    required this.children,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.all(ColorsManager.defaultPadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextUtils(
                text :title,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
              suffix
            ],
          ),
        ),
        ...children,
      ],
    );
  }
}
