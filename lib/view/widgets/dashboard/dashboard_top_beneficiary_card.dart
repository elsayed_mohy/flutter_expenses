import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/card_container.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import '../../../utils/theme.dart';

enum _actions { expenses, income }

class DashboardTopBeneficiaryCard extends StatelessWidget {
  DashboardTopBeneficiaryCard({super.key});

  final Color secColor = ThemesManager().isSavedDarkMode()
      ? ColorsManager.white
      : ColorsManager.darkGrey;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: CardContainer(
        child: Container(
          width: 175,
          height: 160,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14),
            ),
            // shadows: [
            //   BoxShadow(
            //     color: Color.fromRGBO(0, 0, 0, 0.04),
            //     blurRadius: 60,
            //     offset: Offset(8, 30),
            //     spreadRadius: 0,
            //   )
            // ],
          ),
          child: GetBuilder<DashboardController>(
              init: DashboardController(),
              builder: (controller) {
                if (controller.isLoadingMaxBeneficiary) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TextUtils(
                                text: AppLocalizations.of(context)!
                                    .topBeneficiary,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                            PopupMenuButton<_actions>(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              itemBuilder: (context) => [
                                PopupMenuItem(
                                  value: _actions.expenses,
                                  child: TextUtils(
                                    text: AppLocalizations.of(context)!.expense,
                                  ),
                                ),
                                PopupMenuItem(
                                  value: _actions.income,
                                  child: TextUtils(
                                    text: AppLocalizations.of(context)!.income,
                                  ),
                                )
                              ],
                              onSelected: (value) async {
                                switch (value) {
                                  case _actions.expenses:
                                    controller.maxBeneficiary(isExpense: true);
                                    break;
                                  case _actions.income:
                                    controller.maxBeneficiary(isExpense: false);
                                    break;
                                }
                                // var subBoxes = controller.getSubscriptionBoxes();
                              },
                            )
                          ],
                        ),
                        Expanded(
                          child: Container(
                            width: double.infinity,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [

                                TextUtils(
                                  text: controller.maxBeneficiaryObj.elementSum
                                      .toString(),
                                  fontSize: 28,
                                  color: Color(0xff769FCD),
                                  fontWeight: FontWeight.w500,
                                ),

                                TextUtils(
                                  text: (controller.isMaxBeneficiary == true
                                      ? AppLocalizations.of(context)!.expense
                                      : AppLocalizations.of(context)!.income),
                                  fontSize: 14,
                                  color: controller.isMaxBeneficiary == true ? ColorsManager.warning : ColorsManager.success,
                                  fontWeight: FontWeight.w500,
                                ),
                                Spacer(),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    TextUtils(
                                        text:
                                            "${AppLocalizations.of(context)!.username}",
                                        fontSize: 11,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    TextUtils(
                                      text:
                                          "${controller.maxBeneficiaryObj.name}",
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                        color: Color(0xff576CBC)
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                }
              }),
        ),
      ),
    );
  }
}
