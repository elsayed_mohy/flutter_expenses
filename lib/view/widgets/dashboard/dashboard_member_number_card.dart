import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/card_container.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import '../../../utils/theme.dart';


class DashboardMemberNumberCard extends StatelessWidget {
  DashboardMemberNumberCard({super.key});

  final Color secColor = ThemesManager().isSavedDarkMode()
      ? ColorsManager.white
      : ColorsManager.darkGrey;


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: CardContainer(
        child: Container(
          width: 175,
          height: 160,
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14),
            ),
            // shadows: [
            //   BoxShadow(
            //     color: Color.fromRGBO(0, 0, 0, 0.04),
            //     blurRadius: 60,
            //     offset: Offset(8, 30),
            //     spreadRadius: 0,
            //   )
            // ],
          ),
          child: GetBuilder<DashboardController>(
              init: DashboardController(),
              builder: (controller) {
                if (controller.isLoadingMemberNumber) {
                  return Center(
                    child: CircularProgressIndicator(
                      color: Colors.blue,
                    ),
                  );
                }
                else {
                  return Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TextUtils(
                                text: AppLocalizations.of(context)!.memberNumber,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ],
                        ),
                        Expanded(
                          child: Container(
                            width: double.infinity,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                TextUtils(text: controller.memberNumberObj.elementCount
                                    .toString(),
                                  fontSize: 30,
                                  color: Color(0xff00E0FF),
                                  align: TextAlign.center,
                                  fontWeight: FontWeight.w500,),
                              ],),
                          ),
                        )
                      ]
                      ,
                    )
                    ,
                  );
                }
              }
          ),
        ),
      ),
    );
  }
}
