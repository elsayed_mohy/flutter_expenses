import 'package:expenses/data/model/dasboard/max_beneficiary.dart';
import 'package:expenses/data/model/dasboard/max_box.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../../data/mock_data.dart';
import '../shared/responsive.dart';
import 'currency_text.dart';

enum _actions { expenses, income }

class MaxBeneficiaryBarChart extends StatelessWidget {
  final String title;
  final Color barColor;
  final List<MaxBeneficiary> data;
  final controller = Get.find<DashboardController>();

  MaxBeneficiaryBarChart({
    Key? key,
    required this.title,
    required this.barColor,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              // PopupMenuButton<_actions>(
              //   shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(15),
              //   ),
              //   itemBuilder: (context) => [
              //     PopupMenuItem(
              //       value: _actions.expenses,
              //       child: TextUtils(
              //         text: AppLocalizations.of(context)!.expense,
              //       ),
              //     ),
              //     PopupMenuItem(
              //       value: _actions.income,
              //       child: TextUtils(
              //         text: AppLocalizations.of(context)!.income,
              //       ),
              //     )
              //   ],
              //   onSelected: (value) async {
              //     switch (value) {
              //       case _actions.expenses:
              //         controller.beneficiaryChart(isExpense: true);
              //         controller.updateBeneficiaryChartColor(ColorsManager.warning);
              //         break;
              //       case _actions.income:
              //         controller.beneficiaryChart(isExpense: false);
              //         controller.updateBeneficiaryChartColor(ColorsManager.success);
              //         break;
              //     }
              //     // var subBoxes = controller.getSubscriptionBoxes();
              //   },
              // ),
              Obx(() => ToggleButtons(
                    isSelected: controller.beneficiaryChartIsSelected.value,
                    // color: Colors.black,
                    // selectedColor: Colors.blue,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: TextUtils(
                            text: AppLocalizations.of(context)!.expense),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: TextUtils(
                            text: AppLocalizations.of(context)!.income),
                      ),
                    ],
                    onPressed: (int index) {
                      controller.changeBeneficiaryChartTabIndex(index);
                      if (index == 0) {
                        controller.beneficiaryChart(isExpense: true);
                        controller
                            .updateBeneficiaryChartColor(ColorsManager.warning);
                      }
                      if(index == 1){
                        controller.beneficiaryChart(isExpense: false);
                        controller
                            .updateBeneficiaryChartColor(ColorsManager.success);
                      }
                    },
                  ))
            ],
          ),
        ),
        SfCartesianChart(
          primaryXAxis: CategoryAxis(),
          // title: ChartTitle(text: AppLocalizations.of(context)!.topBox),
          // Enable legend
          // legend: Legend(isVisible: true),
          series: <CartesianSeries<MaxBeneficiary, dynamic>>[
            ColumnSeries<MaxBeneficiary, dynamic>(
                color: controller.beneficiaryChartColor,
                dataSource: data,
                animationDuration: 20,
                xValueMapper: (MaxBeneficiary benf, _) => benf.name,
                yValueMapper: (MaxBeneficiary benf, _) => benf.elementSum,
                dataLabelSettings: DataLabelSettings(isVisible: true)),
          ],
        ),
      ],
    );
  }
}
