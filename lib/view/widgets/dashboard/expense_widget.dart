import 'package:expenses/data/model/dasboard/chart_category.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/material.dart';

class ExpenseWidget extends StatelessWidget {
  final ChartCategory category;

  const ExpenseWidget({Key? key, required this.category}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 18),
      child: Row(
        children: [
          Container(
            height: 10,
            width: 10,
            margin: const EdgeInsets.only(right: 18),
            decoration: BoxDecoration(
              color: ColorsManager.main,
              shape: BoxShape.circle,
            ),
          ),
          Expanded(
            child: Text(
              "${category.name} - ${category.elementSum}",
              style: const TextStyle(
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          // const Icon(Icons.chevron_right),
        ],
      ),
    );
  }
}
