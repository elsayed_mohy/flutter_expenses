import 'package:flutter/material.dart';

import '../../../../utils/constants/colors.dart';
import '../../../../utils/theme.dart';
import '../../../data/enums/transaction_type.dart';

class CurrencyText extends StatelessWidget {
  final String currency;
  final double amount;
  final TransactionType transactionType;
  final double fontSize;
  final Color color;
  const CurrencyText({
    Key? key,
    this.fontSize = 18,
    required this.currency,
    required this.amount,
    this.transactionType = TransactionType.unknown, required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "${TransactionTypeExtensions(transactionType).sign} $currency ",
        style: TextStyle(
          fontSize: fontSize * 0.7,
          color:color,
          fontWeight: FontWeight.w500,
        ),
        children: [
          TextSpan(
            text: amount.toStringAsFixed(2),
            style: TextStyle(
              color: color,
              fontSize: fontSize,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
