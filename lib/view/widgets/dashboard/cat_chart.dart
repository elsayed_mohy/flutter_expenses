import 'package:expenses/data/model/dasboard/chart_category.dart';
import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
enum _actions { expenses, income }
class CatChart extends StatelessWidget {
  final String title;
  final Color barColor;
  final List<ChartCategory> data;
  final controller = Get.find<DashboardController>();

  CatChart({
    Key? key,
    required this.title,
    required this.barColor,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15,right:15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              // PopupMenuButton<_actions>(
              //   shape: RoundedRectangleBorder(
              //     borderRadius: BorderRadius.circular(15),
              //   ),
              //   itemBuilder: (context) => [
              //     PopupMenuItem(
              //       value: _actions.expenses,
              //       child: TextUtils(
              //         text: AppLocalizations.of(context)!.expense,
              //       ),
              //     ),
              //     PopupMenuItem(
              //       value: _actions.income,
              //       child: TextUtils(
              //         text: AppLocalizations.of(context)!.income,
              //       ),
              //     )
              //   ],
              //   onSelected: (value) async {
              //     switch (value) {
              //       case _actions.expenses:
              //         controller.categoriesChart(isExpense: true);
              //         break;
              //       case _actions.income:
              //         controller.categoriesChart(isExpense: false);
              //         break;
              //     }
              //     // var subBoxes = controller.getSubscriptionBoxes();
              //   },
              // ),
              Obx(() => ToggleButtons(
                isSelected: controller.categoriesChartIsSelected.value,
                // color: Colors.black,
                // selectedColor: Colors.blue,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: TextUtils(
                        text: AppLocalizations.of(context)!.expense),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: TextUtils(
                        text: AppLocalizations.of(context)!.income),
                  ),
                ],
                onPressed: (int index) {
                  controller.changeCategoriesChartTabIndex(index);
                  if (index == 0) {
                    controller.categoriesChart(isExpense: true);
                  }
                  if (index == 1) {
                    controller.categoriesChart(isExpense: false);
                  }
                },
              ))
            ],
          ),
        ),
        SfCircularChart(
          tooltipBehavior: TooltipBehavior(
            enable: true
          ),
          legend: Legend(isVisible: true, position: LegendPosition.bottom),
          series: <DoughnutSeries<ChartCategory, dynamic>>[
            DoughnutSeries<ChartCategory, dynamic>(
              // pointColorMapper: ColorsManager.chryslerBlue,
              dataSource: data,
             // animationDuration: 20,
              xValueMapper: (ChartCategory cat, _) => cat.name,
              yValueMapper: (ChartCategory cat, _) => cat.elementSum,
              dataLabelSettings: DataLabelSettings(
                isVisible: true,
                labelPosition: ChartDataLabelPosition.outside,
                labelIntersectAction: LabelIntersectAction.none
              ),
            ),
          ],
        ),
      ],
    );
  }
}
