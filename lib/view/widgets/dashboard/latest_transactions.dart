import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/routes/routes.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/constants/config.dart';
import 'package:expenses/view/widgets/shared/card_container.dart';
import 'package:expenses/view/widgets/shared/text_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart' as intl;
import 'package:intl/intl.dart';
import '../../../../data/mock_data.dart';
import '../../../data/enums/transaction_type.dart';
import '../../../data/model/user.dart';
import '../shared/responsive.dart';
import 'category_box.dart';
import 'currency_text.dart';

class LatestTransactions extends StatelessWidget {
  LatestTransactions({Key? key}) : super(key: key);
  final ScrollController _scrollController = ScrollController();
  final User user = userFromJson(GetStorage().read("user"));
  final boxController = Get.put(BoxController());
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
        init: DashboardController(),
        builder: (controller) {
          if(controller.isLoadingInvoices){
            return Center(child: CircularProgressIndicator(color: Colors.blue,),);
          }
          return CategoryBox(
            title: AppLocalizations.of(context)!.lastInvoices,
            suffix: Text(""),
            children: [
              ListView.builder(
                // controller: _scrollController,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: controller.invoicesList.length,
                itemBuilder: (context, index) {
                  // var data = MockData.transactions[index];
                  var data = controller.invoicesList[index];
                  return
                    GestureDetector(
                      onTap: (){
                        boxController.getInvoiceDetails(data.id);
                        boxController.invoiceIsExpense(isExpense: data.expense);
                        boxController.getInvoiceItems();
                        boxController.invoiceInUpdate(isInUpdate: true);
                        Get.toNamed(
                            Routes.home + Routes.addEditInvoiceScreen);
                      },
                      child: CardContainer(
                        child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16.0,
                          vertical: 8.0,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: TextUtils(
                                            text: data.notes,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      TextUtils(
                                        text: data.amount.toString(),
                                        color:data.expense ? ColorsManager.warning :  ColorsManager.success,
                                        fontWeight: FontWeight.w500,

                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 18,),
                                  Row(
                                    children: [
                                      TextUtils(
                                        text: data.beneficiary,
                                        color: ColorsManager.disabled,
                                        fontWeight: FontWeight.bold,),
                                      Spacer(),
                                      TextUtils(
                                        text: Config.formatDate(data.createdDate),
                                        color: ColorsManager.disabled,
                                        fontSize: 12,),
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                  ),
                      ),
                    );
                },
              ),
            ],
          );
        });
  }
}
