import 'package:expenses/services/profile/profile_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/model/user.dart';
import '../../../l10n/app_localizations.dart';
import '../../../logic/controller/auth_controller.dart';
import '../../../logic/controller/profile_controller.dart';
import '../../../utils/constants/colors.dart';
import '../auth/auth_button.dart';
import '../auth/auth_text_form_field.dart';
import '../shared/text_utils.dart';

class EditProfileBottomSheet extends StatelessWidget {
  EditProfileBottomSheet({super.key});

  final User user = userFromJson(GetStorage().read("user"));
  late TextEditingController nameController = TextEditingController(text:user.fullName);
  late TextEditingController phoneController = TextEditingController(text:user.phoneNumber);
  final formKey = GlobalKey<FormState>();
  final controller = Get.find<ProfileController>();


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: TextUtils(
            text: AppLocalizations.of(context)!.editProfile,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: 30),
        Container(
          width: double.infinity,
          // height: MediaQuery.of(context).size.height / 1.6,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  AuthTextFormField(
                    controller: nameController,
                    obscureText: false,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'full name is required';
                      }
                      return null;
                    },
                    hintText: AppLocalizations.of(context)!.username,
                    prefixIcon: CupertinoIcons.person_alt,
                  ),
                  const SizedBox(height: 20),
                  AuthTextFormField(
                    controller: phoneController,
                    obscureText: false,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Phone number is required';
                      }
                      return null;
                    },
                    hintText: AppLocalizations.of(context)!.phone,
                    prefixIcon: CupertinoIcons.phone_fill,
                  ),
                  const SizedBox(height: 30),
                  AuthButton(
                    text: AppLocalizations.of(context)!.save,
                    onPressed: () {
                      if(formKey.currentState!.validate()){
                        controller.updateProfile(
                            nameController.text,
                         phoneController.text
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
