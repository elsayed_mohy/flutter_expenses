import "package:flutter/material.dart";

import "../../../l10n/app_localizations.dart";
import "../../../logic/controller/auth_controller.dart";
import "../../../utils/constants/colors.dart";
import "../shared/text_utils.dart";
import 'package:get/get.dart';

class LogoutBottomSheet extends StatelessWidget {
   LogoutBottomSheet({super.key});
  final controller = Get.find<AuthController>();

  @override
  Widget build(BuildContext context) {
    return         Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          TextUtils(text: AppLocalizations.of(context)!.logout,fontSize: 20,fontWeight: FontWeight.w500,color: ColorsManager.warning,),
          SizedBox(height: 15),

          Container(
            decoration: BoxDecoration(
                border: Border(
                  top: BorderSide( //                    <--- top side
                    color:ColorsManager.disabled.withOpacity(0.5),
                    width: 1.0,
                  ),
                )
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 20,left: 12,right: 12),
              child: Column(
                children: [
                  TextUtils(text: AppLocalizations.of(context)!.logoutConfirm,fontSize: 15),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          if(Get.isBottomSheetOpen ?? false){
                            Get.back();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(100, 50),
                            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 30),
                            shape:RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            backgroundColor:
                            ColorsManager.darkDisabled),
                        child: TextUtils(text: AppLocalizations.of(context)!.cancel, fontSize: 16,color: ColorsManager.white,),
                      ),
                      SizedBox(width: 20),

                      ElevatedButton(
                        onPressed: () {
                          controller.logout();
                        },
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(100, 50),
                            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 30),
                            shape:RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            backgroundColor:
                            ColorsManager.main),
                        child: TextUtils(text: AppLocalizations.of(context)!.logout, fontSize: 16,color: ColorsManager.white,),
                      ),

                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
