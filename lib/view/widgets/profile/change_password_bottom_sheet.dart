import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/model/user.dart';
import '../../../l10n/app_localizations.dart';
import '../../../logic/controller/profile_controller.dart';
import '../../../services/profile/profile_service.dart';
import '../auth/auth_button.dart';
import '../auth/auth_text_form_field.dart';
import '../shared/text_utils.dart';

class ChangePasswordBottomSheet extends StatefulWidget {
  ChangePasswordBottomSheet({super.key});

  @override
  State<ChangePasswordBottomSheet> createState() => _ChangePasswordBottomSheetState();
}

class _ChangePasswordBottomSheetState extends State<ChangePasswordBottomSheet> {
  final controller = Get.find<ProfileController>();

  final TextEditingController oldPasswordController = TextEditingController();

  final TextEditingController newPasswordController = TextEditingController();

  final TextEditingController confirmNewPasswordController =
      TextEditingController();

  final formKey = GlobalKey<FormState>();

  bool oldObscureText = true;

  bool newObscureText = true;

  bool confirmNewObscureText = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: TextUtils(
            text: AppLocalizations.of(context)!.changePassword,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: 30),
        Container(
          width: double.infinity,
          // height: MediaQuery.of(context).size.height / 1.6,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  AuthTextFormField(
                    controller: oldPasswordController,
                    obscureText: oldObscureText,
                    prefixIcon: CupertinoIcons.lock_fill,
                    suffixIcon: IconButton(
                        icon: Icon(oldObscureText
                            ? Icons.visibility
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            oldObscureText = !oldObscureText;
                          });
                        }),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Current Password is required';
                      }
                      return null;
                    },
                    hintText: AppLocalizations.of(context)!.currentPassword,
                  ),
                  const SizedBox(height: 20),
                  AuthTextFormField(
                    controller: newPasswordController,
                    prefixIcon: CupertinoIcons.lock_fill,
                    suffixIcon: IconButton(
                        icon: Icon(newObscureText
                            ? Icons.visibility
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            newObscureText = !newObscureText;
                          });
                        }),
                    obscureText: newObscureText,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'New Password is required';
                      }
                      return null;
                    },
                    hintText: AppLocalizations.of(context)!.newPassword,
                  ),
                  const SizedBox(height: 20),
                  AuthTextFormField(
                    controller: confirmNewPasswordController,
                    obscureText: confirmNewObscureText,
                    prefixIcon: CupertinoIcons.lock_fill,
                    suffixIcon: IconButton(
                        icon: Icon(confirmNewObscureText
                            ? Icons.visibility
                            : Icons.visibility_off,
                        size: 20,),
                        onPressed: () {
                          setState(() {
                            confirmNewObscureText = !confirmNewObscureText;
                          });
                        }),
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'New Password is required';
                      }
                      return null;
                    },
                    hintText: AppLocalizations.of(context)!.confirmNewPassword,
                  ),
                  const SizedBox(height: 30),
                  AuthButton(
                    text: AppLocalizations.of(context)!.save,
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        User user = userFromJson(GetStorage().read("user"));
                        controller.changeUserPassword(
                          oldPasswordController.text,
                          newPasswordController.text,
                          confirmNewPasswordController.text
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
