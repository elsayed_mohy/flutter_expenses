import 'package:expenses/logic/controller/profile_controller.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:expenses/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../l10n/app_localizations.dart';
import '../auth/auth_button.dart';
import '../auth/auth_text_form_field.dart';
import '../shared/text_utils.dart';

class ChangeLanguageBottomSheet extends StatefulWidget {
  ChangeLanguageBottomSheet({super.key});

  @override
  State<ChangeLanguageBottomSheet> createState() => _ChangeLanguageBottomSheetState();
}

class _ChangeLanguageBottomSheetState extends State<ChangeLanguageBottomSheet> {
  final formKey = GlobalKey<FormState>();

  final controller = Get.find<ProfileController>();
  late String language;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    language = controller.lang;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: TextUtils(
            text: AppLocalizations.of(context)!.changeLanguage,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(height: 30),
        Container(
          width: double.infinity,
          // height: MediaQuery.of(context).size.height / 1.6,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  RadioListTile(
                    title: TextUtils(text: "العربية",
                    fontSize: 15,
                    fontWeight: FontWeight.w500,),
                    value: "ar",
                    controlAffinity: ListTileControlAffinity.trailing,
                    dense: true,
                    groupValue: language,
                    onChanged: (value){
                      setState(() {
                        language = value.toString();
                      });
                    },
                  ),

                  RadioListTile(
                    title: TextUtils(text: "English",
                    fontSize: 15,
                    fontWeight: FontWeight.w500,),
                    value: "en",
                    controlAffinity: ListTileControlAffinity.trailing,
                    groupValue: language,
                    onChanged: (value){
                      setState(() {
                        language = value.toString();
                      });
                    },
                  ),
                  const SizedBox(height: 30),
                  GetBuilder<ProfileController>(
                    builder: (_) {
                      return AuthButton(
                        text: AppLocalizations.of(context)!.change,
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            controller.changeLang(language);
                          }
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
