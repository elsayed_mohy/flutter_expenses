import 'package:expenses/logic/controller/auth_controller.dart';
import 'package:expenses/logic/controller/beneficiary_controller.dart';
import 'package:expenses/logic/controller/box_controller.dart';
import 'package:expenses/logic/controller/dashboard_controller.dart';
import 'package:expenses/logic/controller/invitation_controller.dart';
import 'package:expenses/logic/controller/profile_controller.dart';
import 'package:get/get.dart';

class HomeBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => DashboardController());
    Get.lazyPut(() => BeneficiaryController());
    Get.lazyPut(() => BoxController());
    Get.lazyPut(() => AuthController());
    Get.lazyPut(() => InvitationController());
    Get.lazyPut(() => ProfileController());
  }

}
