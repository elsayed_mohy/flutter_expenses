import 'package:expenses/data/model/dasboard/chart_category.dart';
import 'package:expenses/data/model/dasboard/fund_number.dart';
import 'package:expenses/data/model/dasboard/last_modified.dart';
import 'package:expenses/data/model/dasboard/max_beneficiary.dart';
import 'package:expenses/data/model/dasboard/max_box.dart';
import 'package:expenses/data/model/dasboard/user_summary.dart';
import 'package:expenses/services/dashboard/dashboard_service.dart';
import 'package:expenses/utils/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class DashboardController extends GetxController {
  // loaders
  bool isLoadingCategoriesChart = false;
  bool isLoadingBeneficiaryChart = false;
  bool isLoadingBoxChart = false;

  bool isLoadingInvoices = true;
  bool isLoadingMaxBeneficiary = false;
  bool isLoadingMaxBox = false;
  bool isLoadingMemberNumber = false;
  bool isLoadingBoxNumber = false;
  bool isLoadingUserSummary = false;

  List<ChartCategory> categoriesChartList = [];
  List<LastModified> invoicesList = [];

  List<MaxBeneficiary> beneficiaryChartList = [];
  List<MaxBox> boxChartList = [];

  // MaxBeneficiary
  MaxBeneficiary maxBeneficiaryObj =
      MaxBeneficiary(name: "", elementCount: 0, elementSum: 0);
  dynamic isMaxBeneficiary = true;

// MaxBox
  MaxBox maxBoxObj =
      MaxBox(name: "", elementCount: 0, elementSum: 0, elementIndex: 0);
  dynamic isMaxBox = true;

  FundNumber memberNumberObj = FundNumber(elementCount: 0);
  FundNumber boxNumberObj = FundNumber(elementCount: 0);
  UserSummary userSummaryObj = UserSummary(
      totalIncome: 0, elementCount: 0, totalOutcome: 0, difference: 0);

  Color beneficiaryChartColor = ColorsManager.warning;
  Color boxChartColor = ColorsManager.warning;


  RxList<bool> beneficiaryChartIsSelected = [true, false].obs;
  RxList<bool> boxChartIsSelected = [true, false].obs;
  RxList<bool> categoriesChartIsSelected = [true, false].obs;

  void categoriesChart({required bool isExpense}) async {
    var list = await DashboardService().categoriesChart(isExpense);
    try {
      isLoadingCategoriesChart = true;
      if (list.isNotEmpty) {
        categoriesChartList = list;
        update();
      }
    } finally {
      isLoadingCategoriesChart = false;
      update();
    }
  }

  void beneficiaryChart({required bool isExpense}) async {
    var list = await DashboardService().beneficiaryChart(isExpense);
    try {
      isLoadingBeneficiaryChart = true;
      beneficiaryChartList = list;
      update();
    } finally {
      isLoadingBeneficiaryChart = false;
      update();
    }
  }

  void boxChart({required bool isExpense}) async {
    var list = await DashboardService().boxChart(isExpense);
    try {
      isLoadingBoxChart = true;
      boxChartList = list;
      update();
    } finally {
      isLoadingBoxChart = false;
      update();
    }
  }

  void invoices() async {
    var list = await DashboardService().lastModifiedList();
    try {
      isLoadingInvoices = true;
      if (list.isNotEmpty) {
        invoicesList.addAll(list);
        update();
      }
    } finally {
      isLoadingInvoices = false;
      update();
    }
  }

  void maxBeneficiary({required bool isExpense}) async {
    MaxBeneficiary maxBeneficiary =
        await DashboardService().maxBeneficiary(isExpense);
    try {
      isLoadingMaxBeneficiary = true;
      maxBeneficiaryObj = maxBeneficiary;
      isMaxBeneficiary = isExpense;
      update();
    } finally {
      isLoadingMaxBeneficiary = false;
      update();
    }
  }

  void maxBox({required bool isExpense}) async {
    MaxBox maxBox = await DashboardService().maxBox(isExpense);
    try {
      isLoadingMaxBox = true;
      maxBoxObj = maxBox;
      isMaxBox = isExpense;
      update();
    } finally {
      isLoadingMaxBox = false;
      update();
    }
  }

  void memberNumber() async {
    FundNumber memberNumber = await DashboardService().memberNumber();
    try {
      isLoadingMemberNumber = true;
      memberNumberObj = memberNumber;
      update();
    } finally {
      isLoadingMemberNumber = false;
      update();
    }
  }

  void boxNumber() async {
    FundNumber boxNumber = await DashboardService().boxNumber();
    try {
      isLoadingBoxNumber = true;
      boxNumberObj = boxNumber;
      update();
    } finally {
      print("finally");
      isLoadingBoxNumber = false;
      update();
    }
  }

  void userSummary() async {
    UserSummary userSummary = await DashboardService().userSummary();
    try {
      isLoadingUserSummary = true;
      userSummaryObj = userSummary;
      update();
    } finally {
      isLoadingUserSummary = false;
      update();
    }
  }

  updateBeneficiaryChartColor(newColor) {
    beneficiaryChartColor = newColor;
    update();
  }

  updateBoxChartColor(newColor) {
    boxChartColor = newColor;
    update();
  }
  void changeBeneficiaryChartTabIndex(int index) {
    RxInt tabIndex = 0.obs;
    tabIndex.value = index;
    for(int buttonIndex = 0; buttonIndex < beneficiaryChartIsSelected.length; buttonIndex++){
      if(buttonIndex == index){
        beneficiaryChartIsSelected[buttonIndex] = true;
      } else {
        beneficiaryChartIsSelected[buttonIndex] = false;
      }
    }
    update();
  }

  void changeBoxChartTabIndex(int index) {
    RxInt tabIndex = 0.obs;
    tabIndex.value = index;
    for(int buttonIndex = 0; buttonIndex < boxChartIsSelected.length; buttonIndex++){
      if(buttonIndex == index){
        boxChartIsSelected[buttonIndex] = true;
      } else {
        boxChartIsSelected[buttonIndex] = false;
      }
    }
    update();
  }

  void changeCategoriesChartTabIndex(int index) {
    RxInt tabIndex = 0.obs;
    tabIndex.value = index;
    for(int buttonIndex = 0; buttonIndex < categoriesChartIsSelected.length; buttonIndex++){
      if(buttonIndex == index){
        categoriesChartIsSelected[buttonIndex] = true;
      } else {
        categoriesChartIsSelected[buttonIndex] = false;
      }
    }
    update();
  }

  @override
  void onInit() {
    super.onInit();
    categoriesChart(isExpense: true);
    boxChart(isExpense: true);
    beneficiaryChart(isExpense: true);
    maxBeneficiary(isExpense: true);
    maxBox(isExpense: true);
    memberNumber();
    boxNumber();
    userSummary();
    invoices();
  }
}
