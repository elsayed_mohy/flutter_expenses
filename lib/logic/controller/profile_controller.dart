import 'dart:io';
import 'package:expenses/services/base_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import '../../data/model/user.dart';
import '../../l10n/app_localizations.dart';
import '../../services/profile/profile_service.dart';
import '../../utils/constants/app_urls.dart';

class ProfileController extends GetxController {
  bool isLoading = true;
  final GetStorage storage = GetStorage();
  User user = userFromJson(GetStorage().read("user"));
  var lang = 'en';

  updateProfile(fullName, phoneNumber) async {
    try {
      isLoading = true;
      var response = await ProfileService().updateProfile(
          fullName, phoneNumber);
      print("response updateProfile from ProfileController ${response.data}");
      if (response.statusCode == 200) {
        user.fullName = fullName;
        user.phoneNumber = phoneNumber;
        storage.write('user', userToJson(user));
        storage.save();
        if (Get.isBottomSheetOpen ?? false) {
          Get.back();
        }
      }
    } finally {
      isLoading = false;
      update();
    }
  }


  changeUserPassword(oldPassword, newPassword,confirmPassword) async {
    AppLocalizations translate = await AppLocalizations.delegate.load(Locale(BaseService().getCurrentLanguage()));

    try {
      isLoading = true;
      var response = await ProfileService().changeUserPassword(
          oldPassword, newPassword,confirmPassword);
      if(response.data["success"] == true) {
        user.accessToken = response.data["data"]["accessToken"];
        user.refreshToken = response.data["data"]["refreshToken"];
        storage.write('user', userToJson(user));
        storage.save();
        if(Get.isBottomSheetOpen ?? false){
          Get.back();
        }
        BaseService().showSuccess(translate.passwordChanged);
        update();
      }else {
        BaseService().showError(response.data["message"]);
      }

      update();
    } finally {
      isLoading = false;
      update();
    }
  }

  void changeLang(String myLang) {
    saveLang(myLang);
    if(lang == myLang){
      return;
    }
    if(myLang == 'ar'){
      lang = 'ar';
      saveLang('ar');
    }else{
      lang = 'en';
      saveLang('en');
    }
    Get.updateLocale(Locale(lang));
    Get.back();
    update();
  }

  Future pickProfileImage() async {
    //final User user = userFromJson(GetStorage().read("user"));
    try {
      XFile? image = await ImagePicker()
          .pickImage(source: ImageSource.gallery, imageQuality: 30);
      if (image == null) return;
      final imageTemp = File(image.path);
     var img = await BaseService().uploadImage(
          file: imageTemp, id: user.id, pathId: 'IMAGE_USER_PROFILE');
      print("img response ${img["data"]["imagePath"]}");
       User newUser = user;
       newUser.imagePath = img["data"]["imagePath"];
      storage.write('user', userToJson(newUser));
      storage.save();
      user = newUser;
      update();
    } on PlatformException catch (e) {
      print(e);
    }
  }

  void saveLang(String value) async {
    await storage.write("lang", value);
  }

  Future<String> get getLang async {
    return await storage.read("lang") ?? "en";
  }

  @override
  void onInit() async{
    super.onInit();
    lang = await getLang;
  }
}
