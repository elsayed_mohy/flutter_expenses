import 'dart:io';

import 'package:expenses/routes/routes.dart';
import 'package:expenses/services/base_service.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';
import 'package:get_storage/get_storage.dart';

import '../../data/model/user.dart';

class AuthController extends GetxController {
  bool isVisibility = false;
  final Dio dio = Dio();
  bool isLoggedIn = false;
  final GetStorage storage = GetStorage();

  String lang = GetStorage().read<String>('lang') != null
      ? GetStorage().read<String>('lang').toString()
      : BaseService().getDeviceLanguage();

  void visibility() {
    isVisibility = !isVisibility;
    update();
  }

  void login({required String phone, required String password}) async {
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await dio.post(AppUrls.logInEndPoint,
            data: {'login': phone, 'password': password},
            options: Options(
              headers: {'accept-language': '${lang}'},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          isLoggedIn = true;
          storage.write('loggedIn', isLoggedIn);

          User user = User.fromJson(response.data['data']);
          storage.write('user', userToJson(user));
          update();

          Get.offNamed(Routes.home);
        } else {
          BaseService().showError(response.data["message"].toString());
        }
      }
    } on SocketException catch (_) {
      BaseService().showInternetError();
      return;
    }
  }

  void logout() async {
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final User user = userFromJson(GetStorage().read("user"));
        final response = await dio.post(AppUrls.logoutEndPoint,
            data: {'refreshToken': user.refreshToken},
            options: Options(
              headers: {'accept-language': '${lang}'},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          isLoggedIn = false;
          storage.write('loggedIn', isLoggedIn);
          storage.remove("user");
          update();

          Get.offAllNamed(Routes.loginScreen);
        } else {
          BaseService().showError(response.data["message"].toString());
          // Handle the error
        }
      }
    } on SocketException catch (_) {
      BaseService().showInternetError();
      return;
    }
  }

  void sendOTP(
      {@required dialCode,
      @required phoneNumber,
      required bool register}) async {
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await dio.post(AppUrls.sendOTPEndPoint,
            data: {
              "countryCode": dialCode,
              "phoneNumber": phoneNumber,
              "register": register
            },
            options: Options(
              headers: {'accept-language': '${lang}'},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          update();
          Get.offNamed(register
              ? Routes.signUpVerifyOTPScreen
              : Routes.forgotPasswordVerifyOTPScreen);
        } else {
          BaseService().showError(response.data["message"].toString());
        }
      }
    } on SocketException catch (_) {
      BaseService().showInternetError();
      return;
    }
  }

  void validateOTP(
      {@required dialCode,
      @required phoneNumber,
      @required otp,
      required bool register}) async {
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await dio.post(AppUrls.validateOTPEndPoint,
            data: {
              "countryCode": dialCode,
              "phoneNumber": phoneNumber,
              "otp": otp,
              "register": register
            },
            options: Options(
              headers: {'accept-language': '${lang}'},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          storage.write("otp", otp);
          update();
          Get.offNamed(
              register ? Routes.createAccountScreen : Routes.resetPasswordScreen);
        } else {
          BaseService().showError(response.data["message"].toString());
        }
      }
    } on SocketException catch (_) {
      BaseService().showInternetError();
      return;
    }
  }

  void register(
      {@required fullName,
      @required dialCode,
      @required phoneNumber,
      @required password,
      @required preferLanguage,
      @required otp}) async {

    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await dio.post(AppUrls.registerEndPoint,
            data: {
              "fullName": fullName,
              "countryCode": dialCode,
              "phoneNumber": phoneNumber,
              "password": password,
              "preferLanguage": preferLanguage,
              "otp": otp
            },
            options: Options(
              headers: {'accept-language': '${lang}'},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          update();
          Get.offNamed(Routes.loginScreen);
        } else {
          BaseService().showError(response.data["message"].toString());
        }
      }
    } on SocketException catch (_) {
      BaseService().showInternetError();
      return;
    }
  }

  void resetPassword(
      {@required phoneNumber, @required newPassword, @required otp}) async {

    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        final response = await dio.post(AppUrls.resetPasswordEndPoint,
            data: {
              "phoneNumber": phoneNumber,
              "newPassword": newPassword,
              "otp": otp
            },
            options: Options(
              headers: {'accept-language': '${lang}'},
              followRedirects: false,
              validateStatus: (status) {
                return status! < 500;
              },
            ));
        if (response.statusCode == 200) {
          update();
          Get.offNamed(Routes.loginScreen);
        } else {
          BaseService().showError(response.data["message"].toString());
        }
      }
    } on SocketException catch (_) {
      BaseService().showInternetError();
      return;
    }
  }
}
