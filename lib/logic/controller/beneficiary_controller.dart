import 'package:expenses/data/model/beneficiaries/beneficiary.dart';
import 'package:expenses/data/model/beneficiaries/beneficiary_details.dart';
import 'package:expenses/services/beneficiaries/beneficiary_service.dart';
import 'package:get/get.dart';

class BeneficiaryController extends GetxController {
  List<Beneficiary> beneficiariesList = <Beneficiary>[].obs;
  List<Beneficiary> filteredBeneficiariesList = <Beneficiary>[].obs;
  List<BeneficiaryDetails> beneficiaryDetailsList = <BeneficiaryDetails>[];
  late BeneficiaryDetails currentBox;
  List<Invoice> invoices = <Invoice>[];
  var isLoading = true.obs;
  var isLoadingBeneficiaryDetails = false;

  void beneficiaryList() async {
    var list = await BeneficiaryService().beneficiaryList();
    try {
      isLoading(true);
      if (list.isNotEmpty) {
        beneficiariesList.addAll(list);
        print(beneficiariesList);
      }
    } finally {
      isLoading(false);
    }
  }

  void filterBeneficiaryList(beneficiaryName) async {
    try {
      isLoading(true);
      if (beneficiariesList.isNotEmpty) {
        filteredBeneficiariesList = beneficiariesList
            .where((element) => element.beneficiary.contains(beneficiaryName))
            .toList();
        update();
      }
    } finally {
      isLoading(false);
      update();
    }
  }

  getAll(String name) async {
    isLoadingBeneficiaryDetails = true;
    beneficiaryDetailsList =
        await BeneficiaryService().beneficiaryDetails(name);
    filterInvoice(beneficiaryDetailsList[0].name);
    isLoadingBeneficiaryDetails = false;
    update();
  }

  beneficiaryDetails(String name) async {
    var list = await getAll(name);
    if (list != null) {
      beneficiaryDetailsList = await getAll(name);
    } else {
      //beneficiaryDetailsList = [];
    }
    update();
  }

  filterInvoice(String name){
     BeneficiaryDetails box = beneficiaryDetailsList.firstWhere((element) => element.name == name);
     currentBox = box;
     invoices = box.invoices;
     update();
  }

  downloadPdf(String name,dynamic ids) async {
     await BeneficiaryService().openFile(await BeneficiaryService()
         .downFile(name,'${name}account_summary${DateTime.now()}.pdf',ids));
  }

  @override
  void onInit() {
    super.onInit();
    beneficiaryList();
    //downloadPdf();
  }
}
