import 'dart:convert';
import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:dio/dio.dart';
import 'package:expenses/data/model/box/box_details_model.dart';
import 'package:expenses/data/model/invoice/invoice_model.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/services/base_service.dart';
import 'package:expenses/utils/constants/app_urls.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';

import '../../data/model/box_model.dart';
import '../../data/model/user.dart';
import '../../services/boxes/boxes_service.dart';

class BoxController extends GetxController {
  var isLoading = true.obs;
  bool isBoxLoading = false;
  bool isInvoiceLoading = false;
  bool isInvoiceInUpdate = false;
  bool isInvoiceExpense = false;
  bool isInvoiceItemsLoading = false;
  List<String> invoiceBeneficiaryList = [];
  List<String> invoiceTagList = [];
  RxList<BoxModel> boxesList = <BoxModel>[].obs;
  List<Contact> selectedContactList = [];
  RxList<BoxModel> filteredBoxesList = <BoxModel>[].obs;
  RxList<BoxModel> subBoxesList = <BoxModel>[].obs;
  RxList<BoxModel> filteredSubBoxesList = <BoxModel>[].obs;
  List<InvoiceModel> invoicesList = [];
  List<Member> membersList = [];
  BoxDetails boxDetails = BoxDetails(members: []);
  InvoiceModel invoiceModel = InvoiceModel(attachments: []);
  dynamic argumentData = Get.arguments;

  void getBoxesList() async {
    var list = await BoxesService().getBoxes();
    try {
      isLoading(true);
      if (list.isNotEmpty) {
        //boxesList.addAll(list);
        boxesList.value = list;
       // update();
      }
    } finally {
      isLoading(false);

     // update();
    }
  }

  void filterBoxesList(boxName) async {
      if (boxesList.isNotEmpty) {
        filteredBoxesList.value = boxesList
            .where((element) => element.name.contains(boxName))
            .toList();
      }
  }

  void filterSubBoxesList(boxName) async {

      if (boxesList.isNotEmpty) {
        filteredSubBoxesList.value = subBoxesList.value
            .where((element) => element.name.contains(boxName))
            .toList();
      }

  }

  void getSubBoxesList() async {
    var list = await BoxesService().getSubscriptionBoxes();
    try {
      isLoading(true);
      if (list.isNotEmpty) {
        subBoxesList.addAll(list);
        filteredSubBoxesList.addAll(list);
        update();
      }
    } finally {
      isLoading(false);
      update();
    }
  }

  void updateSelectedContactList(Contact contact) {
    if (selectedContactList.contains(contact)) {
      print(contact.displayName);
      selectedContactList.remove(contact);
      update();
    } else {
      selectedContactList.add(contact);
      update();
    }
  }

  void clearSelectedContactList() {
    selectedContactList.clear();
    update();
  }

  void sendInvitation() {
    User user = userFromJson(GetStorage().read("user"));
    var queryParameters = {
      "boxId": boxDetails.id,
      "PhoneNumber":
          selectedContactList.map((e) => e.phones?[0].value).toList(),
      "fromUserId": user.id
    };
    BoxesService().sendInvitation(queryParameters).then((value) {
      Get.back();
      clearSelectedContactList();
    });
  }

  void deleteBox() {
    BoxesService().deleteBox(boxDetails.id).then((value) {
      boxesList.removeWhere((element) => element.id == boxDetails.id);
      update();
      Get.back();
    });
    Get.back();
  }

  void addBox(boxName) async{
    BoxesService().addBox({
      "name":boxName,
      "members":"",
    }).then((value) {
      update();
      BoxesService().getBoxes().then((value) {
        boxesList.assignAll(value);
        boxesList.refresh();
      });
      Get.back();
    });
    print(boxesList.value.map((e) => e.toJson()));
  }
  void editBox(boxName) async{
    BoxesService().editBox({
      "id":boxDetails.id,
      "name":boxName,
      "members":boxDetails.members,
    }).then((value) {
      getBoxDetails(boxDetails.id,boxDetails.edetable);
      BoxesService().getBoxes().then((value) =>
          boxesList.assignAll(value)
      );
      Get.back();
    });
  }

  void deleteInvoice(invoiceId) {
    BoxesService().deleteInvoice(invoiceId).then((value) {
      invoicesList.removeWhere((element) => element.id == invoiceId);
      update();
      Get.back();
    });
  }

  void deleteInvoiceAttach(attachId) {
    BoxesService().deleteInvoiceAttach(attachId).then((value) {
      invoiceModel.attachments.removeWhere((element) => element.id == attachId);
      //invoicesList.removeWhere((element) => element.id == invoiceId);
      update();
      Get.back();
    });
  }
  // deleteInvoiceAttach

  void changeInvoiceType(InvoiceModel invoice) {
    BoxesService().changeInvoiceType(invoice.id).then((value) {
      int index =  invoicesList.indexWhere((element) => element.id == invoice.id);
      invoicesList[index].expense = !invoice.expense;
      getBoxDetails(boxDetails.id,boxDetails.edetable);
      update();
    });
  }




  void getBoxDetails(boxId ,bool editable) async {
    isBoxLoading = true;
    try {
      var box = await BoxesService().getBoxDetails(boxId);
      if (box != null) {
        boxDetails = box;
        filterInvoices(boxId,editable);
        isBoxLoading = false;
        update();
      }
    } finally {
      isBoxLoading = false;
      update();
    }
  }

  getBoxMembers() async{
    var list = await BoxesService().getBoxMembers(boxDetails.id);
    try {
      membersList = list;
      update();
    } finally {
      update();
    }
  }

  toggleMemberType(Member member) async{
    BoxesService().toggleMemberType(member.member.id,boxDetails.id).then((value) {
      int index =  membersList.indexWhere((element) => element.member.id == member.member.id);
      membersList[index].admin = !member.admin;
      getBoxDetails(boxDetails.id,boxDetails.edetable);
      update();
    });
  }

  void filterInvoices(boxId,bool editable) async {
    var list = await BoxesService().filterInvoices({
      "searchCriteria": {"boxId": boxId}
    },editable);
    try {
      //if (list.isNotEmpty) {
        invoicesList = list;
        update();
     // }
    } finally {
      update();
    }
  }

  void getInvoiceItems() async {
    var invoiceBeneficiary = await BoxesService().getInvoiceBeneficiary();
    var invoiceTag = await BoxesService().getInvoiceTag();
    try {
      isInvoiceItemsLoading = true;
      if (invoiceBeneficiary.isNotEmpty) {
        invoiceBeneficiaryList = invoiceBeneficiary;
      }
      if (invoiceTag.isNotEmpty) {
        invoiceTagList = invoiceTag;
      }
      update();
    } finally {
      isInvoiceItemsLoading = false;
      update();
    }
  }


  void invoice(
      {required double amount,
      required String beneficiary,
      required String tag,
      required String note, required bool isUpdate}) {
    var requestBody = {
      'amount': amount,
      'box': {'id': boxDetails.id},
      'beneficiary': beneficiary,
      'expense': isInvoiceExpense,
      'notes': note,
      'tag': tag,
    };

    if(isUpdate){
      requestBody.remove('expense');
      Map<String,Object> map1 = {'id': invoiceModel.id, 'edetable': invoiceModel.edetable, 'expense': invoiceModel.expense};
      requestBody.addAll(map1);
      BoxesService().updateInvoice(requestBody).then((value) {
        getBoxDetails(boxDetails.id,boxDetails.edetable);
        update();
        Get.back();
      });
    }else{
      BoxesService().createInvoice(requestBody).then((value) {
        var invoice = InvoiceModel.fromJson(value['data']);
        if (invoice.id != null) {
          invoiceModel = invoice;
        }
        getBoxDetails(boxDetails.id,boxDetails.edetable);
        update();
        invoiceInUpdate(isInUpdate: true);
        //Get.back();
      });
    }
    update();
  }

  void getInvoiceDetails(id) async {
    isInvoiceLoading = true;
    try {
      var invoice = await BoxesService().getInvoiceDetails(id);
      if (invoice.id != null) {
        invoiceModel = invoice;
        update();
      }
    } finally {
      isInvoiceLoading = false;
      update();
    }
  }
  void invoiceInUpdate({required bool isInUpdate}){
    isInvoiceInUpdate = isInUpdate;
    update();
  }

  void invoiceIsExpense({required bool isExpense}){
    isInvoiceExpense = isExpense;
    update();
  }


  downloadPdf() async {
    await BoxesService().openFile(await BoxesService()
        .downFile('${boxDetails.name}box_summary${DateTime.now()}.pdf',boxDetails.id));
  }

  Future pickImage(invoiceId) async {
    print(invoiceId);
    try {
      XFile? image = await ImagePicker()
          .pickImage(source: ImageSource.gallery, imageQuality: 30);
      if (image == null) return;
      final imageTemp = File(image.path);
      var img = await BaseService().uploadImage(
          file: imageTemp, id: invoiceId, pathId: 'IMAGE_INVOICE_ATTACHMENT');
      getInvoiceDetails(invoiceId);
    } on PlatformException catch (e) {
      print(e.details);
      print(e.code);
      print(e.message);
    }
  }

  f(url){
    BoxesService().saveImageToGallery(url);
  }


  @override
  void onInit() {
    super.onInit();
    getBoxesList();
    getSubBoxesList();
  }


}
