import 'package:expenses/data/model/invitation_model.dart';
import 'package:get/get.dart';
import '../../services/invitations/invitation_service.dart';


class InvitationController extends GetxController {
bool isLoading = true;
var isInvitationsLoading = true.obs;
RxList<Invitation> sentInvitationsList=<Invitation>[].obs;
RxList<Invitation> receivedInvitationsList=<Invitation>[].obs;
  void getSentInvitationsList() async {
    var list = await InvitationService().getSentInvitations();
    try {
      isInvitationsLoading(true);
      if (list.isNotEmpty) {
        sentInvitationsList.value = list;
      }
    } finally {
      isInvitationsLoading(false) ;
    }
  }

void getReceivedInvitationsList() async {
  var list = await InvitationService().getReceivedInvitations();
  try {
    isInvitationsLoading(true) ;
    if (list.isNotEmpty) {
      receivedInvitationsList.value = list;

    }
  } finally {
    isInvitationsLoading(false) ;
  }
}

void acceptInvitation(invitationId) async {
  final response = await InvitationService().acceptInvitation(invitationId);
  try {
    isLoading = true;
    receivedInvitationsList.assignAll(await InvitationService().getReceivedInvitations());
    receivedInvitationsList.refresh();

  } finally {
    isLoading = false;
  }
}

void rejectInvitation(invitationId) async {
  final response = await InvitationService().rejectInvitation(invitationId);
  try {
    isLoading = true;
    receivedInvitationsList.assignAll(await InvitationService().getReceivedInvitations());
    receivedInvitationsList.refresh();
  } finally {
    isLoading = false;
  }
}

void cancelInvitation(invitationId) async {
  final response = await InvitationService().cancelInvitation(invitationId);
  try {
    isLoading = true;
    sentInvitationsList.assignAll(await InvitationService().getSentInvitations());
    sentInvitationsList.refresh();
  } finally {
    isLoading = false;
    update();
  }
}

  @override
  void onInit() {
    super.onInit();
    getSentInvitationsList();
    getReceivedInvitationsList();
  }
}

