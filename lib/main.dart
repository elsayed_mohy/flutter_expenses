import 'package:expenses/l10n/app_localizations.dart';
import 'package:expenses/routes/routes.dart';
import 'package:expenses/services/base_dio.dart';
import 'package:expenses/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';
import 'dart:ui';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BaseDio.initializeInterceptors();
  await GetStorage.init();
  runApp(const MyApp());
}
String getDeviceLanguage() {
  return WidgetsBinding.instance.platformDispatcher.locale.languageCode;
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final deviceLanguage = getDeviceLanguage();
    return GetMaterialApp(
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        localeResolutionCallback: localeCallBack,
        title: 'Flutter Demo',
        locale: GetStorage().read<String>('lang') != null ? Locale(GetStorage().read<String>('lang').toString()) : Locale(deviceLanguage),
        debugShowCheckedModeBanner: false,

        theme: ThemesManager().light,
        darkTheme: ThemesManager().dark,
        themeMode: ThemesManager().getThemeMode(),
        initialRoute: GetStorage().read('loggedIn') == true ? AppRoutes.homeRoute : GetStorage().read('initialized') == true ?  AppRoutes.loginScreen :AppRoutes.welcomeRoute,
        getPages: AppRoutes.routes);
  }

  Locale localeCallBack(Locale? locale, Iterable<Locale> supportedLocales) {
    if (locale == null) {
      return supportedLocales.first;
    }

    for (var supportedLocale in supportedLocales) {
      if (locale.languageCode == supportedLocale.languageCode) {
        return supportedLocale;
      }
    }

    return supportedLocales.first;
  }
}
